﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Geo
{
    public class Track
    {
        private List<LatLng> trackpoints = new List<LatLng>();

        public List<LatLng> Trackpoints
        {
            get { return trackpoints; }
        }

        public void AddTrackpoint(LatLng point)
        {
            trackpoints.Add(point);
        }

        public void AddTrackpoint(double lat, double lng)
        {
            trackpoints.Add(new LatLng(lat, lng));
        }

        public void AddTrackpoint(string lat, string lng, IFormatProvider culture)
        {
            AddTrackpoint(Convert.ToDouble(lat, culture), Convert.ToDouble(lng, culture));
        }

        public void AddTrackpoint(string lat, string lng)
        {
            IFormatProvider culture = CultureInfo.InvariantCulture;
            AddTrackpoint(Convert.ToDouble(lat, culture), Convert.ToDouble(lng, culture));
        }

        public LatLngBounds GetBounds()
        {
            LatLngBounds bounds = new LatLngBounds(Trackpoints[0]);
            foreach (LatLng point in Trackpoints)
            {
                if (!bounds.contains(point))
                    bounds.expand(point);
            }
            return bounds;
        }

        public static Track CreateFromPointsData(string pointsData, Boolean latFirst)
        {
            return CreateFromPointsData(pointsData, CultureInfo.InvariantCulture, latFirst);
        }
        /**
       * @param points
       *            set the points that should be encoded all points have to be in
       *            the following form: latitude, longitude[separator] (if latFirst) or longitude, latitude[separator] (if !latFirst)
       */
        public static Track CreateFromPointsData(string pointsData, IFormatProvider culture, Boolean latFirst)
        {
            Track trk = new Track();
            Char[] pointSeparators = new Char[] { ' ', '\n' };
            Char[] coordSeparators = new Char[] { ',' };

            string[] points = pointsData.Split(pointSeparators);

            for (int i = 0; i < points.Length; i++)
            {
                string[] coords = points[i].Split(coordSeparators);
                if (latFirst)
                    trk.AddTrackpoint(coords[0], coords[1], culture);
                else
                    trk.AddTrackpoint(coords[1], coords[0], culture);
            }
            return trk;
        }

        public static Track CreateFromKmlLineString(string pointsData)
        {
            return CreateFromKmlLineString(pointsData, CultureInfo.InvariantCulture);
        }

        /**
           * @param LineString
           *            set the points that should be encoded all points have to be in
           *            the following form: Longitude,Latitude,Altitude"_"...
           */
        public static Track CreateFromKmlLineString(string pointsData, IFormatProvider culture)
        {
            Track trk = new Track();
            Char[] pointSeparators = new Char[] { ' ', '\n' };
            Char[] coordSeparators = new Char[] { ',' };

            string[] points = pointsData.Split(pointSeparators);

            for (int i = 0; i < points.Length; i++)
            {
                string[] coords = points[i].Split(coordSeparators);
                trk.AddTrackpoint(coords[0], coords[1], culture);
            }
            return trk;
        }

        public LatLng GetCentroid()
        {
            //http://stackoverflow.com/questions/5271583/center-of-gravity-of-a-polygon
            // Not the most legible code...
            LatLng off = trackpoints[0];
            double twicearea = 0;
            double x = 0;
            double y = 0;
            LatLng p1, p2;
            double f;
            for (int i = 0, j = trackpoints.Count - 1; i < trackpoints.Count; j = i++)
            {
                p1 = trackpoints[i];
                p2 = trackpoints[j];
                f = (p1.lat - off.lat) * (p2.lng - off.lng) - (p2.lat - off.lat) * (p1.lng - off.lng);
                twicearea += f;
                x += (p1.lat + p2.lat - 2 * off.lat) * f;
                y += (p1.lng + p2.lng - 2 * off.lng) * f;
            }
            f = twicearea * 3;

            return new LatLng(x / f + off.lat, y / f + off.lng);
        }

        public double GetArea()
        {
            if (Trackpoints.Count < 3)
            {
                return 0;
            }
            double area = GetDeterminant(Trackpoints[Trackpoints.Count - 1].lng, Trackpoints[Trackpoints.Count - 1].lat, Trackpoints[0].lng, Trackpoints[0].lat);
            for (int i = 1; i < Trackpoints.Count; i++)
            {
                area += GetDeterminant(Trackpoints[i - 1].lng, Trackpoints[i - 1].lat, Trackpoints[i].lng, Trackpoints[i].lat);
            }
            return area / 2;
        }

        private static double GetDeterminant(double x1, double y1, double x2, double y2)
        {
            return x1 * y2 - x2 * y1;
        }
    }
}