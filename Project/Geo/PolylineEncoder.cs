/**
 * Reimplementation of Mark McClures Javascript PolylineEncoder
 * All the mathematical logic is more or less copied by McClure
 *  
 * @author Mark Rambow
 * @e-mail markrambow[at]gmail[dot]com
 * @version 0.1
 * 
 * translated from Java into C# Jan Bennewitz
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

//// usage demo
//// initialize trackpoints

//Track trk = new Track();

//trk.AddTrackpoint(new Trackpoint(52.29834, 8.94328));
//trk.AddTrackpoint(new Trackpoint(52.29767, 8.93614));
//trk.AddTrackpoint(new Trackpoint(52.29322, 8.93301));
//trk.AddTrackpoint(new Trackpoint(52.28938, 8.93036));
//trk.AddTrackpoint(new Trackpoint(52.27014, 8.97475));

//System.Diagnostics.Debug.WriteLine(CreateEncodings(trk, 17, 1));

namespace Geo
{
    public class PolylineEncoder
    {

        private int numLevels = 18;
        private int zoomFactor = 2;
        private double verySmall = 0.00001;
        private bool forceEndpoints = true;
        private double[] zoomLevelBreaks;
        public Dictionary<string, double> Bounds { get; private set; }

        // constructor
        public PolylineEncoder(int numLevels, int zoomFactor, double verySmall, bool forceEndpoints)
        {
            this.numLevels = numLevels;
            this.zoomFactor = zoomFactor;
            this.verySmall = verySmall;
            this.forceEndpoints = forceEndpoints;

            Bounds = new Dictionary<string, double>();
            setZoomLevelBreaks();
        }

        public PolylineEncoder()
        {
            Bounds = new Dictionary<string, double>();
            setZoomLevelBreaks();
        }

        private void setZoomLevelBreaks()
        {
            this.zoomLevelBreaks = new double[numLevels];

            for (int i = 0; i < numLevels; i++)
            {
                this.zoomLevelBreaks[i] = verySmall * Math.Pow(this.zoomFactor, numLevels - i - 1);
            }
        }

        /**
         * Douglas-Peucker algorithm, adapted for encoding
         * 
         * @return Dictionary [EncodedPoints;EncodedLevels]
         * 
         */
        public Dictionary<string, string> DPEncode(Track track)
        {
            int i, maxLoc = 0;
            Stack<int[]> stack = new Stack<int[]>();
            double[] dists = new double[track.Trackpoints.Count];
            double maxDist, absMaxDist = 0.0, temp = 0.0;
            int[] current;
            string encodedPoints, encodedLevels;

            if (track.Trackpoints.Count > 2)
            {
                int[] stackVal = new int[] { 0, (track.Trackpoints.Count - 1) };
                stack.Push(stackVal);

                while (stack.Count > 0)
                {
                    current = stack.Pop();
                    maxDist = 0;

                    for (i = current[0] + 1; i < current[1]; i++)
                    {
                        temp = track.Trackpoints[i].DistanceFromSegment(track.Trackpoints[current[0]], track.Trackpoints[current[1]]);
                        if (temp > maxDist)
                        {
                            maxDist = temp;
                            maxLoc = i;
                            if (maxDist > absMaxDist)
                            {
                                absMaxDist = maxDist;
                            }
                        }
                    }
                    if (maxDist > this.verySmall)
                    {
                        dists[maxLoc] = maxDist;
                        int[] stackValCurMax = { current[0], maxLoc };
                        stack.Push(stackValCurMax);
                        int[] stackValMaxCur = { maxLoc, current[1] };
                        stack.Push(stackValMaxCur);
                    }
                }
            }

            encodedPoints = createEncodings(track.Trackpoints, dists); //.Replace("\\", "\\\\");
            encodedLevels = encodeLevels(track.Trackpoints, dists, absMaxDist);

            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("encodedPoints", encodedPoints);
            result.Add("encodedLevels", encodedLevels);
            return result;
        }

        private static int floor1e5(double coordinate)
        {
            return (int)Math.Floor(coordinate * 1e5);
        }

        private static string encodeSignedNumber(int num)
        {
            int sgn_num = num << 1;
            if (num < 0) //if negative invert
            {
                sgn_num = ~(sgn_num);
            }
            return (encodeNumber(sgn_num));
        }

        private static string encodeNumber(int num)
        {
            StringBuilder encodeString = new StringBuilder();
            byte minASCII = 63;
            byte binaryChunkSize = 5;

            while (num >= 0x20)
            {
                //while another chunk follows
                encodeString.Append((char)((0x20 | (num & 0x1f)) + minASCII));
                //OR value with 0x20, convert to decimal and add minASCII
                num >>= binaryChunkSize; //shift to next chunk
            }
            encodeString.Append((char)(num + minASCII));
            return encodeString.ToString();
        }


        /**
         * Now we can use the previous function to march down the list of points and
         * encode the levels. Like createEncodings, we ignore points whose distance
         * (in dists) is undefined.
         */
        private string encodeLevels(List<LatLng> points, double[] dists,
            double absMaxDist)
        {
            int i;
            StringBuilder encoded_levels = new StringBuilder();

            if (this.forceEndpoints)
            {
                encoded_levels.Append(encodeNumber(this.numLevels - 1));
            }
            else
            {
                encoded_levels.Append(encodeNumber(this.numLevels
                    - computeLevel(absMaxDist) - 1));
            }
            for (i = 1; i < points.Count - 1; i++)
            {
                if (dists[i] != 0)
                {
                    encoded_levels.Append(encodeNumber(this.numLevels
                        - computeLevel(dists[i]) - 1));
                }
            }
            if (this.forceEndpoints)
            {
                encoded_levels.Append(encodeNumber(this.numLevels - 1));
            }
            else
            {
                encoded_levels.Append(encodeNumber(this.numLevels
                    - computeLevel(absMaxDist) - 1));
            }
            //		System.out.println("encodedLevels: " + encoded_levels);
            return encoded_levels.ToString();
        }

        /**
         * This computes the appropriate zoom level of a point in terms of it's
         * distance from the relevant segment in the DP algorithm. Could be done in
         * terms of a logarithm, but this approach makes it a bit easier to ensure
         * that the level is not too large.
         */
        private int computeLevel(double absMaxDist)
        {
            int lev = 0;
            if (absMaxDist > this.verySmall)
            {
                lev = 0;
                while (absMaxDist < this.zoomLevelBreaks[lev])
                {
                    lev++;
                }
                return lev;
            }
            return lev;
        }

        private string createEncodings(List<LatLng> points, double[] dists)
        {
            StringBuilder encodedPoints = new StringBuilder();

            double maxlat = 0,
                       minlat = 0,
                       maxlon = 0,
                       minlon = 0;

            int plat = 0;
            int plng = 0;

            for (int i = 0; i < points.Count; i++)
            {

                // determine bounds (max/min lat/lon)
                if (i == 0)
                {
                    maxlat = minlat = points[i].lat;
                    maxlon = minlon = points[i].lng;
                }
                else
                {
                    if (points[i].lat > maxlat)
                    {
                        maxlat = points[i].lat;
                    }
                    else if (points[i].lat < minlat)
                    {
                        minlat = points[i].lat;
                    };
                    if (points[i].lng > maxlon)
                    {
                        maxlon = points[i].lng;
                    }
                    else if (points[i].lng < minlon)
                    {
                        minlon = points[i].lng;
                    };
                }

                if (dists[i] != 0 || i == 0 || i == points.Count - 1)
                {
                    LatLng point = points[i];

                    int late5 = floor1e5(point.lat);
                    int lnge5 = floor1e5(point.lng);

                    int dlat = late5 - plat;
                    int dlng = lnge5 - plng;

                    plat = late5;
                    plng = lnge5;

                    encodedPoints.Append(encodeSignedNumber(dlat));
                    encodedPoints.Append(encodeSignedNumber(dlng));

                }
            }

            Dictionary<string, double> bounds = new Dictionary<string, double>();
            bounds.Add("maxlat", maxlat);
            bounds.Add("minlat", minlat);
            bounds.Add("maxlon", maxlon);
            bounds.Add("minlon", minlon);

            Bounds = bounds;
            return encodedPoints.ToString();
        }

        public static Dictionary<string, string> CreateEncodings(Track track, int level, int step)
        {

            Dictionary<string, string> resultMap = new Dictionary<string, string>();
            StringBuilder encodedPoints = new StringBuilder();
            StringBuilder encodedLevels = new StringBuilder();

            List<LatLng> trackpointList = track.Trackpoints;

            int plat = 0;
            int plng = 0;
            int counter = 0;

            int listSize = trackpointList.Count;

            LatLng trackpoint;

            for (int i = 0; i < listSize; i += step)
            {
                counter++;
                trackpoint = (LatLng)trackpointList[i];

                int late5 = floor1e5(trackpoint.lat);
                int lnge5 = floor1e5(trackpoint.lng);

                int dlat = late5 - plat;
                int dlng = lnge5 - plng;

                plat = late5;
                plng = lnge5;

                encodedPoints.Append(encodeSignedNumber(dlat)).Append(
                    encodeSignedNumber(dlng));
                encodedLevels.Append(encodeNumber(level));

            }

            System.Diagnostics.Debug.WriteLine("[PolylineEncoder.CreateEncodings says:] listSize: " + listSize + " step: " + step
            + " counter: " + counter);

            resultMap.Add("encodedPoints", encodedPoints.ToString());
            resultMap.Add("encodedLevels", encodedLevels.ToString());

            return resultMap;
        }
    }
}