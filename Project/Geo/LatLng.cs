﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geo
{
    public struct LatLng
    {
        internal const double R = 6371000; // Earth's radius in m

        public double lat,
                      lng;

        public LatLng(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        /// <summary>
        /// Constructor from degrees and decimal minutes
        /// </summary>
        /// <param name="latDeg">Latitude - Degrees</param>
        /// <param name="latMin">Latitude - Minutes</param>
        /// <param name="lngDeg">Longitude - Degrees</param>
        /// <param name="lngMin">Longitude - Minutes</param>
        public LatLng(HemisphereNS latHemisphere, UInt16 latDeg, double latMin,
                        HemisphereEW lngHemisphere, UInt16 lngDeg, double lngMin)
            : this()
        {
            double absLat = latDeg + latMin / 60;
            double absLng = lngDeg + lngMin / 60;
            SetLatLngFromAbsLatLngAndHemispheres(latHemisphere, lngHemisphere, absLat, absLng);
        }

        private void SetLatLngFromAbsLatLngAndHemispheres(HemisphereNS latHemisphere, HemisphereEW lngHemisphere, double absLat, double absLng)
        {
            if (latHemisphere == HemisphereNS.N)
                lat = absLat;
            else
                lat = -absLat;

            if (lngHemisphere == HemisphereEW.E)
                lng = absLng;
            else
                lng = -absLng;
        }

        public LatLng(char latHem, int latDeg, int latMin, int latSec, char lngHem, int lngDeg, int lngMin, int lngSec)
            : this()
        {
            HemisphereNS latHemisphere;
            switch (latHem)
            {
                case 'N':
                    latHemisphere = HemisphereNS.N;
                    break;
                case 'S':
                    latHemisphere = HemisphereNS.S;
                    break;
                default:
                    throw new ArgumentException("Unrecognised latitude hemisphere. Value was " + latHem + ".");
            }
            HemisphereEW lngHemisphere;
            switch (lngHem)
            {
                case 'E':
                    lngHemisphere = HemisphereEW.E;
                    break;
                case 'W':
                    lngHemisphere = HemisphereEW.W;
                    break;
                default:
                    throw new ArgumentException("Unrecognised longitude hemisphere. Value was " + lngHem + ".");
            }
            double absLat = latDeg + latMin / 60f + latSec / 60f / 60;
            double absLng = lngDeg + lngMin / 60f + lngSec / 60f / 60;

            SetLatLngFromAbsLatLngAndHemispheres(latHemisphere, lngHemisphere, absLat, absLng);
        }

        public override string ToString()
        {
            return lat + ", " + lng;
        }

        /**
         * distance(segmentPoint1, segmentPoint2) computes the distance between the this and the
         * segment [segmentPoint1, segmentPoint2]. This could probably be replaced with something that is a
         * bit more numerically stable.
         */
        // The result is in degrees
        public double DistanceFromSegment(LatLng segmentPoint1, LatLng segmentPoint2)
        {
            double u, diff = 0.0;

            if (segmentPoint1.lat == segmentPoint2.lat
                && segmentPoint1.lng == segmentPoint2.lng)
            {
                diff = Math.Sqrt(Math.Pow(segmentPoint2.lat - lat, 2)
                    + Math.Pow(segmentPoint2.lng - lng, 2));
            }
            else
            {
                u = ((lat - segmentPoint1.lat)
                    * (segmentPoint2.lat - segmentPoint1.lat) + (lng - segmentPoint1.lng)
                    * (segmentPoint2.lng - segmentPoint1.lng))
                    / (Math.Pow(segmentPoint2.lat - segmentPoint1.lat, 2)
                                  + Math.Pow(segmentPoint2.lng - segmentPoint1.lng, 2));

                if (u <= 0)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint1.lat,
                        2)
                        + Math.Pow(lng - segmentPoint1.lng, 2));
                }
                if (u >= 1)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint2.lat,
                        2)
                        + Math.Pow(lng - segmentPoint2.lng, 2));
                }
                if (0 < u && u < 1)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint1.lat
                        - u * (segmentPoint2.lat - segmentPoint1.lat), 2)
                        + Math.Pow(lng - segmentPoint1.lng - u
                            * (segmentPoint2.lng - segmentPoint1.lng), 2));
                }
            }
            return diff;
        }

        // Math adapted from http://www.movable-type.co.uk/scripts/latlong.html

        public Double DistanceTo(LatLng point)
        {
            return Math.Acos(
                    Math.Sin(DegToRad(lat)) * Math.Sin(DegToRad(point.lat)) +
                    Math.Cos(DegToRad(lat)) * Math.Cos(DegToRad(point.lat)) *
                    Math.Cos(DegToRad(point.lng) - DegToRad(lng))
                ) * R; // simple spherical distance
        }

        //Initial bearing (forward azimuth) to target
        internal double BearingToR(LatLng target) // in radians
        {
            Double y = Math.Sin(target.DegToRad(lng) - DegToRad(lng)) * Math.Cos(target.DegToRad(lat));
            Double x = Math.Cos(DegToRad(lat)) * Math.Sin(target.DegToRad(lat)) -
                    Math.Sin(DegToRad(lat)) * Math.Cos(target.DegToRad(lat)) * Math.Cos(target.DegToRad(lng) - DegToRad(lng));
            return Math.Atan2(y, x);
        }

        public double BearingTo(LatLng target) // in degrees
        {
            return RadToDeg(BearingToR(target));
        }

        ///The along-track distance, from the start point to the closest point on the path to the end point
        public Double AlongTrackDistance(LatLng Start, LatLng End)
        {
            double distanceToStart = DistanceTo(Start);
            double bearingFromStartR = Start.BearingToR(this);
            double bearingStartToEndR = Start.BearingToR(End);
            // cross-track distance:
            double dXt = Math.Asin(Math.Sin(distanceToStart / R) * Math.Sin(bearingFromStartR - bearingStartToEndR)) * R;

            return Math.Acos(Math.Cos(distanceToStart / R) / Math.Cos(dXt / R)) * R;
        }

        public List<LatLng> CirclePoints(double radius, int count)
        {
            var locs = new List<LatLng>();

            for (int n = 0; n < count; n++)
            {
                double bearing = 2 * Math.PI * n / count;
                locs.Add(GoTo(bearing, radius));
            }
            return locs;
        }

        public LatLng GoTo(double bearing, double distance)
        {
            //Formula:  lat2 = asin(sin(lat1)*cos(d/R) + cos(lat1)*sin(d/R)*cos(θ)) 
            //          lon2 = lon1 + atan2(sin(θ)*sin(d/R)*cos(lat1), cos(d/R)−sin(lat1)*sin(lat2)) 

            double destLatR = Math.Asin(Math.Sin(DegToRad(lat)) * Math.Cos(distance / R) + Math.Cos(DegToRad(lat)) * Math.Sin(distance / R) * Math.Cos(bearing));
            double destLngR = DegToRad(lng) + Math.Atan2(Math.Sin(bearing) * Math.Sin(distance / R) * Math.Cos(DegToRad(lat)), Math.Cos(distance / R) - Math.Sin(DegToRad(lat)) * Math.Sin(destLatR));

            return new LatLng(RadToDeg(destLatR), RadToDeg(destLngR));
        }

        private double DegToRad(double angle) { return Math.PI * angle / 180.0; }
        private double RadToDeg(double angle) { return angle * 180.0 / Math.PI; }
    }

    public enum HemisphereNS
    {
        N,
        S
    }
    public enum HemisphereEW
    {
        E,
        W
    }
}
