﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geo
{
    public struct LatLngBounds
    {
        private const int GMAPGLOBEWIDTH = 256; // a constant in Google's map projection

        public double
            latMin,
            latMax,
            lngMin,
            lngMax;

        public LatLngBounds(LatLng initialPoint)
        {
            latMin = initialPoint.lat;
            latMax = latMin;
            lngMin = initialPoint.lng;
            lngMax = lngMin;
        }

        public LatLng Center
        {
            get { return new LatLng((latMin + latMax) / 2, (lngMin + lngMax) / 2); }
        }

        public bool contains(LatLng point)
        {
            //TODO: Currently fails if bounds cross 180 deg longitude
            return (latMin <= point.lat && point.lat <= latMax && lngMin <= point.lng && point.lng <= lngMax);
        }

        public void expand(LatLng point)
        {
            if (NotInitialised)
            {
                latMin = point.lat;
                latMax = point.lat;
                lngMin = point.lng;
                lngMax = point.lng;
            }
            else
            {
                //TODO: Currently fails when crossing 180 deg longitude
                latMin = Math.Min(latMin, point.lat);
                latMax = Math.Max(latMax, point.lat);
                lngMin = Math.Min(lngMin, point.lng);
                lngMax = Math.Max(lngMax, point.lng);
            }
        }

        public void expand(LatLngBounds bounds)
        {
            if (NotInitialised)
            {
                latMin = bounds.latMin;
                latMax = bounds.latMax;
                lngMin = bounds.lngMin;
                lngMax = bounds.lngMax;
            }
            else
            {
                //TODO: Currently fails on bounds crossing 180 deg longitude
                latMin = Math.Min(latMin, bounds.latMin);
                latMax = Math.Max(latMax, bounds.latMax);
                lngMin = Math.Min(lngMin, bounds.lngMin);
                lngMax = Math.Max(lngMax, bounds.lngMax);
            }
        }

        public double SizeLat
        {
            get { return latMax - latMin; }
        }

        public double SizeLng
        {
            get
            {
                double lngSize = lngMax - lngMin;
                if (lngSize < 0)
                    lngSize += 360;
                return lngSize;
            }
        }

        public int GetGMapZoom(int pixelWidth, int pixelHeight)
        {
            // Scale in pixel per degree if bounds are displayed within specified size
            double pixelPerDegree = Math.Min(pixelWidth / SizeLat, pixelHeight / SizeLng);

            return (int)Math.Floor(Math.Log(360 * pixelPerDegree / GMAPGLOBEWIDTH, 2));
        }

        public int GMapSizeInPixel(int zoom)
        {
            double pixelPerDegree = Math.Pow(2, zoom) * GMAPGLOBEWIDTH / 360;
            return (int)Math.Ceiling(pixelPerDegree * Math.Max(SizeLat, SizeLng));
        }

        private Boolean NotInitialised
        {
            get { return latMin == 0 && latMax == 0 && lngMin == 0 && lngMax == 0; }
        }
    }
}
