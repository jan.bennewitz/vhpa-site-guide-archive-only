﻿namespace VHPASiteGuide
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnBrowseSourceDir = new System.Windows.Forms.Button();
            this.sourceDir = new System.Windows.Forms.TextBox();
            this.targetDir = new System.Windows.Forms.TextBox();
            this.btnBrowseTargetDir = new System.Windows.Forms.Button();
            this.btnBrowseIncludeDir = new System.Windows.Forms.Button();
            this.includeDir = new System.Windows.Forms.TextBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.lblInclude = new System.Windows.Forms.Label();
            this.progressReport = new System.Windows.Forms.TextBox();
            this.lblBase = new System.Windows.Forms.Label();
            this.baseDir = new System.Windows.Forms.TextBox();
            this.btnBrowseBaseDir = new System.Windows.Forms.Button();
            this.saveSettings = new System.Windows.Forms.Button();
            this.btnExtract = new System.Windows.Forms.Button();
            this.btnUpdateCAR166Data = new System.Windows.Forms.Button();
            this.btnCanungraAreas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(330, 233);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 12;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnBrowseSourceDir
            // 
            this.btnBrowseSourceDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseSourceDir.Location = new System.Drawing.Point(381, 73);
            this.btnBrowseSourceDir.Name = "btnBrowseSourceDir";
            this.btnBrowseSourceDir.Size = new System.Drawing.Size(24, 20);
            this.btnBrowseSourceDir.TabIndex = 5;
            this.btnBrowseSourceDir.Text = "...";
            this.btnBrowseSourceDir.UseVisualStyleBackColor = true;
            this.btnBrowseSourceDir.Click += new System.EventHandler(this.btnBrowseSourceDir_Click);
            // 
            // sourceDir
            // 
            this.sourceDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::VHPASiteGuide.Properties.Settings.Default, "sourceDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.sourceDir.Location = new System.Drawing.Point(2, 73);
            this.sourceDir.Name = "sourceDir";
            this.sourceDir.Size = new System.Drawing.Size(373, 20);
            this.sourceDir.TabIndex = 4;
            this.sourceDir.Text = global::VHPASiteGuide.Properties.Settings.Default.sourceDir;
            // 
            // targetDir
            // 
            this.targetDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.targetDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::VHPASiteGuide.Properties.Settings.Default, "targetDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.targetDir.Location = new System.Drawing.Point(2, 119);
            this.targetDir.Name = "targetDir";
            this.targetDir.Size = new System.Drawing.Size(373, 20);
            this.targetDir.TabIndex = 7;
            this.targetDir.Text = global::VHPASiteGuide.Properties.Settings.Default.targetDir;
            // 
            // btnBrowseTargetDir
            // 
            this.btnBrowseTargetDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseTargetDir.Location = new System.Drawing.Point(381, 119);
            this.btnBrowseTargetDir.Name = "btnBrowseTargetDir";
            this.btnBrowseTargetDir.Size = new System.Drawing.Size(24, 20);
            this.btnBrowseTargetDir.TabIndex = 8;
            this.btnBrowseTargetDir.Text = "...";
            this.btnBrowseTargetDir.UseVisualStyleBackColor = true;
            this.btnBrowseTargetDir.Click += new System.EventHandler(this.btnBrowseTargetDir_Click);
            // 
            // btnBrowseIncludeDir
            // 
            this.btnBrowseIncludeDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseIncludeDir.Location = new System.Drawing.Point(381, 167);
            this.btnBrowseIncludeDir.Name = "btnBrowseIncludeDir";
            this.btnBrowseIncludeDir.Size = new System.Drawing.Size(24, 20);
            this.btnBrowseIncludeDir.TabIndex = 11;
            this.btnBrowseIncludeDir.Text = "...";
            this.btnBrowseIncludeDir.UseVisualStyleBackColor = true;
            this.btnBrowseIncludeDir.Click += new System.EventHandler(this.btnBrowseIncludeDir_Click);
            // 
            // includeDir
            // 
            this.includeDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.includeDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::VHPASiteGuide.Properties.Settings.Default, "includeDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.includeDir.Location = new System.Drawing.Point(2, 167);
            this.includeDir.Name = "includeDir";
            this.includeDir.Size = new System.Drawing.Size(373, 20);
            this.includeDir.TabIndex = 10;
            this.includeDir.Text = global::VHPASiteGuide.Properties.Settings.Default.includeDir;
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(-1, 57);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 3;
            this.lblSource.Text = "Source:";
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Location = new System.Drawing.Point(-1, 103);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(41, 13);
            this.lblTarget.TabIndex = 6;
            this.lblTarget.Text = "Target:";
            // 
            // lblInclude
            // 
            this.lblInclude.AutoSize = true;
            this.lblInclude.Location = new System.Drawing.Point(-1, 151);
            this.lblInclude.Name = "lblInclude";
            this.lblInclude.Size = new System.Drawing.Size(89, 13);
            this.lblInclude.TabIndex = 9;
            this.lblInclude.Text = "Include files from:";
            // 
            // progressReport
            // 
            this.progressReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressReport.Location = new System.Drawing.Point(2, 268);
            this.progressReport.Multiline = true;
            this.progressReport.Name = "progressReport";
            this.progressReport.ReadOnly = true;
            this.progressReport.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.progressReport.Size = new System.Drawing.Size(403, 228);
            this.progressReport.TabIndex = 13;
            // 
            // lblBase
            // 
            this.lblBase.AutoSize = true;
            this.lblBase.Location = new System.Drawing.Point(-1, 12);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(120, 13);
            this.lblBase.TabIndex = 0;
            this.lblBase.Text = "Common base directory:";
            // 
            // baseDir
            // 
            this.baseDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.baseDir.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::VHPASiteGuide.Properties.Settings.Default, "baseDir", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.baseDir.Location = new System.Drawing.Point(2, 28);
            this.baseDir.Name = "baseDir";
            this.baseDir.Size = new System.Drawing.Size(373, 20);
            this.baseDir.TabIndex = 1;
            this.baseDir.Text = global::VHPASiteGuide.Properties.Settings.Default.baseDir;
            // 
            // btnBrowseBaseDir
            // 
            this.btnBrowseBaseDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseBaseDir.Location = new System.Drawing.Point(381, 28);
            this.btnBrowseBaseDir.Name = "btnBrowseBaseDir";
            this.btnBrowseBaseDir.Size = new System.Drawing.Size(24, 20);
            this.btnBrowseBaseDir.TabIndex = 2;
            this.btnBrowseBaseDir.Text = "...";
            this.btnBrowseBaseDir.UseVisualStyleBackColor = true;
            this.btnBrowseBaseDir.Click += new System.EventHandler(this.btnBrowseBaseDir_Click);
            // 
            // saveSettings
            // 
            this.saveSettings.Location = new System.Drawing.Point(2, 233);
            this.saveSettings.Name = "saveSettings";
            this.saveSettings.Size = new System.Drawing.Size(86, 23);
            this.saveSettings.TabIndex = 14;
            this.saveSettings.Text = "Make default";
            this.saveSettings.UseVisualStyleBackColor = true;
            this.saveSettings.Click += new System.EventHandler(this.saveSettings_Click);
            // 
            // btnExtract
            // 
            this.btnExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExtract.Location = new System.Drawing.Point(88, 204);
            this.btnExtract.Name = "btnExtract";
            this.btnExtract.Size = new System.Drawing.Size(86, 23);
            this.btnExtract.TabIndex = 15;
            this.btnExtract.Text = "Extract data";
            this.btnExtract.UseVisualStyleBackColor = true;
            this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
            // 
            // btnUpdateCAR166Data
            // 
            this.btnUpdateCAR166Data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateCAR166Data.Location = new System.Drawing.Point(297, 204);
            this.btnUpdateCAR166Data.Name = "btnUpdateCAR166Data";
            this.btnUpdateCAR166Data.Size = new System.Drawing.Size(108, 23);
            this.btnUpdateCAR166Data.TabIndex = 16;
            this.btnUpdateCAR166Data.Text = "Update CAR166";
            this.btnUpdateCAR166Data.UseVisualStyleBackColor = true;
            this.btnUpdateCAR166Data.Click += new System.EventHandler(this.btnUpdateCAR166Data_Click);
            // 
            // btnCanungraAreas
            // 
            this.btnCanungraAreas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCanungraAreas.Location = new System.Drawing.Point(180, 204);
            this.btnCanungraAreas.Name = "btnCanungraAreas";
            this.btnCanungraAreas.Size = new System.Drawing.Size(111, 23);
            this.btnCanungraAreas.TabIndex = 17;
            this.btnCanungraAreas.Text = "Get Canungra areas";
            this.btnCanungraAreas.UseVisualStyleBackColor = true;
            this.btnCanungraAreas.Click += new System.EventHandler(this.btnCanungraAreas_Click);
            // 
            // Main
            // 
            this.AcceptButton = this.btnGenerate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 508);
            this.Controls.Add(this.btnCanungraAreas);
            this.Controls.Add(this.btnUpdateCAR166Data);
            this.Controls.Add(this.btnExtract);
            this.Controls.Add(this.saveSettings);
            this.Controls.Add(this.lblBase);
            this.Controls.Add(this.baseDir);
            this.Controls.Add(this.btnBrowseBaseDir);
            this.Controls.Add(this.progressReport);
            this.Controls.Add(this.lblInclude);
            this.Controls.Add(this.lblTarget);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.btnBrowseIncludeDir);
            this.Controls.Add(this.includeDir);
            this.Controls.Add(this.btnBrowseTargetDir);
            this.Controls.Add(this.targetDir);
            this.Controls.Add(this.sourceDir);
            this.Controls.Add(this.btnBrowseSourceDir);
            this.Controls.Add(this.btnGenerate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(427, 360);
            this.Name = "Main";
            this.Text = "VHPA Site Guide Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnBrowseSourceDir;
        private System.Windows.Forms.TextBox sourceDir;
        private System.Windows.Forms.TextBox targetDir;
        private System.Windows.Forms.Button btnBrowseTargetDir;
        private System.Windows.Forms.Button btnBrowseIncludeDir;
        private System.Windows.Forms.TextBox includeDir;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.Label lblInclude;
        private System.Windows.Forms.TextBox progressReport;
        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.TextBox baseDir;
        private System.Windows.Forms.Button btnBrowseBaseDir;
        private System.Windows.Forms.Button saveSettings;
        private System.Windows.Forms.Button btnExtract;
        private System.Windows.Forms.Button btnUpdateCAR166Data;
        private System.Windows.Forms.Button btnCanungraAreas;
    }
}

