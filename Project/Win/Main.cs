﻿using System;
using System.Windows.Forms;
using System.Configuration;
using VHPASiteGuide.Properties;

namespace VHPASiteGuide
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btnBrowseBaseDir_Click(object sender, EventArgs e)
        {
            browseDirForTextBox(baseDir, "Base Directory");
        }

        private void btnBrowseSourceDir_Click(object sender, EventArgs e)
        {
            browseDirForTextBoxWithBaseDir(sourceDir, "Source Directory");
        }

        private void btnBrowseTargetDir_Click(object sender, EventArgs e)
        {
            browseDirForTextBoxWithBaseDir(targetDir, "Target Directory");
        }

        private void btnBrowseIncludeDir_Click(object sender, EventArgs e)
        {
            browseDirForTextBoxWithBaseDir(includeDir, "Copy files from ...");
        }

        private static void browseDirForTextBox(TextBox textBox, string description)
        {
            using (FolderBrowserDialog dirBrowser = new FolderBrowserDialog { SelectedPath = textBox.Text, Description = description })
                if (dirBrowser.ShowDialog() == DialogResult.OK)
                    textBox.Text = dirBrowser.SelectedPath;
        }

        private void browseDirForTextBoxWithBaseDir(TextBox textBox, string description)
        {
            using (FolderBrowserDialog dirBrowser = new FolderBrowserDialog { SelectedPath = getFullDir(textBox.Text), Description = description })
            {
                if (dirBrowser.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        textBox.Text = getSubDir(dirBrowser.SelectedPath);
                    }
                    catch (ArgumentException e)
                    {
                        MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            Enabled = false;
            Generator generator = new Generator(getFullDir(sourceDir.Text), getFullDir(targetDir.Text));
            generator.ProgressEvent += progressEvent;
            generator.GenerateAll();
            generator.CopyIncludeDir(getFullDir(includeDir.Text));
            status("Done.");
            statusNewLine();
            Enabled = true;
        }

        private void progressEvent(object sender, ProgressEventArgs progressEventArgs)
        {
            status(progressEventArgs.Text);
        }

        private void statusNewLine()
        {
            progressReport.AppendText(Environment.NewLine);
        }

        private void status(string text)
        {
            progressReport.AppendText(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.ToString("fff") + ": " + text + Environment.NewLine);
        }

        private string getFullDir(string subDir)
        {
            return System.IO.Path.Combine(baseDir.Text, subDir);
        }

        private string getSubDir(string fullDir)
        {
            if (fullDir == baseDir.Text)
                return "";
            if (fullDir.StartsWith(baseDir.Text + "\\"))
                return fullDir.Substring(baseDir.Text.Length + 1);
            else
                throw new ArgumentException("Not a subdirectory of the base directory.");
        }

        private void saveSettings_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();
        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            Enabled = false;
            Generator generator = new Generator(getFullDir(sourceDir.Text), getFullDir(targetDir.Text));
            generator.ProgressEvent += progressEvent;
            generator.ExtractData();
            status("Done.");
            statusNewLine();
            Enabled = true;
        }

        private void btnUpdateCAR166Data_Click(object sender, EventArgs e)
        {
            Enabled = false;

            using (var fileBrowser = new OpenFileDialog { Title = "Select CAR166 OpenAir airspace file" })
                if (fileBrowser.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        int count = new Generator(getFullDir(sourceDir.Text), "").UpdateCAR166DataFromFile(fileBrowser.FileName);
                        status(count + " CAR166 aerodromes found and written to VHFAerodromes.json.");
                    }
                    catch (ArgumentException ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        status("Error: " + ex.Message);
                    }

                    status("Done.");
                    statusNewLine();
                }

            Enabled = true;
        }

        private void btnCanungraAreas_Click(object sender, EventArgs e)
        {
            Enabled = false;
            int count;

            using (var fileBrowserSource = new OpenFileDialog { Title = "Select Canungra areas kml" })// Get source from http://www.google.com/maps/d/u/0/kml?mid=1h1Gv6VQcD7eqYd8xFSakgibMfWE
                if (fileBrowserSource.ShowDialog() == DialogResult.OK)
                    using (var fileBrowserTarget = new SaveFileDialog { Title = "Select target file", DefaultExt = "xml" })
                        if (fileBrowserTarget.ShowDialog() == DialogResult.OK)
                            try
                            {
                                count = (new Generator("", "")).ReadCanungraAreas(fileBrowserSource.FileName, fileBrowserTarget.FileName);
                                status(count + " areas imported and written to target file.");
                            }
                            catch (ArgumentException ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                status("Error: " + ex.Message);
                            }

            status("Done.");
            statusNewLine();
            Enabled = true;
        }
    }
}
