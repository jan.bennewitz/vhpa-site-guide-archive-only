﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Xsl;
using System.Xml.Schema;
using System.IO;

namespace VHPASiteGuide
{
    public class DataExtractor
    {
        static void Main(string[] args)
        {

        }

        public event ProgressEventHandler ProgressEvent;

        private string
          sourceDir,
          targetDir,
          sitesDir;

        public DataExtractor(string SourceDir, string TargetDir)
        {
            sourceDir = SourceDir;
            targetDir = TargetDir;
            if (targetDir.EndsWith("\\"))
                targetDir = targetDir.Remove(targetDir.Length - 1);
            sitesDir = targetDir + "\\Sites";
        }

        private void Generate(string[] SiteNames)
        {
            XmlDocument xmlSites = LoadSiteGuideData();

            EnsureDirExists(targetDir);

            //Generate index pages
            //all sites
            runSimpleXsl(sourceDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesindex.html", null);
            //open sites
            XsltArgumentList argsOpenIndex = new XsltArgumentList();
            argsOpenIndex.AddParam("restrictToType", string.Empty, "open");
            runSimpleXsl(sourceDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesopenindex.html", argsOpenIndex);
            //closed sites
            XsltArgumentList argsClosedIndex = new XsltArgumentList();
            argsClosedIndex.AddParam("restrictToType", string.Empty, "closed");
            runSimpleXsl(sourceDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesclosedindex.html", argsClosedIndex);

            //microlight sites
            runSimpleXsl(sourceDir + "\\sitesmicrolightindex.xsl", xmlSites, targetDir + "\\sitesmicrolightindex.html", null);

            //Generate site page(s)
            EnsureDirExists(sitesDir);
            XslCompiledTransform sitePageTransform = new XslCompiledTransform();
            sitePageTransform.Load(sourceDir + "\\sitespage.xsl");
            if (SiteNames == null)
            {
                XmlNodeList sites = xmlSites.SelectNodes("//siteFreeFlight");
                raiseProgressEvent("Generate " + sites.Count + " site pages...");
                foreach (XmlNode site in sites)
                {
                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }
            else
            {
                foreach (string siteName in SiteNames)
                {
                    raiseProgressEvent("Generate site page for \"" + siteName + "\"");
                    XmlNode site = xmlSites.SelectSingleNode("//siteFreeFlight[name=\"" + siteName + "\"]");

                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }

            //Generate kmz
            // for Google Earth
            generateKmz(sourceDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesAll.kmz", null);

            // for Google Earth - freeflight only
            XsltArgumentList argsFreeflight = new XsltArgumentList();
            argsFreeflight.AddParam("subset", string.Empty, "freeflight");
            generateKmz(sourceDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesFreeflight.kmz", argsFreeflight);

            // for Google Earth - microlight only
            XsltArgumentList argsMicrolight = new XsltArgumentList();
            argsMicrolight.AddParam("subset", string.Empty, "microlight");
            generateKmz(sourceDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesMicrolight.kmz", argsMicrolight);

            // for Google Earth - closed only
            XsltArgumentList argsClosed = new XsltArgumentList();
            argsClosed.AddParam("subset", string.Empty, "closed");
            generateKmz(sourceDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesClosed.kmz", argsClosed);

            // for map page
            //encode mapShape coordinates
            PolylineEncoder polyEncoder = new PolylineEncoder(18, 2, 1e-5, true);
            foreach (XmlNode coordNode in xmlSites.SelectNodes("//mapShape/coordinates"))
            {
                Dictionary<String, String> dpEncoded = polyEncoder.DPEncode(PolylineEncoder.PointsToTrack(coordNode.InnerText.Trim(), false));
                coordNode.ParentNode.AppendChild(xmlSites.CreateElement("encodedCoordinates")).InnerText = dpEncoded["encodedPoints"];
                coordNode.ParentNode.AppendChild(xmlSites.CreateElement("encodedLevels")).InnerText = dpEncoded["encodedLevels"];
            }
            //encode launch coordinates ** currently not used **
            Track launchCoords = new Track();
            foreach (XmlNode markerNode in xmlSites.SelectNodes("//siteFreeFlight/launch|//siteMicrolight"))
            {
                launchCoords.AddTrackpoint(markerNode.SelectSingleNode("lat").InnerText, markerNode.SelectSingleNode("lon").InnerText);
            }
            string encodedCoords = PolylineEncoder.CreateEncodings(launchCoords, 0, 1)["encodedPoints"];
            xmlSites.DocumentElement.AppendChild(xmlSites.CreateElement("encodedMarkerCoordinates")).InnerText = encodedCoords;

            runSimpleXsl(sourceDir + "\\sitesForGMaps.xsl", xmlSites, targetDir + "\\sitesForGMaps.xml", null);

            //Print/download guide
            int maxSitesPerPage = 20; // max sites per 'download or print' page
            string areasWithPrintGuidesXPath = "siteGuideData//area[" +
              ".//siteFreeFlight[not(closed)] and " +
              "(" +
                "(" +
                  "count(.//siteFreeFlight[not(closed)]) <= " + maxSitesPerPage + " and " +
                  "(count(..//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " or parent::siteGuideData)" +
                ") " +
                "or count(.//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " and not(area)" +
              ")" +
            "]";
            XmlNodeList areasWithPrintGuides = xmlSites.SelectNodes(areasWithPrintGuidesXPath);

            //Generate print index
            XsltArgumentList argsPrintIndex = new XsltArgumentList();
            argsPrintIndex.AddParam("maxSitesPerPage", string.Empty, maxSitesPerPage);
            runSimpleXsl(sourceDir + "\\downloads.xsl", xmlSites, targetDir + "\\downloads.html", argsPrintIndex);

            //Generate downloadable/printable site guides
            XslCompiledTransform printGuideTransform = new XslCompiledTransform();
            printGuideTransform.Load(sourceDir + "\\sitesForPrint.xsl");
            GeneratePrintGuides(xmlSites, printGuideTransform, areasWithPrintGuides);

            raiseProgressEvent("Generated site guide.");
        }

        private void GeneratePrintGuides(XmlDocument xmlSites, XslCompiledTransform printGuideTransform, XmlNodeList areas)
        {
            foreach (XmlNode area in areas)
            {
                XmlWriter pageResult = XmlWriter.Create(targetDir + "\\VHPA Site Guide - " + AreaFullName(area) + ".html", printGuideTransform.OutputSettings);
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("area", string.Empty, area);
                printGuideTransform.Transform(xmlSites, args, pageResult);
                pageResult.Close();
            }
        }

        private static string AreaFullName(XmlNode area)
        {
            string areaName = area.Attributes.GetNamedItem("name").InnerText;
            if (area.ParentNode.Name == "area")
                return AreaFullName(area.ParentNode) + " - " + areaName;
            else
                return areaName;
        }

        private void GenerateSitePage(XmlDocument xmlSites, XslCompiledTransform sitePageTransform, XmlNode site)
        {
            XmlWriter pageResult = XmlWriter.Create(SitePageFileName(site.SelectSingleNode("name").InnerText), sitePageTransform.OutputSettings);
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("siteName", string.Empty, site.SelectSingleNode("name").InnerText);
            sitePageTransform.Transform(xmlSites, args, pageResult);
            pageResult.Close();
        }

        private string SitePageFileName(string siteName)
        {
            return sitesDir + "\\" + siteName + ".html";
        }

        private XmlDocument LoadSiteGuideData()
        {
            //Load and validate
            raiseProgressEvent("Load data ...");
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(null, sourceDir + "\\siteGuideData.xsd");
            settings.ValidationType = ValidationType.Schema;
            //            settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);

            XmlReader reader = XmlReader.Create(sourceDir + "\\siteGuideData.xml", settings);
            XmlDocument xmlSites = new XmlDocument();
            xmlSites.Load(reader);
            reader.Close();

            xmlSites.Validate(new ValidationEventHandler(ValidationEventHandler));
            raiseProgressEvent("Data loaded and valid.");
            return xmlSites;
        }

        private void generateKmz(string transformFile, XmlDocument xmlSites, string targetFile, XsltArgumentList args)
        {
            raiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(transformFile);
            MemoryStream kml = new MemoryStream();
            XmlWriter result = XmlWriter.Create(kml, transform.OutputSettings);
            transform.Transform(xmlSites, args, result);
            result.Close();
            byte[] buffer = new byte[(int)kml.Length];
            kml.Position = 0;
            kml.Read(buffer, 0, (int)kml.Length);
            kmlSaveAsKmz(buffer, targetFile);
        }

        private void runSimpleXsl(string transformFile, XmlDocument source, string targetFile, XsltArgumentList args)
        {
            raiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(transformFile);
            XmlWriter result = XmlWriter.Create(targetFile, transform.OutputSettings);
            transform.Transform(source, args, result);
            result.Close();
        }

        public void CopyIncludeDir(string includeDir)
        {
            raiseProgressEvent("Copy Include directory");
            copyDir(new DirectoryInfo(includeDir), new DirectoryInfo(targetDir));
        }

        private static void copyDir(DirectoryInfo source, DirectoryInfo target)
        {
            //A simple directory copy.

            //ignore svn source control dirs
            if (source.Name == ".svn") return;

            //Copy all files
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy all subdirectories
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                copyDir(dir, target.CreateSubdirectory(dir.Name));
            }
        }

        public static void EnsureDirExists(string dir)
        {
            //TODO: Maybe get user confirmation for create
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    break;
            }

        }

        void raiseProgressEvent(string description)
        {
            if (ProgressEvent != null)
                ProgressEvent(this, new ProgressEventArgs(description));
        }

        static void kmlSaveAsKmz(byte[] kmlBytes, string outFile)
        {
            System.IO.MemoryStream ms = new MemoryStream();
            ZipOutputStream zs = new ZipOutputStream(ms);
            zs.SetLevel(6);
            zs.IsStreamOwner = true;

            //create zipped kml entry
            ZipEntry zipEntry = new ZipEntry("Doc.kml");

            zipEntry.DateTime = DateTime.Now;
            zipEntry.Size = kmlBytes.Length;

            zs.PutNextEntry(zipEntry);
            zs.Write(kmlBytes, 0, kmlBytes.Length);
            zs.CloseEntry();

            zs.Finish();

            //write to disk
            FileStream kmz = new FileStream(outFile, FileMode.Create);
            kmz.Write(ms.ToArray(), 0, (int)ms.Length);
            zs.Close();
            ms.Close();
            kmz.Close();
        }
    }

    public class ProgressEventArgs : EventArgs
    {
        public ProgressEventArgs(string text) { Text = text; }
        public String Text { get; private set; }
    }

    public delegate void ProgressEventHandler(object sender, ProgressEventArgs e);
}
