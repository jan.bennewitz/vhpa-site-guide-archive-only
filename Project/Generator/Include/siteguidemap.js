// Written 2008, 2010 by Jan Bennewitz jan.bennewitz@gmail.com

/// <reference path="google-maps-3-vs-1-0.js" />

window.onresize = resize;

var map; // the google map
var oms; // the OverlappingMarkerSpiderfier

var types;
var shadowImage = new google.maps.MarkerImage('Images/mm_20_shadow.png', null, null, new google.maps.Point(6, 20));

// [i] (Array):
var markers = [];
// [name]:
var markerIndexByName = []; // numeric index of the named marker

var hoveringMarker = null; // index of hovered marker - null means none
var hoveringShape = null; // shape, if any, the pointer is over
var pinnedMarker = null; // index of pinned marker - null means none
var pinnedShape = null; // pinned shape, if any

var urlParams; // has .Center, .Zoom, .Span, .Pin, .Types, .Query
var geocodeSuccess = false; // indicates if q ('query') url param was successfully geocoded

var sitesListJustScrolled = false; // used to switch off the onmouseover event on LI items while the sites-list changes
var doubleClickDeadline; // milliseconds - used to check for double-clicks on sites-list LI elements
var doubleClickInterval = 250; // time in ms within which two clicks count as a double-click
var clickTimeoutId; // id of the click handler timeout - use a delay of doubleClickInterval to allow for possible double-clicks
var openSitePageCooldown = 0; // openSitePage is inactive for a cooldown time after firing to deal with different browsers firing click and doubleclick events differently
var loadingMarkerFiles; // Count of currently still loading marker data files
var aerodromes; // Holds the CAR166 aerodromes data (from json).

var maxZIndex = 1E9; // TODO:check if better solution available

var markersData = [];

var typesSortOrder = {
    school: 0,
    club: 1,
    open: 2,
    closed: 3,
    //microlight: 4,
}

var shapeStyle = {
    'Landing': { dimensions: 2, color: "#00ff00" },
    'No Landing': { dimensions: 2, color: "#ff0000" },
    'No Launching': { dimensions: 2, color: "#ff7f00" },
    'Emergency Landing': { dimensions: 2, color: "#ff7f00" },
    'No Fly Zone': { dimensions: 2, color: "#ff0000" },
    'Hazard': { dimensions: 2, color: "#ff0000" },
    'Feature': { dimensions: 2, color: "#0000ff" },
    'Access': { dimensions: 1, color: "#0000ff" },
    'Powerline': { dimensions: 1, color: "#ff0000" },
};
var userLocationMarker;
var userLocationControlImg;
var userLocationReceived = false;
var mapLockedToUserLocation = false;

function load() {
    types = {
        school: getStandardTypeFromColor("orange"),
        club: getStandardTypeFromColor("yellow"),
        open: getStandardTypeFromColor("green"),
        //types['microlight:  getStandardTypeFromColor("red"),
        closed: getStandardTypeFromColor("gray"),
        car166: {
            image: 'Images/OrangeDisk.png',
            inactiveImage: 'Images/FadedDisk.png',
            isHidden: true
        },
    };
    resize(); // set initial div sizes

    urlParams = getUrlParams();

    //set which types are shown
    var initialTypes;
    if (urlParams.Types == null)
        initialTypes = ['school', 'club', 'open', 'closed']; //default if unspecified
    else
        initialTypes = urlParams.Types;

    for (i = 0; i < initialTypes.length; i++) {
        if (types[initialTypes[i].toLowerCase()])
            types[initialTypes[i].toLowerCase()].isHidden = false;
    }

    map = new google.maps.Map(
        get$("map"),
        {
            zoom: 4,
            center: new google.maps.LatLng(-25, 136), // arbitrary default position to init map
            mapTypeId: urlParams.MapTypeId == null ? google.maps.MapTypeId.TERRAIN : urlParams.MapTypeId,
            disableDoubleClickZoom: true,
            scaleControl: true
        }
    );

    google.maps.visualRefresh = true; // TODO: remove this line once this becomes default.

    // Create the DIV to hold the userLocationControl and call the UserLocationControl()
    // constructor passing in this DIV.
    var userLocationControlDiv = document.createElement('div');
    UserLocationControl(userLocationControlDiv, map);

    userLocationControlDiv.index = 2;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(userLocationControlDiv);

    // Create the DIV to hold the linkControl and call the linkControl()
    // constructor passing in this DIV.
    var linkControlDiv = document.createElement('div');
    LinkControl(linkControlDiv, map);

    linkControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(linkControlDiv);

    oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, nearbyDistance: 14 });

    google.maps.event.addListener(map, 'click', clearPinned); // clicking anywhere on the map unpins the currently pinned marker
    google.maps.event.addListener(map, 'bounds_changed', mapMoved);
    google.maps.event.addListener(map, 'dragstart', disableUserLocationTracking);

    loadingMarkerFiles = 3;
    downloadUrl("schools.csv", function (request) { loadSchoolsData(request.responseText) }); // rewrite to https://spreadsheets.google.com/pub?hl=en&hl=en&key=0Arr1c-bJxr2idEh1Vld2OFRlT0lJYVh5bU54RS0tTlE&single=true&gid=0&output=csv
    downloadUrl("clubs.csv", function (request) { loadClubsData(request.responseText) }); // rewrite to https://spreadsheets.google.com/pub?hl=en&hl=en&key=0Arr1c-bJxr2idEh1Vld2OFRlT0lJYVh5bU54RS0tTlE&single=true&gid=1&output=csv
    downloadUrl("sitesForGMaps.xml", function (request) { loadSitesData(request.responseXML) });

    // set initial viewport if specified by url params (if not, we'll show all markers once they're loaded)
    if (urlParams.Center) {
        if (urlParams.Zoom) {
            map.setCenter(urlParams.Center);
            map.setZoom(urlParams.Zoom);
        } else if (urlParams.Span)
            setMapCenterAndSpan(urlParams.Center, urlParams.Span);
        else {
            map.setCenter(urlParams.Center);
            map.setZoom(8); // default if just center specified
        }
    }

    if (urlParams.Query) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': urlParams.Query, 'region': 'AU' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.fitBounds(results[0].geometry.viewport);
                geocodeSuccess = true;
            } else {
                alert("Could not geocode the location '" + urlParams.Query + "' for the following reason: " + status);
            }
        });
    }

    var typeToggles = get$('show-types').getElementsByTagName('li');
    for (i = 0; i < typeToggles.length; i++) {
        typeToggles[i].onclick = function () { toggleTypeVis(this.id) };
        typeToggles[i].onmouseover = function () { this.className = 'highlight' };
        typeToggles[i].onmouseout = function () { this.className = '' };
        setTypeHeaderImage(typeToggles[i].id);
    }

    showHideCAR166();
}

function unload() {
    //  saveState();
}

function UserLocationControl(controlDiv, map) {
    var controlUI = MapCustomControl('Show my position');
    controlDiv.appendChild(controlUI);

    controlUI.style.marginBottom = '10px';

    userLocationControlImg = document.createElement('img');
    userLocationControlImg.style.padding = '5px 5px 2px 5px';
    userLocationControlImg.EnabledImageSrc = 'Images/UserLocationButtonEnabled.png';
    userLocationControlImg.DisabledImageSrc = 'Images/UserLocationButtonDisabled.png';
    userLocationControlImg.src = userLocationControlImg.DisabledImageSrc;
    controlUI.appendChild(userLocationControlImg);

    controlUI.addEventListener('click', enableUserLocationTracking);
}

function LinkControl(controlDiv, map) {
    var controlUI = MapCustomControl('Link to this map');
    controlDiv.appendChild(controlUI);

    var linkControlImg = document.createElement('img');
    linkControlImg.style.padding = '5px 5px 2px 5px';
    linkControlImg.src = 'Images/links-16.png';
    controlUI.appendChild(linkControlImg);

    controlUI.addEventListener('click', showLinkPopup);
}

function MapCustomControl(title) {
    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '1px solid #fff';
    controlUI.style.marginRight = '10px';
    controlUI.style.borderRadius = '2px';
    controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px';
    controlUI.style.cursor = 'pointer';
    controlUI.title = title;
    return controlUI;
}

function userLocationUpdate(position) {
    userLocationMarker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));

    if (mapLockedToUserLocation)
        map.setCenter(userLocationMarker.position);
}

function enableUserLocationTracking() {
    if (userLocationReceived)
        enableUserLocationTrackingPositionKnown();
    else
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                userLocationReceived = true;

                if (!userLocationMarker) {// first call
                    userLocationMarker = new google.maps.Marker({
                        icon: {
                            url: 'Images/UserLocationMarker.png',
                            anchor: new google.maps.Point(12, 12)
                        },
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        map: map,
                        zIndex: maxZIndex + 1 // in front of any other marker
                    });
                    navigator.geolocation.watchPosition(userLocationUpdate);
                }

                enableUserLocationTrackingPositionKnown()
            }, function () {
                //handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            alert('Sorry, your browser does not support geolocation.');
        }
}

function enableUserLocationTrackingPositionKnown() {
    map.setCenter(userLocationMarker.position);
    map.setZoom(12);

    userLocationControlImg.src = userLocationControlImg.EnabledImageSrc;
    mapLockedToUserLocation = true;
}

function disableUserLocationTracking() {
    mapLockedToUserLocation = false;
    userLocationControlImg.src = userLocationControlImg.DisabledImageSrc;
}

function getStandardTypeFromColor(color) {
    return {
        image: 'Images/mm_20_' + color + '.png', // default icon image for this type of site
        highlightImage: 'Images/mm_20_' + color + '_hi.png', // highlighted icon image for this type of site
        fadeImage: 'Images/mm_20_' + color + '_fade.png', // icon image for this type of site when not visible on map
        inactiveImage: 'Images/mm_20_transparent_fade.png', // icon image for deselected marker type;
        isHidden: true // boolean - type is currently hidden
    };

};

function mousemove() {
    sitesListJustScrolled = false;
}

function loadSitesData(xml) {
    loadingMarkerFiles--;

    var markerNodes = xml.documentElement.getElementsByTagName("marker");
    for (var n = 0; n < markerNodes.length; n++) {
        var node = markerNodes[n];
        var site = node.getAttribute("site");

        markersData.push({
            lat: parseFloat(node.getAttribute("lat")),
            lng: parseFloat(node.getAttribute("lon")),
            name: node.getAttribute("name"),
            type: node.getAttribute("type"), // ['open', 'closed', 'microlight']
            href: (site != null && site != '') ? 'Sites/' + site + '.html' : null,
            description: node.firstChild.data
        });
    }

    var shapeNodes = xml.documentElement.getElementsByTagName("shape");
    for (n = 0; n < shapeNodes.length; n++) {
        createShapeFromNode(shapeNodes[n]);
    }

    markersLoaded();
}

function loadClubsData(csv) {
    loadingMarkerFiles--;

    var clubsDataRaw = CSVToArray(csv);

    for (var n = 0; n < clubsDataRaw.length; n++) {
        var thisClubData = clubsDataRaw[n];
        var club = {
            name: thisClubData[1],
            href: thisClubData[2],
            contactName: thisClubData[3],
            email: thisClubData[4],
            contactPhone: thisClubData[5],
            address: thisClubData[6],
            lat: parseFloat(thisClubData[7]),
            lng: parseFloat(thisClubData[8]),
            comments: thisClubData[9]
        };
        club.email = (club.email) ? "mailto:" + club.email : null;
        club.href = (club.href) ? "http://" + club.href : null;
        var description = club.contactName;
        if (club.email)
            description = "<a href='" + club.email + "'>" + description + "</a>";
        description = "<p>" + description + " " + club.contactPhone + "</p><p>" + club.address + "</p>";
        if (club.comments)
            description += "<p>" + club.comments + "</p>";

        markersData.push({
            lat: club.lat,
            lng: club.lng,
            name: club.name,
            type: 'club',
            href: club.href,
            description: description
        });
    }

    markersLoaded();
};

function loadSchoolsData(csv) {
    loadingMarkerFiles--;

    var schoolsDataRaw = CSVToArray(csv);

    for (var n = 0; n < schoolsDataRaw.length; n++) {
        var thisSchoolData = schoolsDataRaw[n];
        thisSchoolData.length = 15; // pad empty trailing fields
        var school = {
            name: thisSchoolData[1],
            href: thisSchoolData[2],
            instructorType: thisSchoolData[3],
            instructor: thisSchoolData[4],
            phone: thisSchoolData[5],
            email: thisSchoolData[6],
            address: thisSchoolData[7],
            lat: parseFloat(thisSchoolData[13]),
            lng: parseFloat(thisSchoolData[14]),
            isHG: (thisSchoolData[8] || "") != "",
            isHGM: (thisSchoolData[9] || "") != "",
            isPG: (thisSchoolData[10] || "") != "",
            isPGM: (thisSchoolData[11] || "") != "",
            isWM: (thisSchoolData[12] || "") != ""
        };
        school.email = (school.email) ? "mailto:" + school.email : null;
        school.href = (school.href) ? "http://" + school.href : null;
        var description = school.instructor;
        if (school.email)
            description = school.instructorType + ": <a href='" + school.email + "'>" + description + "</a>";
        var types = "";
        if (school.isHG) types += "<li>Hang Gliding</li>";
        if (school.isHGM) types += "<li>Motorised Hang Gliding</li>";
        if (school.isPG) types += "<li>Paragliding</li>";
        if (school.isPGM) types += "<li>Paramotoring</li>";
        if (school.isWM) types += "<li>Weightshift Microlighting</li>";
        description = "<ul>" + types + "</ul><p>" + description + " " + school.phone + "</p><p>" + school.address + "</p>";
        if (school.comments)
            description += "<p>" + school.comments + "</p>";

        markersData.push({
            lat: school.lat,
            lng: school.lng,
            name: school.name,
            type: 'school',
            href: school.href,
            description: description
        });
    }

    markersLoaded();
};

function markersLoaded() {
    if (loadingMarkerFiles) return; //still loading more data

    markersData.sort(function (a, b) { // sort by type, then by name
        if (typesSortOrder[a.type] < typesSortOrder[b.type]) return -1
        if (typesSortOrder[a.type] > typesSortOrder[b.type]) return 1

        var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
        if (nameA < nameB) return -1
        if (nameA > nameB) return 1
        return 0
    });

    for (var n = 0; n < markersData.length; n++)
        createMarker(markersData[n]);

    markersData = null;

    var initialLaunchIndex = markerIndexByName[urlParams.Pin];

    if (urlParams.Center || geocodeSuccess) {
        // the viewport has already been specified. If a launch has been specified, just pin it without panning
        if (initialLaunchIndex)
            setPin(initialLaunchIndex, false, true);
    }
    else if (initialLaunchIndex) {
        // center the map on the specified launch
        if (urlParams.Zoom)
            map.setZoom(urlParams.Zoom);
        else if (urlParams.Span)
            setMapCenterAndSpan(markers[initialLaunchIndex].Marker.position, urlParams.Span);
        else
            map.setZoom(12); // default zoom level for showing a launch

        setPin(initialLaunchIndex, true, true);
    }
    else {
        // there is no initial launch and no center, so we'll ignore any span or zoom level and just show all non-hidden markers
        var bounds = new google.maps.LatLngBounds;
        for (var i = 0; i < markers.length; i++) {
            if (!types[markers[i].Type].isHidden)
                bounds.extend(markers[i].Marker.position);
        }
        if (!bounds.isEmpty()) map.fitBounds(bounds);
    }

    $('#loading').hide();
    setInfoPanel();
    setLaunchVisibilities();
}

function setInfoPanel() {
    var detail = '';
    if (hoveringMarker != null) {
        detail = markers[hoveringMarker].Description;
    }
    else if (hoveringShape) {
        detail = hoveringShape.description;
    }
    else if (pinnedMarker != null) {
        detail = markers[pinnedMarker].Description;
    }
    else if (pinnedShape != null) {
        detail = pinnedShape.description;
    }

    $('#legend').toggle(detail == '');
    $('#site-detail').toggle(detail != '');
    $('#site-detail').html(detail);
}

function createShapeFromNode(node) {
    var shape;
    var category = node.getAttribute('category');
    var descriptionNodes = node.getElementsByTagName('desc');
    var shapeDescription;
    if (descriptionNodes.length != 0)
        shapeDescription = node.getElementsByTagName('desc')[0].firstChild.data;
    else
        shapeDescription = category;
    var coordSets = node.getElementsByTagName('coords');
    var polys = [];
    var shapeDim = shapeStyle[category].dimensions;
    var shapeColor = shapeStyle[category].color;

    var setDescriptionAndEventHandlers = function (shape) {
        shape.description = shapeDescription;
        google.maps.event.addListener(shape, 'mouseover', function () { hoveringShape = this; setInfoPanel() });
        google.maps.event.addListener(shape, 'mouseout', function () { hoveringShape = null; setInfoPanel() });
        google.maps.event.addListener(shape, 'click', function () { pinnedShape = this; pinnedMarker = null; setInfoPanel() });
    }

    if (shapeDim == 2) {
        for (var s = 0; s < coordSets.length; s++) {
            polys[s] = decodeLine(coordSets[s].firstChild.data);
        }

        shape = new google.maps.Polygon({
            map: map,
            paths: polys,
            fillColor: shapeColor,
            fillOpacity: .2,
            strokeColor: shapeColor,
            strokeOpacity: .4,
            strokeWeight: 1
        });

        setDescriptionAndEventHandlers(shape);
    }
    else if (shapeDim == 1) {
        for (var s = 0; s < coordSets.length; s++) {
            shape = new google.maps.Polyline({
                map: map,
                path: decodeLine(coordSets[s].firstChild.data),
                strokeColor: shapeColor,
                strokeOpacity: .6,
                strokeWeight: 1
            });

            setDescriptionAndEventHandlers(shape);
        }
    }

    return shape;
}

function createMarker(markerData) { //lat, lng, name, type, href, description
    var index = markers.length;
    var marker = new google.maps.Marker({
        icon: types[markerData.type].image,
        shadow: shadowImage,
        position: new google.maps.LatLng(markerData.lat, markerData.lng),
        map: map
    });
    marker.index = index;

    google.maps.event.addListener(marker, 'mouseover', function () { hoveringMarker = index; markerHighlight(index, true); });
    google.maps.event.addListener(marker, 'mouseout', function () { hoveringMarker = null; markerDeHighlight(index); });
    //google.maps.event.addListener(marker, 'click', function () { setPin(index, false, true) });
    oms.addListener('click', function (marker) { setPin(marker.index, false, true) });
    oms.addMarker(marker);
    if (markerData.href) google.maps.event.addListener(marker, 'dblclick', function () { openSitePage(); });

    var li = document.createElement("li"); // <li> element for this marker on the markers list
    li.appendChild(document.createTextNode(markerData.name));
    li.style.backgroundImage = 'url(' + types[markerData.type].image + ')';
    var liOutOfRange = li.cloneNode(true);
    liOutOfRange.style.backgroundImage = 'url(' + types[markerData.type].fadeImage + ')';

    li.onmouseover = function () {
        if (sitesListJustScrolled)
            sitesListJustScrolled = false;
        else {
            hoveringMarker = index;
            markerHighlight(index, false)
        }
    };
    li.onmouseout = function () { markerDeHighlight(index) };
    li.onclick = function () { listClick(index) };
    liOutOfRange.onmouseover = li.onmouseover; // <li> element for this marker on the out-of-range list
    liOutOfRange.onmouseout = li.onmouseout;
    liOutOfRange.onclick = li.onclick;
    if (markerData.href != null) {
        li.ondblclick = function () { openSitePage() };
        liOutOfRange.ondblclick = li.ondblclick;
    };

    $('#sites-list').append(li);
    $('#out-of-range').append(liOutOfRange);

    markerIndexByName[markerData.name] = index;

    launchDescription = "<h3>" + markerData.name + "</h3>" + markerData.description
    if (markerData.href) {
        if (markerData.type == 'school' || markerData.type == 'club')
            launchDescription += "<p>See the " + markerData.type + "'s <a href='" + markerData.href + "'>home page</a> for full details.</p>";
        else
            launchDescription += "<p>See <a href='" + markerData.href + "'>site page</a> for full details.</p>";

        launchDescription += "<p><a href='https://www.google.com/maps/dir//" + markerData.lat + "," + markerData.lng + "'><img src='Images/maps_14dp.png'/> Navigate to here.</a></p>";
    }


    markers.push({
        Marker: marker,
        Type: markerData.type,
        Name: markerData.name,
        Description: launchDescription,
        Href: markerData.href,
        ListItem: li,
        ListItemOutOfRange: liOutOfRange
    });
}

function listClick(index) {
    if (new Date().getTime() < doubleClickDeadline) { //double-click
        clearTimeout(clickTimeoutId); setPin(index, true, true); // handle first click immediately
        openSitePage();
    } else { //single-click or first click of a double-click
        setPin(index, false, false);
        clickTimeoutId = setTimeout('setPin("' + index + '", true, true)', doubleClickInterval);
        doubleClickDeadline = new Date().getTime() + doubleClickInterval;
    }
}

function mapMoved() {
    if (markers != null)
        setLaunchVisibilities();
    if (pinnedMarker != null)
        scrollIntoViewIfHidden(pinnedMarker);
    else
        get$('sites-list-wrapper2').scrollTop = 0;
}

function resize() {
    var mapCenter;
    if (map != null) {
        mapCenter = map.getCenter();
    }

    //Width first, since width can influence height but not vice-versa
    var newWidth =
        document.body.clientWidth
        - get$('sites-list-wrapper').offsetWidth
        - get$('site-detail-wrapper').offsetWidth
        + 'px';

    get$('wrapper-menu-top').style.width = newWidth;
    get$('menu-top').style.width = newWidth;
    get$('wrapper-content').style.width = newWidth;
    get$('header').style.width = newWidth;

    var contentYOffset =
        get$('header').offsetHeight
        + get$('wrapper-menu-top').offsetHeight

    get$('site-detail-wrapper').style.top = contentYOffset;
    get$('sites-list-wrapper').style.top = contentYOffset;

    var totalHeight = document.body.parentNode.clientHeight;
    if (totalHeight == 0) {
        // IE6 or earlier
        totalHeight = document.body.clientHeight;
    }
    var contentHeight =
        totalHeight
        - contentYOffset
        - 1
        + 'px';
    get$('sites-map').style.height = contentHeight;
    get$('sites-list-wrapper').style.height = contentHeight;
    get$('site-detail-wrapper').style.height = contentHeight;
    get$('sites-list-wrapper2').style.height =
        totalHeight
        - contentYOffset
        - get$('show-types').offsetHeight
        - 1
        + 'px';

    if (mapCenter != null) {
        map.setCenter(mapCenter);
    }

}

function markerHighlight(index, scroll) {
    markers[index].ListItem.className = 'highlight';
    markers[index].ListItemOutOfRange.className = 'highlight';
    if (scroll)
        scrollIntoViewIfHidden(index);
    //  hoveringMarker = index;
    setInfoPanel();
    var highlightImage = types[markers[index].Type].highlightImage;
    markers[index].Marker.setIcon(highlightImage);
    markers[index].Marker.setZIndex(maxZIndex); // bring to front
    markers[index].ListItem.style.backgroundImage = 'url(' + highlightImage + ')';
    markers[index].ListItemOutOfRange.style.backgroundImage = 'url(' + highlightImage + ')';
}

function markerDeHighlight(index) {
    if (hoveringMarker == index)
        hoveringMarker = null;

    if (pinnedMarker != index) {
        if (pinnedMarker != null)
            markerHighlight(pinnedMarker, false);
        else if (hoveringMarker != null)
            markerHighlight(hoveringMarker, false);
        else
            setInfoPanel(); // to idle

        var marker = markers[index];
        marker.ListItem.className = '';
        marker.ListItemOutOfRange.className = '';
        var idleImage = types[marker.Type].image;
        marker.Marker.setIcon(idleImage);
        marker.Marker.setZIndex(undefined);
        marker.ListItem.style.backgroundImage = 'url(' + idleImage + ')';
        marker.ListItemOutOfRange.style.backgroundImage = 'url(' + types[marker.Type].fadeImage + ')';
    }
}

function clearPinned() {
    if (pinnedShape != null) {
        pinnedShape = null;
        setInfoPanel();
    }
    if (pinnedMarker != null) {
        var oldPinned = pinnedMarker;
        pinnedMarker = null;
        markerDeHighlight(oldPinned);
    }
}

function setPin(index, pan, scroll) {
    sitesListJustScrolled = false;
    if (pinnedMarker != null && pinnedMarker != index)
        clearPinned();
    pinnedShape = null;
    pinnedMarker = index;
    markerHighlight(index, false);
    if (pan) {
        sitesListJustScrolled = true;
        map.panTo(markers[index].Marker.position);
        setLaunchVisibilities();
    }
    if (scroll)
        scrollIntoViewIfHidden(index);
}

function scrollIntoViewIfHidden(index) {
    container = get$('sites-list-wrapper2');
    var visTop = container.scrollTop;
    var visBottom = visTop + container.offsetHeight;
    var li;
    if (markers[index].ListItem.style.display != 'none')
        li = markers[index].ListItem;
    else
        li = markers[index].ListItemOutOfRange;
    var top = li.offsetTop;
    var bottom = top + li.offsetHeight;
    if (visTop > top || visBottom < bottom)
        container.scrollTop = (top + bottom - container.offsetHeight) / 2;
}

function openSitePage() {
    //      var body = document.getElementsByTagName("body")[0];
    //      window.getSelection().collapse(body);                 // removed, since doesn't work in ie

    if (markers[pinnedMarker].Href != null && new Date().getTime() > openSitePageCooldown) {
        openSitePageCooldown = new Date().getTime() + 2 * doubleClickInterval; // it's safe to be generous with cooldown time
        window.open(markers[pinnedMarker].Href, '_blank'); // new window
        //    window.location = markers[pinnedMarker].Href; // same window
    }
}

function toggleTypeVis(type) {
    oms.unspiderfy();

    types[type].isHidden = !types[type].isHidden;

    if (types[type].isHidden) {
        if (pinnedMarker != null)
            if (markers[pinnedMarker].Type == type)
                clearPinned();
        if (hoveringMarker != null)
            if (markers[hoveringMarker].Type == type)
                markerDeHighlight(hoveringMarker);
    }

    setTypeHeaderImage(type);
    if (type == 'car166')
        showHideCAR166();
    else
        setLaunchVisibilities();
}

function setTypeHeaderImage(type) {
    if (types[type].isHidden)
        get$(type).style.backgroundImage = 'url(' + types[type].inactiveImage + ')'
    else
        get$(type).style.backgroundImage = 'url(' + types[type].image + ')';
}

function setLaunchVisibilities() {
    var visBounds = map.getBounds();
    if (!visBounds) return;

    var anyVisLaunches = false;
    var anyOutOfRangeLaunches = false;

    for (i = 0; i < markers.length; i++) {
        var marker = markers[i];

        if (types[marker.Type].isHidden) {
            marker.ListItem.style.display = 'none';
            marker.ListItemOutOfRange.style.display = 'none';
            marker.Marker.setVisible(false);
        } else {
            if (visBounds.contains(marker.Marker.getPosition())) {
                marker.ListItem.style.display = 'block';
                marker.ListItemOutOfRange.style.display = 'none';
                anyVisLaunches = true;
            } else {
                marker.ListItem.style.display = 'none';
                marker.ListItemOutOfRange.style.display = 'block';
                anyOutOfRangeLaunches = true;
            }
            marker.Marker.setVisible(true);
        }
    }

    sitesListJustScrolled = true;

    if (anyVisLaunches && anyOutOfRangeLaunches)
        get$('sites-list').style.borderBottom = 'solid 1px #666';
    else
        get$('sites-list').style.borderBottom = 'none';
}

function showHideCAR166() {
    if (types["car166"].isHidden) {
        if (aerodromes != undefined)
            for (var n = 0; n < aerodromes.length; n++) {
                aerodrome = aerodromes[n];
                aerodrome.circle.setMap(null);
                aerodrome.circle = null;
                aerodrome.Label.setMap(null);
                aerodrome.Label = null;
            }
    }
    else {
        if (aerodromes == undefined)
            downloadUrl("VHFAerodromes.json", function (request) { loadCAR166(request.responseText) });
        else
            showCAR166();
    }
}

function loadCAR166(json) {
    aerodromes = eval("(" + json + ")");

    var aerodrome;
    for (var n = 0; n < aerodromes.length; n++) {
        aerodrome = aerodromes[n];
        aerodrome.position = new google.maps.LatLng(aerodrome.Lat, aerodrome.Lng);
    }
    showHideCAR166();
}

function showCAR166() {
    var aerodrome;

    for (var n = 0; n < aerodromes.length; n++) {
        aerodrome = aerodromes[n];

        aerodrome.circle = new google.maps.Circle({
            center: aerodrome.position,
            fillColor: "#E56717",
            fillOpacity: .3,
            strokeWeight: 0.01,
            radius: 18520, // 10 nautical miles
            map: map
        });
        aerodrome.circle.aerodrome = aerodrome;
        aerodrome.Label = new LabelOverlay(aerodrome);

        google.maps.event.addListener(aerodrome.circle, 'mouseover', function () {
            highlightAerodrome(this.aerodrome, true)
        });
        google.maps.event.addListener(aerodrome.circle, 'mouseout', function () {
            highlightAerodrome(this.aerodrome, false)
        });
        google.maps.event.addListener(aerodrome.circle, 'dblclick', function () {
            window.open("http://www.airservicesaustralia.com/aip/current/ersa/FAC_" + this.aerodrome.Code + "_09-Nov-2017.pdf", '_blank'); // new window
        });
    }
}

function highlightAerodrome(aerodrome, highlight) {
    aerodrome.circle.setOptions({
        strokeWeight: (highlight) ? 1 : 0.01
    });
    aerodrome.Label.div_.style.fontWeight = (highlight) ? 'bold' : '';
    aerodrome.Label.positionDiv();
}

LabelOverlay.prototype = new google.maps.OverlayView();
function LabelOverlay(aerodrome) {
    if (!(this instanceof arguments.callee))
        throw new Error("Constructor called as a function.");

    this.aerodrome = aerodrome;
    this.setMap(map);
}

LabelOverlay.prototype.onAdd = function () {
    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.textAlign = "center";
    div.appendChild(document.createTextNode(this.aerodrome.Name + " " + this.aerodrome.CTAF));

    this.div_ = div;

    this.getPanes().overlayLayer.appendChild(div);
}

LabelOverlay.prototype.draw = function () {
    this.div_.style.fontSize = Math.pow(1.25, map.getZoom() + 4) + 'px';
    this.positionDiv();
}

LabelOverlay.prototype.positionDiv = function () {
    var point = this.getProjection().fromLatLngToDivPixel(this.aerodrome.position);
    var div = this.div_;
    div.style.left = point.x - div.clientWidth / 2 + 'px';
    div.style.top = point.y - div.clientHeight / 2 + 'px';
}

LabelOverlay.prototype.onRemove = function () {
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
}

// ---- utility functions ----

function getUrlParams() {
    // url parameters, first three are equivalent to Google Maps params:
    var llParam = urlParam('ll'); // lat, lon
    var zParam = urlParam('z');  // zoom
    var spnParam = urlParam('spn'); // span - lat, lon
    var pinParam = urlParam('pin'); // pinned launch name
    var typeParam = urlParam('type'); // shown types (e.g. 'open,closed')
    var qParam = urlParam('q'); // query - name of area to be shown via geocoding
    var mapTypeParam = urlParam('mapType'); // map type - roadmap, satellite, hybrid or terrain

    var center;
    if (llParam) {
        var coords = llParam.split(",");
        center = new google.maps.LatLng(coords[0], coords[1]);
    };

    var zoom;
    if (zParam) {
        zoom = parseFloat(zParam);
        if (isNaN(zoom)) zoom = null;
    }

    var span;
    if (spnParam) {
        coords = spnParam.split(",");
        span = new google.maps.LatLng(coords[0], coords[1]);
    };

    var types;
    if (typeParam != null) {
        if (typeParam == '')
            types = [];
        else
            types = typeParam.split(",");
    }
    var mapTypeId = null;
    if (mapTypeParam == null)
        mapTypeId = null;
    else
        switch (mapTypeParam.substring(0, 1)) {
            case 'r': mapTypeId = google.maps.MapTypeId.ROADMAP; break;
            case 's': mapTypeId = google.maps.MapTypeId.SATELLITE; break;
            case 'h': mapTypeId = google.maps.MapTypeId.HYBRID; break;
            case 't': mapTypeId = google.maps.MapTypeId.TERRAIN; break;
            default: null;
        }

    return {
        "Center": center,
        "Zoom": zoom,
        "Span": span,
        "Query": qParam,
        "Pin": pinParam,
        "Types": types,
        "MapTypeId": mapTypeId,
    }
}

function urlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return null;
    else
        return decodeURI(results[1]);
}

function setMapCenterAndSpan(center, span) {
    map.fitBounds(new google.maps.LatLngBounds(
        new google.maps.LatLng(center.lat() - span.lat() / 2, center.lng() - span.lng() / 2),
        new google.maps.LatLng(center.lat() + span.lat() / 2, center.lng() + span.lng() / 2)
    ));
}

//function saveState() {
//  window.location.hash = getState();
//}

//function getState() {
//  var center = map.getCenter();
//  return("ll=" + center.lat() + "," + center.lng());
//}

//---------------------------------------------------------------
/**
* This functions wraps XMLHttpRequest open/send function.
* It lets you specify a URL and will call the callback if
* it gets a status code of 200.
* @param {String} url The URL to retrieve
* @param {Function} callback The function to call once retrieved.
*/
function downloadUrl(url, callback) {
    var status = -1;
    var request = createXmlHttpRequest();
    if (!request) {
        return false;
    }

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            try {
                status = request.status;
            } catch (e) {
                // Usually indicates request timed out in FF.
            }
            if (status == 200 || status == 0) { //TODO - should be 200, getting 0
                callback(request);
                request.onreadystatechange = function () { };
            }
        }
    }
    request.open('GET', url, true);
    try {
        request.send(null);
    } catch (e) {
        changeStatus(e);
    }
};

/**
* Returns an XMLHttp instance to use for asynchronous
* downloading. This method will never throw an exception, but will
* return NULL if the browser does not support XmlHttp for any reason.
* @return {XMLHttpRequest|Null}
*/
function createXmlHttpRequest() {
    try {
        if (typeof ActiveXObject != 'undefined') {
            return new ActiveXObject('Microsoft.XMLHTTP');
        } else if (window["XMLHttpRequest"]) {
            return new XMLHttpRequest();
        }
    } catch (e) {
        changeStatus(e);
    }
    return null;
};

// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray(strData, strDelimiter) {
    strDelimiter = (strDelimiter || ",");

    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
    );

    var arrData = [[]];
    var arrMatches = null;

    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
            strMatchedDelimiter.length &&
            (strMatchedDelimiter != strDelimiter)
        ) {
            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);
        }

        if (arrMatches[2]) {
            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            var strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"),
                "\""
            );
        } else {
            // We found a non-quoted value.
            var strMatchedValue = arrMatches[3];
        }

        // Add value string to the data array.
        arrData[arrData.length - 1].push(strMatchedValue);
    }

    //discard header row
    arrData.splice(0, 1);
    return (arrData);
};

// Decode an encoded polyline string into an array of coordinates.
// Adapted from http://code.google.com/apis/maps/documentation/utilities/include/polyline.js
function decodeLine(encoded) {
    var len = encoded.length;
    var index = 0;
    var array = [];
    var lat = 0;
    var lng = 0;

    while (index < len) {
        var b;
        var shift = 0;
        var result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;

        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;

        array.push(new google.maps.LatLng(lat * 1e-5, lng * 1e-5));
    }

    return array;
}

function get$(ID) { // "Poor man's JQuery". Thanks Leon http://secretgeek.net/higgins/slides_alt_net.html#44
    return document.getElementById(ID);
}

$(function () { // Modal popup - thanks to http://www.jqueryscript.net/lightbox/Super-Simple-Modal-Popups-with-jQuery-CSS3-Transitions.html
    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();
        showLinkPopup();
    });

    $(window).resize(function () {
        $(".modal-box").css({
            top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });

    $(window).resize();

});

function showLinkPopup() {
    $("body").append("<div class='modal-overlay js-modal-close'></div>");
    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(250, function () {
            $(".modal-overlay").remove();
        });
    });

    $(".modal-overlay").fadeTo(250, 0.7);
    //$(".js-modalbox").fadeIn(250);
    $('#popup').fadeIn($(this).data());
    updateLinksToThisMap();
    $('#linkByArea').select();
};

function updateLinksToThisMap() {
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var spnFactor = .8;
    var spnLat = spnFactor * (ne.lat() - sw.lat());
    var spnLng = spnFactor * (ne.lng() - sw.lng());
    var llScale = -Math.max(orderOfNumber(spnLat), orderOfNumber(spnLng)) + 3; // Four significant digits
    var spn = roundNumber(spnLat, llScale) + ',' + roundNumber(spnLng, llScale);
    var center = map.getCenter();
    var ll = roundNumber(center.lat(), llScale) + ',' + roundNumber(center.lng(), llScale);
    //var z = map.getZoom();
    //var pin = // handled below
    var showingTypes = [];
    for (var type in types)
        if (!types[type].isHidden)
            showingTypes.push(type);
    var type = showingTypes.join(',');

    var mapTypeId = map.getMapTypeId();

    var url = window.location.origin + window.location.pathname + '?mapType=' + mapTypeId + '&type=' + type + (pinnedMarker == null ? '' : ('&pin=' + encodeURIComponent(markers[pinnedMarker].Name))) + '&ll=' + ll + '&spn=' + spn;
    $('#linkByArea').val(url);
    $('#testLink').attr('href', url);
}

function roundNumber(num, scale) { // http://stackoverflow.com/a/12830454
    if (!("" + num).includes("e")) {
        return +(Math.round(num + "e" + scale) + "e" + -scale);
    } else {
        var arr = ("" + num).split("e");
        var sig = ""
        if (+arr[1] + scale > 0) {
            sig = "+";
        }
        return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e" + -scale);
    }
}

function orderOfNumber(num) {
    return Math.floor(Math.log(num) / Math.LN10);
}

(function () {/*
 OverlappingMarkerSpiderfier
https://github.com/jawj/OverlappingMarkerSpiderfier
Copyright (c) 2011 - 2012 George MacKerron
Released under the MIT licence: http://opensource.org/licenses/mit-license
Note: The Google Maps API v3 must be included *before* this code
*/
    var h = null;
    (function () {
        var s, t = {}.hasOwnProperty, u = [].slice; if (((s = this.google) != h ? s.maps : void 0) != h) this.OverlappingMarkerSpiderfier = function () {
            function o(b, d) { var a, g, e, f, c = this; this.map = b; d == h && (d = {}); for (a in d) t.call(d, a) && (g = d[a], this[a] = g); this.m = new this.constructor.c(this.map); this.i(); this.b = {}; f = ["click", "zoom_changed", "maptypeid_changed"]; g = 0; for (e = f.length; g < e; g++) a = f[g], l.addListener(this.map, a, function () { return c.unspiderfy() }) } var l, p, n, q, k, c, r; c = o.prototype; c.VERSION = "0.2.6"; p = google.maps; l =
                p.event; k = p.MapTypeId; r = 2 * Math.PI; c.keepSpiderfied = !1; c.markersWontHide = !1; c.markersWontMove = !1; c.nearbyDistance = 20; c.circleSpiralSwitchover = 9; c.circleFootSeparation = 23; c.circleStartAngle = r / 12; c.spiralFootSeparation = 26; c.spiralLengthStart = 11; c.spiralLengthFactor = 4; c.spiderfiedZIndex = 1E3; c.usualLegZIndex = 10; c.highlightedLegZIndex = 20; c.legWeight = 1.5; c.legColors = { usual: {}, highlighted: {} }; q = c.legColors.usual; n = c.legColors.highlighted; q[k.HYBRID] = q[k.SATELLITE] = "#fff"; n[k.HYBRID] = n[k.SATELLITE] = "#f00";
            q[k.TERRAIN] = q[k.ROADMAP] = "#444"; n[k.TERRAIN] = n[k.ROADMAP] = "#f00"; c.i = function () { this.a = []; this.f = [] }; c.addMarker = function (b) { var d, a = this; if (b._oms != h) return this; b._oms = !0; d = [l.addListener(b, "click", function () { return a.B(b) })]; this.markersWontHide || d.push(l.addListener(b, "visible_changed", function () { return a.k(b, !1) })); this.markersWontMove || d.push(l.addListener(b, "position_changed", function () { return a.k(b, !0) })); this.f.push(d); this.a.push(b); return this }; c.k = function (b, d) {
                if (b._omsData != h && (d ||
                    !b.getVisible()) && !(this.p != h || this.q != h)) return this.F(d ? b : h)
            }; c.getMarkers = function () { return this.a.slice(0) }; c.removeMarker = function (b) { var d, a, g, e, f; b._omsData != h && this.unspiderfy(); d = this.h(this.a, b); if (0 > d) return this; g = this.f.splice(d, 1)[0]; e = 0; for (f = g.length; e < f; e++) a = g[e], l.removeListener(a); delete b._oms; this.a.splice(d, 1); return this }; c.clearMarkers = function () {
                var b, d, a, g, e, f, c, i; this.unspiderfy(); i = this.a; b = g = 0; for (f = i.length; g < f; b = ++g) {
                    a = i[b]; d = this.f[b]; e = 0; for (c = d.length; e < c; e++) b =
                        d[e], l.removeListener(b); delete a._oms
                } this.i(); return this
            }; c.addListener = function (b, d) { var a, g; ((g = (a = this.b)[b]) != h ? g : a[b] = []).push(d); return this }; c.removeListener = function (b, d) { var a; a = this.h(this.b[b], d); 0 > a || this.b[b].splice(a, 1); return this }; c.clearListeners = function (b) { this.b[b] = []; return this }; c.trigger = function () { var b, d, a, g, e, f; d = arguments[0]; b = 2 <= arguments.length ? u.call(arguments, 1) : []; d = (a = this.b[d]) != h ? a : []; f = []; g = 0; for (e = d.length; g < e; g++) a = d[g], f.push(a.apply(h, b)); return f }; c.r = function (b,
                d) { var a, g, e, f, c; e = this.circleFootSeparation * (2 + b) / r; g = r / b; c = []; for (a = f = 0; 0 <= b ? f < b : f > b; a = 0 <= b ? ++f : --f) a = this.circleStartAngle + a * g, c.push(new p.Point(d.x + e * Math.cos(a), d.y + e * Math.sin(a))); return c }; c.s = function (b, d) { var a, g, e, c, j; e = this.spiralLengthStart; a = 0; j = []; for (g = c = 0; 0 <= b ? c < b : c > b; g = 0 <= b ? ++c : --c) a += this.spiralFootSeparation / e + 5.0E-4 * g, g = new p.Point(d.x + e * Math.cos(a), d.y + e * Math.sin(a)), e += r * this.spiralLengthFactor / a, j.push(g); return j }; c.B = function (b) {
                    var d, a, g, c, f, j, i, m, l; d = b._omsData != h; (!d ||
                        !this.keepSpiderfied) && this.unspiderfy(); if (d || this.map.getStreetView().getVisible()) return this.trigger("click", b); c = []; f = []; j = this.nearbyDistance * this.nearbyDistance; g = this.j(b.position); l = this.a; i = 0; for (m = l.length; i < m; i++) d = l[i], d.getVisible() && d.map != h && (a = this.j(d.position), this.n(a, g) < j ? c.push({ v: d, l: a }) : f.push(d)); return 1 === c.length ? this.trigger("click", b) : this.C(c, f)
                }; c.u = function (b) {
                    var d = this; return {
                        d: function () {
                            return b._omsData.e.setOptions({
                                strokeColor: d.legColors.highlighted[d.map.mapTypeId],
                                zIndex: d.highlightedLegZIndex
                            })
                        }, g: function () { return b._omsData.e.setOptions({ strokeColor: d.legColors.usual[d.map.mapTypeId], zIndex: d.usualLegZIndex }) }
                    }
                }; c.C = function (b, d) {
                    var a, c, e, f, j, i, m, k, o, n; this.p = !0; n = b.length; a = this.z(function () { var a, d, c; c = []; a = 0; for (d = b.length; a < d; a++) k = b[a], c.push(k.l); return c }()); f = n >= this.circleSpiralSwitchover ? this.s(n, a).reverse() : this.r(n, a); a = function () {
                        var a, d, k, n = this; k = []; a = 0; for (d = f.length; a < d; a++) e = f[a], c = this.A(e), o = this.w(b, function (a) { return n.n(a.l, e) }),
                            m = o.v, i = new p.Polyline({ map: this.map, path: [m.position, c], strokeColor: this.legColors.usual[this.map.mapTypeId], strokeWeight: this.legWeight, zIndex: this.usualLegZIndex }), m._omsData = { D: m.position, e: i }, this.legColors.highlighted[this.map.mapTypeId] !== this.legColors.usual[this.map.mapTypeId] && (j = this.u(m), m._omsData.t = { d: l.addListener(m, "mouseover", j.d), g: l.addListener(m, "mouseout", j.g) }), m.setPosition(c), m.setZIndex(Math.round(this.spiderfiedZIndex + e.y)), k.push(m); return k
                    }.call(this); delete this.p; this.o =
                        !0; return this.trigger("spiderfy", a, d)
                }; c.unspiderfy = function (b) { var d, a, c, e, f, j, i; b == h && (b = h); if (this.o == h) return this; this.q = !0; e = []; c = []; i = this.a; f = 0; for (j = i.length; f < j; f++) a = i[f], a._omsData != h ? (a._omsData.e.setMap(h), a !== b && a.setPosition(a._omsData.D), a.setZIndex(h), d = a._omsData.t, d != h && (l.removeListener(d.d), l.removeListener(d.g)), delete a._omsData, e.push(a)) : c.push(a); delete this.q; delete this.o; this.trigger("unspiderfy", e, c); return this }; c.n = function (b, d) {
                    var a, c; a = b.x - d.x; c = b.y - d.y; return a *
                        a + c * c
                }; c.z = function (b) { var d, a, c, e, f; e = a = c = 0; for (f = b.length; e < f; e++) d = b[e], a += d.x, c += d.y; b = b.length; return new p.Point(a / b, c / b) }; c.j = function (b) { return this.m.getProjection().fromLatLngToDivPixel(b) }; c.A = function (b) { return this.m.getProjection().fromDivPixelToLatLng(b) }; c.w = function (b, c) { var a, g, e, f, j, i; e = j = 0; for (i = b.length; j < i; e = ++j) if (f = b[e], f = c(f), !("undefined" !== typeof a && a !== h) || f < g) g = f, a = e; return b.splice(a, 1)[0] }; c.h = function (b, c) {
                    var a, g, e, f; if (b.indexOf != h) return b.indexOf(c); a = e = 0; for (f =
                        b.length; e < f; a = ++e) if (g = b[a], g === c) return a; return -1
                }; o.c = function (b) { return this.setMap(b) }; o.c.prototype = new p.OverlayView; o.c.prototype.draw = function () { }; return o
        }()
    }).call(this);
}).call(this);
/* Thu 17 May 2012 12:12:01 BST */