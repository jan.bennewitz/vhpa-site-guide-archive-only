window.onload = function () {
    document.getElementById('searchBar').style.display = '';
    ConvertRowsToLinks("index");
    ConvertRowsToLinks("sites");
    IdentifySites();
    if (!window.location.hash)
        document.getElementById('searchstring').focus();
    if (document.getElementById('searchstring').value == '') {
        document.getElementById('searchstring').value = unescape(urlParam('q')).replace(/\+/g, " ");
        fromUrl = true;
    }
    Filter(document.getElementById('searchstring').value);
    if (fromUrl && matchCount == 1)
        document.location.href = lastMatchRow.getElementsByTagName("a")[0].href;
}

var innerTextSupported = (document.getElementsByTagName("body")[0].innerText != undefined) ? true : false;
var areaRows = new Array();
var matchCount;
var lastMatchRow;
var fromUrl = false;

function ConvertRowsToLinks(xTableId) {

    var rows = document.getElementById(xTableId).getElementsByTagName("tr");

    for (i = 0; i < rows.length; i++) {
        var link = rows[i].getElementsByTagName("a")
        if (link.length > 0) if (link[0].href != '') {
            rows[i].onclick = new Function('this.className=""; document.location.href="' + link[0].href + '"');
            rows[i].onmouseover = new Function("this.className='highlight'");
            rows[i].onmouseout = new Function("this.className=''");
        }
    }
}

function IdentifySites() {
    var rows = document.getElementById('sites').getElementsByTagName('tr');
    var currentParentArea;
    for (i = 0; i < rows.length; i++) {
        var row = rows[i];
        switch (row.getElementsByTagName('td').length) {
            case 1: //Area
                areaRows[areaRows.length] = row;
                currentParentArea = row;
                currentParentArea.siteRows = new Array();
                break;
            case 0: //Area headers
                currentParentArea.headerRow = row;
                break;
            default: //Site
                currentParentArea.siteRows[currentParentArea.siteRows.length] = row;
        }
    }
};

function Filter(filterstring) {

    //            var theText = document.getElementById('sites').innerHTML;
    //            theText = theText.replace(/<b>/gi, "");
    //            theText = theText.replace(/<\/b>/gi, "");
    //            document.getElementById('sites').innerHTML = theText;

    filterstring = filterstring.toLowerCase();
    matchCount = 0;
    for (i = 0; i < areaRows.length; i++) {
        var area = areaRows[i];
        var areaMatchCount = 0;
        for (j = 0; j < area.siteRows.length; j++) {
            var site = area.siteRows[j];
            if (innerTextSupported)
                var siteText = site.innerText;
            else
                var siteText = site.textContent;
            if (siteText.toLowerCase().indexOf(filterstring) == -1)
                site.style.display = 'none';
            else {
                site.style.display = '';
                //                        var theText = document.getElementById('sites').innerHTML;
                //                        theText = theText.replace(/searchstring/gi, "<span class=searchStringHighlight>$1</span>");
                //                        document.getElementById('sites').innerHTML = theText;

                matchCount++;
                areaMatchCount++;
                lastMatchRow = site;
            }
        }
        if (areaMatchCount == 0) {
            area.style.display = 'none';
            area.headerRow.style.display = 'none';
        }
        else {
            area.style.display = '';
            area.headerRow.style.display = '';
        }
    }
    if (filterstring == '')
        document.getElementById('searchHitsCount').innerHTML = matchCount + ' sites';
    else if (matchCount == 0)
        document.getElementById('searchHitsCount').innerHTML = 'No match';
    else if (matchCount == 1)
        document.getElementById('searchHitsCount').innerHTML = '1 matching site';
    else
        document.getElementById('searchHitsCount').innerHTML = matchCount + ' matching sites';

    if (matchCount == 1)
        document.getElementById('searchSubmit').style.display = '';
    else
        document.getElementById('searchSubmit').style.display = 'none';

    if (filterstring == '')
        document.getElementById('index').style.display = '';
    else
        document.getElementById('index').style.display = 'none';
}

function ShowSearchResult() {
    if (matchCount == 1) {
        document.getElementById('searchForm').action = lastMatchRow.getElementsByTagName("a")[0].href;
        return true;
    }
    else
        return false;
}

function urlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}
