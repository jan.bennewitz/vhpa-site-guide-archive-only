﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VHPASiteGuide
{
    public static class DrawSettings
    {
        public static Dictionary<String, ShapeDrawSetting> ShapeColours { get; set; }

        static DrawSettings()
        {
            ShapeColours = new Dictionary<string, ShapeDrawSetting>();
            foreach (var shapeColour in new ShapeDrawSetting[] {
                new ShapeDrawSetting("Landing", 2, "00ff00"),
                new ShapeDrawSetting("No Landing", 2, "ff0000"),
                new ShapeDrawSetting("No Launching", 2, "ff7f00"),
                new ShapeDrawSetting("Emergency Landing", 2, "ff7f00"),
                new ShapeDrawSetting("No Fly Zone", 2, "ff0000"),
                new ShapeDrawSetting("Hazard", 2, "ff0000"),
                new ShapeDrawSetting("Feature", 2, "0000ff"),
                new ShapeDrawSetting("Access", 1, "0000ff"),
                new ShapeDrawSetting("Powerline", 1, "ff0000"),
            })
                ShapeColours.Add(shapeColour.Class, shapeColour);
        }

        public struct ShapeDrawSetting
        {
            public String Class;
            public Byte Dimension;
            public String Colour;
            public ShapeDrawSetting(string @class, byte dimension, string colour) { Class = @class; Dimension = dimension; Colour = colour; }
        }
    }
}