<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>

  <xsl:include href="common.xsl"/>

  <xsl:param name="area"/>
  <xsl:key name="keyContactByName" match="contact" use="name"/>

  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          <xsl:text>Australian National Site Guide - </xsl:text>
          <xsl:choose>
            <xsl:when test="$area/@fullName">
              <xsl:value-of select="$area/@fullName"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$area/@name"/>
            </xsl:otherwise>
          </xsl:choose>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="printGuide.css" />
        <link rel="stylesheet" type="text/css" media="print" href="print.css" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <meta name="description" content="Home of the Australian National Site Guide including schools and clubs" />
        <meta name="keywords" content="Australia Victoria paragliding hang gliding hanggliding microlight flying VHPA Australian National Site Guide clubs schools committee association" />
      </head>
      <body>
        <script>
          window.cookieconsent_options = {
          message: "Use of the Australian National Site Guide is covered by our disclaimer.",
          learnMore: 'Read the disclaimer here.',
          link: 'disclaimer.html',
          dismiss : "Got it.",
          theme: "dark-floating",
          expiryDays: 99999,
          };
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <xsl:call-template name="common_menu-top"/>
        <div id="content">
          <h1>
            <xsl:text>Australian National Site Guide - </xsl:text>
            <xsl:choose>
              <xsl:when test="$area/@fullName">
                <xsl:value-of select="$area/@fullName"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$area/@name"/>
              </xsl:otherwise>
            </xsl:choose>
          </h1>

          <div class="instructions">
            <p>When printing, you might find that Firefox doesn't print correctly - Chrome, Internet Explorer and Safari seem to work better.</p>
            <p>You can save this page (File -> Save Page As...) to your hard drive for offline browsing.</p>
          </div>

          <xsl:apply-templates select="$area"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="area">
    <xsl:if test=".//siteFreeFlight">
      <xsl:variable name="areaName">
        <xsl:call-template name="common_areaFullName"/>
      </xsl:variable>

      <div xmlns="http://www.w3.org/1999/xhtml" class="area-sites">
        <xsl:attribute name="id">
          <xsl:call-template name="common_areaOrSiteId"/>
        </xsl:attribute>
        <h2>
          <xsl:call-template name="common_areaQualifiedName">
            <xsl:with-param name="full" select="'y'"/>
          </xsl:call-template>
          - <xsl:value-of select="count(.//siteFreeFlight[not(closed)])"/> site<xsl:if test="count(.//siteFreeFlight[not(closed)]) >1">s</xsl:if>
        </h2>

        <a class="areaMap">
          <xsl:attribute name="href">
            <xsl:text>siteguidemap.html?type=open&amp;</xsl:text>
            <xsl:if test="count(.//siteFreeFlight[not(closed)]/launch)=1">
              <xsl:text>pin=</xsl:text>
              <xsl:value-of select=".//siteFreeFlight[not(closed)]/name"/>
              <xsl:if test=".//siteFreeFlight[not(closed)]/launch/name">
                <xsl:text> - </xsl:text>
                <xsl:value-of select=".//siteFreeFlight[not(closed)]/launch/name"/>
              </xsl:if>
              <xsl:text>&amp;</xsl:text>
            </xsl:if>
            <xsl:call-template name="spanAndCenter">
              <xsl:with-param name="spanParamName" select="'spn'"/>
              <xsl:with-param name="centerParamName" select="'ll'"/>
            </xsl:call-template>
          </xsl:attribute>
          <img alt="Area map" title="Area map">
            <xsl:attribute name="alt">
              <xsl:text>Map of </xsl:text>
              <xsl:value-of select="$areaName"/>
              <xsl:text> sites</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
              <xsl:text>Map of </xsl:text>
              <xsl:value-of select="$areaName"/>
              <xsl:text> sites</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="src">
              <xsl:text>https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&amp;size=500x250&amp;visible=</xsl:text>
              <xsl:call-template name="common_launchesVisible">
                <xsl:with-param name="minSpan" select="2"/>
              </xsl:call-template>
              <xsl:text>&amp;markers=icon:http://siteguide.org.au/Images/mm_20_green.png</xsl:text>
              <xsl:for-each select=".//siteFreeFlight[not(closed)]">
                <xsl:text>|</xsl:text>
                <xsl:call-template name="common_launchesCenterpoint"/>
              </xsl:for-each>
            </xsl:attribute>
          </img>
        </a>

        <xsl:if test="siteFreeFlight[not(closed)]">
          <table class="index-table" rules="rows" frame="void">
            <tbody>
              <tr>
                <th>Name</th>
                <th>Location</th>
                <th>Type</th>
                <th>Conditions</th>
                <th>Rating</th>
                <th>Height</th>
              </tr>
              <xsl:for-each select="siteFreeFlight[not(closed)]">
                <xsl:sort select="name"/>
                <tr>
                  <td>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:call-template name="common_areaOrSiteId"/>
                      </xsl:attribute>
                      <xsl:value-of select="name"/>
                    </a>
                  </td>
                  <td>
                    <xsl:value-of select="shortlocation"/>
                  </td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="type"/>
                  </td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="conditions"/>
                  </td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="rating"/>
                  </td>
                  <td>
                    <xsl:value-of disable-output-escaping="yes" select="height"/>
                  </td>
                </tr>
              </xsl:for-each>
              <!--site-->
            </tbody>
          </table>
        </xsl:if>

        <xsl:if test="area[.//siteFreeFlight]">
          <table class="index-table" rules="rows" frame="void">
            <tbody>
              <tr>
                <th>Area</th>
                <th>Sites</th>
              </tr>
              <xsl:for-each select="area[.//siteFreeFlight]">
                <xsl:sort select="@name"/>
                <tr>
                  <td>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:call-template name="common_areaOrSiteId"/>
                      </xsl:attribute>
                      <xsl:call-template name="common_areaFullName"/>
                    </a>
                  </td>
                  <td>
                    <xsl:value-of select="count(.//siteFreeFlight[not(closed)])"/>
                  </td>
                </tr>
              </xsl:for-each>
              <!--site-->
            </tbody>
          </table>
        </xsl:if>

        <xsl:apply-templates select="siteFreeFlight[not(closed)]">
          <xsl:sort select="name"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="area">
          <xsl:sort select="@name"/>
        </xsl:apply-templates>
        <!--area-->
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template match="siteFreeFlight">
    <div xmlns="http://www.w3.org/1999/xhtml" class="siteguide-page">
      <xsl:attribute name="id">
        <xsl:call-template name="common_areaOrSiteId"/>
      </xsl:attribute>
      <div class="sidebar">
        <table class="sitestats" rules="rows" width="100%" frame="void">
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="type"/>
            </td>
            <td class="lastRow">
              <xsl:value-of disable-output-escaping="yes" select="conditions"/>
            </td>
          </tr>
          <tr>
            <td>
              <xsl:value-of disable-output-escaping="yes" select="height"/>
            </td>
            <td class="lastRow">
              <xsl:value-of disable-output-escaping="yes" select="rating"/>
            </td>
          </tr>
        </table>
        <ul class="launchList">
          <xsl:for-each select="launch">
            <li>
              <a class="gmaplink">
                <xsl:attribute name="title">
                  <xsl:text>Click to navigate to </xsl:text>
                  <xsl:choose>
                    <xsl:when test="name">
                      <xsl:value-of select="name"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>launch</xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="href">
                  <xsl:text>https://www.google.com/maps/dir//'</xsl:text>
                  <xsl:value-of select="format-number(lat, 'N0.00000;S0.00000')" />,<xsl:value-of select="format-number(lon, 'E0.00000;W0.00000')" />
                  <xsl:text>'</xsl:text>
                </xsl:attribute>
                <img src="Images\maps_14dp.png"/>
                <span>
                  <xsl:choose>
                    <xsl:when test="name">
                      <xsl:value-of select="name"/>
                      <xsl:text>:</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text>Launch:</xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </a>
              <a title="Click for interactive map">
                <xsl:attribute name="href">
                  <xsl:text>siteguidemap.html?type=open</xsl:text>
                  <xsl:if test="../closed">,closed</xsl:if>
                  <xsl:text>&amp;pin=</xsl:text>
                  <xsl:value-of select="../name"/>
                  <xsl:if test="name">
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="name"/>
                  </xsl:if>
                </xsl:attribute>
                <span class="coords">
                  <xsl:value-of select="format-number(lat, 'N0.00000;S0.00000')" />
                  <xsl:text disable-output-escaping="yes">,&amp;nbsp;</xsl:text>
                  <xsl:value-of select="format-number(lon, 'E0.00000;W0.00000')" />
                </span>
              </a>
            </li>
          </xsl:for-each>
        </ul>
        <div class="sideBarImage">
          <xsl:call-template name="common_sitemap"/>
        </div>

        <xsl:if test="shortlocation">
          <xsl:value-of select="shortlocation"/>
          <xsl:if test="shortlocation and extendedlocation">
            <xsl:text> </xsl:text>
          </xsl:if>
          <xsl:value-of disable-output-escaping="yes" select="extendedlocation"/>
        </xsl:if>

        <xsl:if test="launch[estaEmergencyMarker] or mapShape[estaEmergencyMarker]">
          <div class="emergencyMarkers">
            <h5>
              <xsl:text>ESTA 000 Emergency Markers (</xsl:text>
              <a href="https://www.esta.vic.gov.au/emergency-markers" title="Emergency markers pinpoint your exact location during an emergency in public open spaces or a hard to define locality. Click to learn more.">?</a>
              <xsl:text>)</xsl:text>
            </h5>
            <ul>
              <xsl:for-each select="*[estaEmergencyMarker]|mapShapeRef">
                <xsl:choose>
                  <xsl:when test="self::launch|self::mapShape">
                    <xsl:call-template name="common_estaEmergencyMarker">
                      <xsl:with-param name="element" select="."/>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:when test="self::mapShapeRef">
                    <xsl:variable name="ref" select="@site"/>
                    <xsl:for-each select="//siteFreeFlight[name=$ref]/mapShape[estaEmergencyMarker]">
                      <xsl:call-template name="common_estaEmergencyMarker">
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </xsl:for-each>
                  </xsl:when>
                </xsl:choose>
              </xsl:for-each>
            </ul>
          </div>
        </xsl:if>

        <xsl:if test="smallpicture">
          <div class="sideBarImage">
            <img alt="">
              <xsl:choose>
                <xsl:when test="smallpicturetitle">
                  <xsl:attribute name="title">
                    <xsl:value-of disable-output-escaping="yes" select="../smallpicturetitle"/>
                  </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="title">
                    <xsl:text>Site picture</xsl:text>
                  </xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:attribute name="src">
                <xsl:text>Site%20Photos/</xsl:text>
                <xsl:value-of select="smallpicture"/>
              </xsl:attribute>
            </img>
          </div>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="landowners">
            <h4>Landowners</h4>
            <xsl:value-of disable-output-escaping="yes" select="landowners"/>
          </xsl:when>
          <xsl:otherwise>
            <h4>Landowners - ?</h4>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:call-template name="contactresponsible"/>

        <xsl:if test="seeAlso">
          <h4>See also:</h4>
          <a xmlns="http://www.w3.org/1999/xhtml">
            <xsl:attribute name="href">
              <xsl:value-of select="seeAlso/link"/>
            </xsl:attribute>
            <xsl:value-of select="seeAlso/name"/>
          </a>
        </xsl:if>
      </div>

      <div class="main">
        <!--<xsl:attribute name="class">
					<xsl:if test="closed">invalid</xsl:if>
				</xsl:attribute>-->

        <xsl:for-each select="..">
          <xsl:call-template name="common_areaQualifiedName">
            <xsl:with-param name="full" select="'y'"/>
          </xsl:call-template>
        </xsl:for-each>
        <h3>
          <xsl:value-of select="name"/>
        </h3>

        <!--<xsl:if test="closed">
					<h3 class="closed-summary">
						This site is closed. <xsl:value-of disable-output-escaping="yes" select="closed"/>
					</h3>
				</xsl:if>-->
        <xsl:value-of disable-output-escaping="yes" select="description"/>
        <xsl:if test="restrictions">
          <div class="restrictions">
            <img alt="Restrictions:" title="Restrictions" src="Images/alert.png" class="alertIcon"/>
            <xsl:value-of disable-output-escaping="yes" select="restrictions"/>
          </div>
        </xsl:if>
        <xsl:if test="takeoff or launch[conditions or description]">
          <h4>Takeoff</h4>
          <xsl:if test="takeoff">
            <div class="site-field">
              <xsl:value-of disable-output-escaping="yes" select="takeoff"/>
            </div>
          </xsl:if>
          <xsl:for-each select="launch[conditions or description]">
            <div class="launch">
              <h5>
                <xsl:value-of select="name"/>
              </h5>
              <xsl:if test="conditions">
                <xsl:text>(</xsl:text>
                <xsl:value-of select="conditions"/>
                <xsl:text>) </xsl:text>
              </xsl:if>
              <xsl:value-of disable-output-escaping="yes" select="description"/>
            </div>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="landing">
          <h4>Landing</h4>
          <xsl:value-of disable-output-escaping="yes" select="landing"/>
        </xsl:if>
        <xsl:if test="flightcomments">
          <h4>Flight</h4>
          <xsl:value-of disable-output-escaping="yes" select="flightcomments"/>
        </xsl:if>
        <xsl:if test="hazardscomments">
          <h4>Hazards/Comments</h4>
          <xsl:value-of disable-output-escaping="yes" select="hazardscomments"/>
        </xsl:if>
        <xsl:if test="mapShape[coordinates]|mapShapeRef">
          <h4>Map information</h4>
          <div>
            <xsl:call-template name="common_mapShapeMap">
              <xsl:with-param name="size" select="525"/>
            </xsl:call-template>
          </div>
          <!--<xsl:for-each select="mapShape">
                    <div>
                      <h4>
                        <xsl:value-of select="@category"/>
                        <xsl:if test="name">
                          <xsl:text>: </xsl:text>
                          <xsl:value-of select="name"/>
                        </xsl:if>
                      </h4>
                    </div>
                    <xsl:value-of select="description"/>
                    <div>
                      <xsl:call-template name="common_mapShapeMap"/>
                    </div>
                  </xsl:for-each>-->
        </xsl:if>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="contactresponsible">
    <!--check if contact and responsible entries are the same - '***' acts as a spacer between contacts. Might be a better way for this? -->
    <xsl:variable name="contact">
      <xsl:for-each select="contactName">
        <xsl:value-of select="." />***
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="responsible">
      <xsl:for-each select="responsibleName">
        <xsl:value-of select="." />***
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$contact = $responsible">
        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Contact/Responsible'"/>
          <xsl:with-param name="contacts" select="contactName"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:otherwise>
        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Contact'"/>
          <xsl:with-param name="contacts" select="contactName"/>
        </xsl:call-template>

        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Responsible'"/>
          <xsl:with-param name="contacts" select="responsibleName"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="contactlist">
    <xsl:param name="title"/>
    <xsl:param name="contacts"/>

    <xsl:choose>
      <xsl:when test="$contacts">
        <h4 xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="$title"/>
        </h4>
        <xsl:for-each select="key('keyContactByName', $contacts)">
          <xsl:apply-templates select="."/>
          <xsl:if test="not(position()=last())">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <h4 xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="$title"/> - ?
        </h4>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="contact">
    <xsl:choose>
      <xsl:when test="link">
        <a xmlns="http://www.w3.org/1999/xhtml">
          <xsl:attribute name="href">
            <xsl:value-of select="link"/>
          </xsl:attribute>
          <xsl:if test="fullname">
            <xsl:attribute name="title">
              <xsl:value-of select="fullname"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="name"/>
        </a>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="name"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="phone">
      <xsl:text> (</xsl:text>
      <xsl:for-each select="phone">
        <span class="nobr" xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="."/>
        </span>
        <xsl:if test="not(position()=last())">, </xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:if test="address">
      <xsl:choose>
        <xsl:when test="phone">, </xsl:when>
        <xsl:otherwise> </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="address"/>
    </xsl:if>

  </xsl:template>

  <xsl:template name="spanAndCenter">
    <xsl:param name="spanParamName" select="'span'"/>
    <xsl:param name="centerParamName" select="'center'"/>

    <xsl:variable name="minSpan">2</xsl:variable>

    <xsl:variable name="max_lat">
      <xsl:for-each select=".//siteFreeFlight[not(closed)]/launch/lat">
        <xsl:sort data-type="number" order="descending"/>
        <xsl:if test="position()=1">
          <xsl:value-of select=". + $minSpan div 10"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="min_lat">
      <xsl:for-each select=".//siteFreeFlight[not(closed)]/launch/lat">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1">
          <xsl:value-of select=". - $minSpan div 10"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="max_lon">
      <xsl:for-each select=".//siteFreeFlight[not(closed)]/launch/lon">
        <xsl:sort data-type="number" order="descending"/>
        <xsl:if test="position()=1">
          <xsl:value-of select=". + $minSpan div 10"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="min_lon">
      <xsl:for-each select=".//siteFreeFlight[not(closed)]/launch/lon">
        <xsl:sort data-type="number" order="ascending"/>
        <xsl:if test="position()=1">
          <xsl:value-of select=". - $minSpan div 10"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="$spanParamName"/>
    <xsl:text>=</xsl:text>
    <xsl:choose>
      <xsl:when test="$minSpan > $max_lat - $min_lat">
        <xsl:value-of select="$minSpan"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$max_lat - $min_lat"></xsl:value-of>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>,</xsl:text>
    <xsl:choose>
      <xsl:when test="$minSpan > $max_lon - $min_lon">
        <xsl:value-of select="$minSpan"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$max_lon - $min_lon"></xsl:value-of>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&amp;</xsl:text>
    <xsl:value-of select="$centerParamName"/>
    <xsl:text>=</xsl:text>
    <xsl:value-of select="($max_lat + $min_lat) div 2" />,<xsl:value-of select="($max_lon + $min_lon) div 2" />
  </xsl:template>

</xsl:stylesheet>