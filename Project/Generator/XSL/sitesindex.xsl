<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>
	
	<xsl:include href="common.xsl"/>

	<xsl:param name="restrictToType"/> <!--Optional. 'open' or 'closed' - default is to show both.-->
	
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<xsl:call-template name="common_head">
				<xsl:with-param name="title">Australian National Site Guide - List of all <xsl:if test="$restrictToType = 'open'">open </xsl:if><xsl:if test="$restrictToType = 'closed'">closed </xsl:if>sites</xsl:with-param>
				<!--<xsl:with-param name="clickableTableRows" select="'Y'"/>-->
			</xsl:call-template>
			<body><!--onload="ConvertRowsToLinks()">-->
        <script xml:space="preserve" type="text/javascript" src="siteIndexSearch.js"> </script>
        <script>
          window.cookieconsent_options = {
          message: "Use of the Australian National Site Guide is covered by our disclaimer.",
          learnMore: 'Read the disclaimer here.',
          link: 'disclaimer.html',
          dismiss : "Got it.",
          theme: "dark-floating",
          expiryDays: 99999,
          };
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <xsl:call-template name="common_menu-top"/>
				<xsl:call-template name="common_header_site-guide"/>
				<div id="wrapper-content">
					<div id="wrapper-content2">
						<xsl:call-template name="common_menu-page_siteguide">
							<xsl:with-param name="mapType">
								<xsl:choose>
									<xsl:when test="$restrictToType='open'">open</xsl:when>
									<xsl:otherwise>open,closed</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
						</xsl:call-template>

						<div class="content">
							<h2>Index of <xsl:if test="$restrictToType = 'open'">Open </xsl:if><xsl:if test="$restrictToType = 'closed'">Closed </xsl:if>Sites</h2>
							<xsl:call-template name="common_contribute"/>
							<xsl:choose>
								<xsl:when test="$restrictToType = 'open'">
									<p>You can <a href="sitesindex.html">show both open and closed sites</a> or <a href="sitesclosedindex.html">show only closed sites</a>.</p>
								</xsl:when>
								<xsl:when test="$restrictToType = 'closed'">
									<p>You can <a href="sitesindex.html">show both open and closed sites</a> or <a href="sitesopenindex.html">show only open sites</a>.</p>
								</xsl:when>
								<xsl:otherwise>
									<p>You can <a href="sitesopenindex.html">show only open sites</a> or <a href="sitesclosedindex.html">show only closed sites</a>.</p>
								</xsl:otherwise>
							</xsl:choose>
              <form id="searchForm" name="search" onsubmit="return ShowSearchResult()">
                <p id="searchBar" style='display: none'>
                  Find:        
                  <input id="searchstring" name="q" type="text" onkeyup="Filter(this.value)" onblur="Filter(this.value)"/>
                  <input id="searchSubmit" type="submit" value="Show" style="display: none" />
                  <span id="searchHitsCount">-</span>
                </p>
              </form>
              <noscript>
                <p>Search is not available since javascript is disabled in your browser. Enable javascript or use your browser's built-in search function (try ctrl-F or ⌘-F).</p>
              </noscript>
            </div>
            <div class="content">
							<table id="index" class="index-table" rules="rows" frame="void">
								<tbody>
									<tr>
										<th>Area</th>
										<xsl:if test="$restrictToType!='closed'">
											<th class="siteCountHeader">Open sites</th>
										</xsl:if>
										<xsl:if test="$restrictToType!='open'">
											<th class="siteCountHeader">Closed sites</th>
										</xsl:if>
									</tr>
									<xsl:for-each select="siteGuideData">
										<xsl:call-template name="area">
											<xsl:with-param name="index" select="'Y'"/>
										</xsl:call-template>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
						<div class="content-full-width">
							<table id="sites" class="index-table" rules="rows" frame="void">
								<tbody>
									<xsl:for-each select="siteGuideData">
										<xsl:call-template name="area"/>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<xsl:call-template name="common_footer"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="area">
		<xsl:param name="parent"/>
		<xsl:param name="index"/>
		
		<xsl:if test="$index=''">
			<xsl:for-each select="siteFreeFlight[(not(closed) or $restrictToType!='open') and (closed or $restrictToType!='closed')]">
				<xsl:sort select="name"/>
				<tr xmlns="http://www.w3.org/1999/xhtml">
					<td><a href="Sites/{name/node()}.html"><xsl:value-of select="name"/>
            <xsl:if test="launch[name]">
              <ul>
                <xsl:for-each select="launch[name]">
                  <li>
                    <xsl:value-of select="name"/>
                  <!--<xsl:if test="not(position()=last())">, </xsl:if>-->
                  </li>
                </xsl:for-each>
              </ul>
            </xsl:if>
          </a></td>
					<td><xsl:value-of select="shortlocation"/></td>
					<td><xsl:value-of disable-output-escaping="yes" select="type"/></td>
					<xsl:choose>
						<xsl:when test="closed">
							<td>
								<xsl:if test="$restrictToType!='closed'">
									<xsl:attribute name="colspan">99</xsl:attribute>
									<span style="color:red">Closed. </span>
								</xsl:if>
								<xsl:value-of disable-output-escaping="yes" select="closed"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of disable-output-escaping="yes" select="conditions"/></td>
							<td><xsl:value-of disable-output-escaping="yes" select="rating"/></td>
							<td><xsl:value-of disable-output-escaping="yes" select="height"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</tr>
			</xsl:for-each>
		</xsl:if>
		
		<xsl:for-each select="area[.//siteFreeFlight[(not(closed) or $restrictToType!='open') and (closed or $restrictToType!='closed')]]">
			<xsl:sort select="@name"/>

			<xsl:variable name="header">
				<xsl:if test="$parent"><xsl:value-of select="$parent"/> - </xsl:if>
				<xsl:call-template name="common_areaFullName"/>
			</xsl:variable>

			<xsl:if test="siteFreeFlight[(not(closed) or $restrictToType!='open') and (closed or $restrictToType!='closed')]">
				<xsl:choose>
					<xsl:when test="$index">
						<tr xmlns="http://www.w3.org/1999/xhtml">
							<td>
								<a>
									<xsl:attribute name="href">#<xsl:value-of select="$header"/></xsl:attribute>
									<xsl:value-of select="$header"/>
								</a>
							</td>
							<xsl:if test="$restrictToType!='closed'">
								<td>
									<xsl:attribute name="class">
                    <xsl:text>numeric</xsl:text>
                    <xsl:if test="$restrictToType='open'"> lastrow</xsl:if>
									</xsl:attribute>
									<xsl:if test="count(siteFreeFlight[not(closed)])>0">
										<xsl:value-of select="count(siteFreeFlight[not(closed)])"/>
									</xsl:if>
								</td>
							</xsl:if>
							<xsl:if test="$restrictToType!='open'">
								<td class="numeric lastRow">
									<xsl:if test="count(siteFreeFlight[(closed)])>0">
										<xsl:value-of select="count(siteFreeFlight[closed])"/>
									</xsl:if>
								</td>
							</xsl:if>
						</tr>
					</xsl:when>
					<xsl:otherwise>
						<tr xmlns="http://www.w3.org/1999/xhtml">
							<td colspan="99">
								<a>
									<xsl:attribute name="name">
										<xsl:value-of select="$header"/>
									</xsl:attribute>
								</a>
								<h3>
									<xsl:value-of select="$header"/>
								</h3>
							</td>
						<!--<td>
								<a class="toIndex" href="#index">Back to index</a>
							</td>-->
						</tr>
						<tr xmlns="http://www.w3.org/1999/xhtml">
							<th>Name</th>
							<th>Location</th>
							<th>Type</th>
							<xsl:choose>
								<xsl:when test="$restrictToType='closed'">
									<th>Closed</th>
								</xsl:when>
								<xsl:otherwise>
									<th>Conditions</th>
									<th>Rating</th>
									<th>Height</th>
								</xsl:otherwise>
							</xsl:choose>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:call-template name="area">
				<xsl:with-param name="parent" select="$header"/>
				<xsl:with-param name="index" select="$index"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>