<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="text/xml"
				indent="yes"
				cdata-section-elements="marker desc"/>

  <!--<xsl:include href="common.xsl"/>-->
  <xsl:key name="keyContactByName" match="contact" use="name"/>

  <xsl:template match="/">
    <mapData>
      <!--<markerCoords>
				<xsl:value-of select="siteGuideData/encodedMarkerCoordinates"/>
			</markerCoords>-->
      <!-- sorted by type, then alphabetically -->
      <xsl:apply-templates select="//siteFreeFlight[not(closed)]">
        <xsl:sort select="name"/>
      </xsl:apply-templates>
      <!--<xsl:apply-templates select="//siteMicrolight">
        <xsl:sort select="name"/>
      </xsl:apply-templates>-->
      <xsl:apply-templates select="//siteFreeFlight[closed]">
        <xsl:sort select="name"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="//area/mapShape"/>

      <!-- alternative: sorted alphabetically
			<xsl:apply-templates select="//siteFreeFlight/launch|//siteMicrolight">
				<xsl:sort select="self::node()[name(.)='launch']/../name|self::node()[name(.)='siteMicrolight']/name"/>
				<xsl:sort select="self::node()[name(.)='launch']/name"/>
			</xsl:apply-templates>
			-->
    </mapData>
  </xsl:template>

  <xsl:template match="siteFreeFlight">
    <site>
      <xsl:apply-templates select="launch">
        <xsl:sort select="name"/>
      </xsl:apply-templates>

      <xsl:apply-templates select="mapShape"/>
    </site>
  </xsl:template>

  <xsl:template match="launch">
    <!--coords, name, description, type-->
    <marker>
      <xsl:attribute name="lat">
        <xsl:value-of select="lat"/>
      </xsl:attribute>
      <xsl:attribute name="lon">
        <xsl:value-of select="lon"/>
      </xsl:attribute>
      <xsl:attribute name="name">
        <xsl:value-of select="../name"/>
        <xsl:if test="name">
          <xsl:text> - </xsl:text>
          <xsl:value-of select="name"/>
        </xsl:if>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="../closed">
          <xsl:attribute name="type">closed</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="type">open</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="site">
        <xsl:value-of select="../name"/>
      </xsl:attribute>
      <xsl:if test="../closed">
        <xsl:text><![CDATA[<font color="red"><b>This site is closed.</b></font> ]]></xsl:text>
        <xsl:value-of select="../closed"/>
        <xsl:text><![CDATA[<br/>]]></xsl:text>
      </xsl:if>
      <xsl:text><![CDATA[<hr/><table width="100%"><tr><td>]]></xsl:text>
      <xsl:value-of select="../type"/>
      <xsl:text><![CDATA[</td><td>]]></xsl:text>
      <xsl:value-of select="../height"/>
      <xsl:text><![CDATA[</td></tr><tr><td>]]></xsl:text>
      <xsl:choose>
        <xsl:when test="conditions">
          <xsl:value-of select="conditions"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="../conditions"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text><![CDATA[</td><td>]]></xsl:text>
      <xsl:value-of select="../rating"/>
      <xsl:text><![CDATA[</td></tr></table><hr/>]]></xsl:text>
      <xsl:if test="not(../closed)">
        <xsl:value-of select="../description"/>
        <xsl:if test="description and ../description">
          <xsl:text><![CDATA[<p></p>]]></xsl:text>
        </xsl:if>
        <xsl:if test="description">
          <xsl:text><![CDATA[<u>]]></xsl:text>
          <xsl:value-of select="name"/>
          <xsl:text><![CDATA[</u>: ]]></xsl:text>
          <xsl:value-of select="description"/>
        </xsl:if>
        <xsl:if test="(description or ../description) and ../restrictions">
          <xsl:text><![CDATA[<p></p>]]></xsl:text>
        </xsl:if>
        <xsl:if test="../restrictions">
          <xsl:text><![CDATA[<img alt="Restrictions:" title="Restrictions" src="http://siteguide.org.au/Images/alert.png" style="margin-right: .3em;"/>]]></xsl:text>
          <xsl:value-of select="../restrictions"/>
        </xsl:if>

        <!--check if contact and responsible entries are the same - '***' acts as a spacer between contacts. Might be a better way for this? -->
        <xsl:variable name="contact">
          <xsl:for-each select="../contactName">
            <xsl:value-of select="." />***
          </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="responsible">
          <xsl:for-each select="../responsibleName">
            <xsl:value-of select="." />***
          </xsl:for-each>
        </xsl:variable>

        <xsl:choose>
          <xsl:when test="$contact = $responsible">
            <xsl:call-template name="contactlist">
              <xsl:with-param name="title" select="'Contact/Responsible'"/>
              <xsl:with-param name="contacts" select="../contactName"/>
            </xsl:call-template>
          </xsl:when>

          <xsl:otherwise>
            <xsl:call-template name="contactlist">
              <xsl:with-param name="title" select="'Contact'"/>
              <xsl:with-param name="contacts" select="../contactName"/>
            </xsl:call-template>

            <xsl:call-template name="contactlist">
              <xsl:with-param name="title" select="'Responsible'"/>
              <xsl:with-param name="contacts" select="../responsibleName"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>

        <!--<xsl:if test="../seeAlso">
          <xsl:text><![CDATA[<h4>See also:</h4><a href="]]></xsl:text>
              <xsl:value-of select="../seeAlso/link"/>
          <xsl:text><![CDATA[">]]></xsl:text>
          <xsl:value-of select="../seeAlso/name"/>
          <xsl:text><![CDATA[</a>]]></xsl:text>
        </xsl:if>-->
      </xsl:if>
    </marker>
  </xsl:template>

  <!--<xsl:template match="siteMicrolight">
    <marker>
      <xsl:attribute name="lat">
        <xsl:value-of select="lat"/>
      </xsl:attribute>
      <xsl:attribute name="lon">
        <xsl:value-of select="lon"/>
      </xsl:attribute>
      <xsl:attribute name="name">
        <xsl:value-of select="name"/>
      </xsl:attribute>
      <xsl:attribute name="type">microlight</xsl:attribute>
      <xsl:value-of select="facilities"/>
    </marker>
  </xsl:template>-->

  <xsl:template match="mapShape">
    <shape>
      <xsl:attribute name="category">
        <xsl:value-of select="@category"/>
      </xsl:attribute>
      <desc>
        <xsl:text><![CDATA[<b>]]></xsl:text>
        <xsl:if test="parent::siteFreeFlight">
          <xsl:value-of select="../name"/>
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:text>(</xsl:text>
        <xsl:value-of select="@category"/>
        <xsl:text>) </xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text><![CDATA[</b>]]></xsl:text>
        <xsl:if test="description or estaEmergencyMarker">
          <xsl:if test="description">
            <xsl:text><![CDATA[<br/><br/>]]></xsl:text>
            <xsl:value-of select="description"/>
          </xsl:if>
          <xsl:if test="estaEmergencyMarker">
            <xsl:text><![CDATA[<br/><br/><u>ESTA 000 Emergency Marker</u> (<a href="https://www.esta.vic.gov.au/emergency-markers" title="Emergency markers pinpoint your exact location during an emergency in public open spaces or a hard to define locality. Click to learn more.">?</a>): ]]></xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="estaEmergencyMarker"/>
          </xsl:if>
        </xsl:if>
      </desc>
      <xsl:for-each select="encodedCoordinates[@level='detail']">
        <coords>
          <xsl:value-of select="."/>
        </coords>
      </xsl:for-each>
      <xsl:for-each select="encodedLevels">
        <levels>
          <xsl:value-of select="."/>
        </levels>
      </xsl:for-each>
    </shape>
  </xsl:template>

  <xsl:template name="contactlist">
    <xsl:param name="title"/>
    <xsl:param name="contacts"/>

    <xsl:choose>
      <xsl:when test="$contacts">
        <xsl:text><![CDATA[<h4>]]></xsl:text>
        <xsl:value-of select="$title"/>
        <xsl:text><![CDATA[</h4>]]></xsl:text>
        <xsl:for-each select="key('keyContactByName', $contacts)">
          <xsl:apply-templates select="."/>
          <xsl:if test="not(position()=last())">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text><![CDATA[<h4>]]></xsl:text>
        <xsl:value-of select="$title"/>
        <xsl:text><![CDATA[ - ?</h4>]]></xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="contact">

    <xsl:choose>
      <xsl:when test="link">
        <xsl:text><![CDATA[<a href=']]></xsl:text>
        <xsl:value-of select="link"/>
        <xsl:text><![CDATA['>]]></xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text><![CDATA[</a>]]></xsl:text>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="name"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="phone">
      <xsl:text> (</xsl:text>
      <xsl:for-each select="phone">
        <xsl:value-of select="."/>
        <xsl:if test="not(position()=last())">, </xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:if test="address">
      <xsl:choose>
        <xsl:when test="phone">, </xsl:when>
        <xsl:otherwise> </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="address"/>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>