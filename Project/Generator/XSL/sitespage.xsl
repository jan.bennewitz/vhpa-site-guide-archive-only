<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>

  <xsl:include href="common.xsl"/>

  <xsl:param name="siteName"/>
  <xsl:key name="keyContactByName" match="contact" use="name"/>

  <xsl:template match="/">
    <xsl:apply-templates select="//siteFreeFlight[name=$siteName]"/>
  </xsl:template>

  <xsl:template match="siteFreeFlight">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          <xsl:value-of select="name"/>
          <xsl:text> Hang-Gliding and Paragliding Site - Australian National Site Guide</xsl:text>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <link rel="stylesheet" type="text/css" media="print" href="../print.css" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
        <link rel="icon" href="../favicon.ico" type="image/x-icon" />
        <meta name="description">
          <xsl:attribute name="content">
            <xsl:text>Australian National Site Guide page for </xsl:text>
            <xsl:value-of select="name"/>
          </xsl:attribute>
        </meta>
        <meta name="keywords" content="Australia Victoria paragliding hang gliding hanggliding microlight flying VHPA Australian National Site Guide clubs schools committee association" />
      </head>
      <body>
        <script>
          window.cookieconsent_options = {
          message: "Use of the Australian National Site Guide is covered by our disclaimer.",
          learnMore: 'Read the disclaimer here.',
          link: 'disclaimer.html',
          dismiss : "Got it.",
          theme: "dark-floating",
          expiryDays: 99999,
          };
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <xsl:call-template name="common_menu-top">
          <xsl:with-param name="path" select="'../'"/>
        </xsl:call-template>
        <xsl:call-template name="common_header_site-guide">
          <xsl:with-param name="title">
            <xsl:for-each select="..">
              <xsl:call-template name="common_areaQualifiedName">
                <xsl:with-param name="full" select="'y'"/>
              </xsl:call-template>
            </xsl:for-each>
          </xsl:with-param>
        </xsl:call-template>
        <div id="wrapper-content">
          <div id="wrapper-content2">
            <div id="siteguide-page">
              <div id="siteguide-sidebar">
                <table id="sitestats" rules="rows" width="100%" frame="void">
                  <tr>
                    <td>
                      <xsl:value-of disable-output-escaping="yes" select="type"/>
                    </td>
                    <td class="lastRow">
                      <xsl:value-of disable-output-escaping="yes" select="conditions"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <xsl:value-of disable-output-escaping="yes" select="height"/>
                    </td>
                    <td class="lastRow">
                      <xsl:value-of disable-output-escaping="yes" select="rating"/>
                    </td>
                  </tr>
                </table>
                <ul id="launchList">
                  <xsl:for-each select="launch">
                    <li>
                      <a class="gmaplink"> 
                        <xsl:attribute name="title">
                          <xsl:text>Click to navigate to </xsl:text>
                          <xsl:choose>
                            <xsl:when test="name">
                              <xsl:value-of select="name"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:text>launch</xsl:text>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:attribute>
                        <xsl:attribute name="href">
                          <xsl:text>https://www.google.com/maps/dir//'</xsl:text>
                          <xsl:value-of select="format-number(lat, 'N0.00000;S0.00000')" />,<xsl:value-of select="format-number(lon, 'E0.00000;W0.00000')" />
                          <xsl:text>'</xsl:text>
                        </xsl:attribute>
                        <img src="..\Images\maps_14dp.png"/>
                        <span>
                          <xsl:choose>
                            <xsl:when test="name">
                              <xsl:value-of select="name"/>
                              <xsl:text>:</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:text>Launch:</xsl:text>
                            </xsl:otherwise>
                          </xsl:choose>
                        </span>
                      </a>
                      <a title="Click for interactive map">
                        <xsl:attribute name="href">
                          <xsl:text>../siteguidemap.html?type=open</xsl:text>
                          <xsl:if test="../closed">,closed</xsl:if>
                          <xsl:text>&amp;pin=</xsl:text>
                          <xsl:value-of select="../name"/>
                          <xsl:if test="name">
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="name"/>
                          </xsl:if>
                        </xsl:attribute>
                        <span class="coords">
                          <xsl:value-of select="format-number(lat, 'N0.00000;S0.00000')" />,<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          <xsl:value-of select="format-number(lon, 'E0.00000;W0.00000')" />
                        </span>
                      </a>
                    </li>
                  </xsl:for-each>
                </ul>
                <div class="sideBarImage">
                  <xsl:call-template name="common_sitemap">
                    <xsl:with-param name="path" select="'../'"/>
                  </xsl:call-template>
                </div>

                <xsl:if test="shortlocation">
                  <xsl:value-of disable-output-escaping="yes" select="shortlocation"/>
                  <xsl:if test="shortlocation and extendedlocation">
                    <xsl:text> </xsl:text>
                  </xsl:if>
                  <xsl:value-of disable-output-escaping="yes" select="extendedlocation"/>
                </xsl:if>

                <xsl:if test="*[estaEmergencyMarker]">
                  <div class="emergencyMarkers">
                    <h4>
                      <xsl:text>ESTA 000 Emergency Markers (</xsl:text>
                      <a href="https://www.esta.vic.gov.au/emergency-markers" title="Emergency markers pinpoint your exact location during an emergency in public open spaces or a hard to define locality. Click to learn more.">?</a>
                      <xsl:text>)</xsl:text>
                    </h4>
                    <ul>
                      <xsl:for-each select="*[estaEmergencyMarker]|mapShapeRef">
                        <xsl:choose>
                          <xsl:when test="self::launch|self::mapShape">
                            <xsl:call-template name="common_estaEmergencyMarker">
                              <xsl:with-param name="element" select="."/>
                            </xsl:call-template>
                          </xsl:when>
                          <xsl:when test="self::mapShapeRef">
                            <xsl:variable name="ref" select="@site"/>
                            <xsl:for-each select="//siteFreeFlight[name=$ref]/mapShape[estaEmergencyMarker]">
                              <xsl:call-template name="common_estaEmergencyMarker">
                                <xsl:with-param name="element" select="."/>
                              </xsl:call-template>
                            </xsl:for-each>
                          </xsl:when>
                        </xsl:choose>
                      </xsl:for-each>
                    </ul>
                  </div>
                </xsl:if>

                <xsl:if test="smallpicture">
                  <div class="sideBarImage">
                    <xsl:choose>
                      <xsl:when test="smallpicturehref">
                        <a>
                          <xsl:attribute name="href">
                            <xsl:value-of select="smallpicturehref"/>
                          </xsl:attribute>
                          <xsl:call-template name="smallsitepicture"/>
                        </a>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:call-template name="smallsitepicture"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                </xsl:if>
                <xsl:choose>
                  <xsl:when test="landowners">
                    <h3>Landowners</h3>
                    <xsl:value-of disable-output-escaping="yes" select="landowners"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <h3>Landowners - ?</h3>
                  </xsl:otherwise>
                </xsl:choose>

                <xsl:call-template name="contactresponsible"/>

                <xsl:if test="seeAlso">
                  <h3>See also</h3>
                  <a xmlns="http://www.w3.org/1999/xhtml">
                    <xsl:attribute name="href">
                      <xsl:value-of select="seeAlso/link"/>
                    </xsl:attribute>
                    <xsl:value-of select="seeAlso/name"/>
                  </a>
                </xsl:if>
              </div>

              <div id="siteguide-main">
                <xsl:attribute name="class">
                  <xsl:if test="closed">invalid</xsl:if>
                </xsl:attribute>

                <span class="updateLink">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:text>mailto:webmaster@vhpa.org.au?subject=Site%20guide%20update%20-%20</xsl:text>
                      <xsl:value-of select="name"/>
                    </xsl:attribute>
                    <xsl:text>Submit update</xsl:text>
                  </a>
                </span>

                <h2 id="siteguide-title">
                  <xsl:value-of disable-output-escaping="yes" select="name"/>
                </h2>

                <xsl:if test="closed">
                  <h2 id="closed-summary">
                    <xsl:text>This site is closed. </xsl:text>
                    <xsl:value-of disable-output-escaping="yes" select="closed"/>
                  </h2>
                </xsl:if>
                <xsl:value-of disable-output-escaping="yes" select="description"/>
                <xsl:if test="restrictions">
                  <div id="restrictions">
                    <img alt="Restrictions:" title="Restrictions" src="../Images/alert.png" class="alertIcon"/>
                    <xsl:value-of disable-output-escaping="yes" select="restrictions"/>
                  </div>
                </xsl:if>
                <xsl:if test="takeoff or launch[conditions or description]">
                  <h3>Takeoff</h3>
                  <div class="site-field">
                    <xsl:value-of disable-output-escaping="yes" select="takeoff"/>
                  </div>
                  <xsl:for-each select="launch[conditions or description]">
                    <div class="launch">
                      <h4>
                        <xsl:value-of select="name"/>
                      </h4>
                      <xsl:if test="conditions">
                        <xsl:text>(</xsl:text>
                        <xsl:value-of disable-output-escaping="yes" select="conditions"/>
                        <xsl:text>) </xsl:text>
                      </xsl:if>
                      <xsl:value-of disable-output-escaping="yes" select="description"/>
                    </div>
                  </xsl:for-each>
                </xsl:if>
                <xsl:if test="landing">
                  <h3>Landing</h3>
                  <xsl:value-of disable-output-escaping="yes" select="landing"/>
                </xsl:if>
                <xsl:if test="flightcomments">
                  <h3>Flight</h3>
                  <xsl:value-of disable-output-escaping="yes" select="flightcomments"/>
                </xsl:if>
                <xsl:if test="hazardscomments">
                  <h3>Hazards/Comments</h3>
                  <xsl:value-of disable-output-escaping="yes" select="hazardscomments"/>
                </xsl:if>
                <xsl:if test="mapShape[coordinates]|mapShapeRef">
                  <h3>Map information</h3>
                  <div>
                    <xsl:call-template name="common_mapShapeMap"/>
                  </div>
                  <!--<xsl:for-each select="mapShape">
                    <div>
                      <h4>
                        <xsl:value-of select="@category"/>
                        <xsl:if test="name">
                          <xsl:text>: </xsl:text>
                          <xsl:value-of select="name"/>
                        </xsl:if>
                      </h4>
                    </div>
                    <xsl:value-of select="description"/>
                    <div>
                      <xsl:call-template name="common_mapShapeMap"/>
                    </div>
                  </xsl:for-each>-->
                </xsl:if>
              </div>
            </div>
          </div>
        </div>

        <xsl:call-template name="common_footer">
          <xsl:with-param name="path" select="'..'"/>
        </xsl:call-template>
      </body>
    </html>

  </xsl:template>

  <xsl:template name="contactresponsible">
    <!--check if contact and responsible entries are the same - '***' acts as a spacer between contacts. Might be a better way for this? -->
    <xsl:variable name="contact">
      <xsl:for-each select="contactName">
        <xsl:value-of select="." />***
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="responsible">
      <xsl:for-each select="responsibleName">
        <xsl:value-of select="." />***
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$contact = $responsible">
        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Contact/Responsible'"/>
          <xsl:with-param name="contacts" select="contactName"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:otherwise>
        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Contact'"/>
          <xsl:with-param name="contacts" select="contactName"/>
        </xsl:call-template>

        <xsl:call-template name="contactlist">
          <xsl:with-param name="title" select="'Responsible'"/>
          <xsl:with-param name="contacts" select="responsibleName"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="contactlist">
    <xsl:param name="title"/>
    <xsl:param name="contacts"/>

    <xsl:choose>
      <xsl:when test="$contacts">
        <h3 xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="$title"/>
        </h3>
        <xsl:for-each select="key('keyContactByName', $contacts)">
          <xsl:apply-templates select="."/>
          <xsl:if test="not(position()=last())">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <h3 xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="$title"/>
          <xsl:text> - ?</xsl:text>
        </h3>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="contact">
    <xsl:choose>
      <xsl:when test="link">
        <a xmlns="http://www.w3.org/1999/xhtml">
          <xsl:attribute name="href">
            <xsl:value-of select="link"/>
          </xsl:attribute>
          <xsl:if test="fullname">
            <xsl:attribute name="title">
              <xsl:value-of select="fullname"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="name"/>
        </a>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="name"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="phone">
      <xsl:text> (</xsl:text>
      <xsl:for-each select="phone">
        <span class="nobr" xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="."/>
        </span>
        <xsl:if test="not(position()=last())">, </xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:if test="address">
      <xsl:choose>
        <xsl:when test="phone">, </xsl:when>
        <xsl:otherwise> </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="address"/>
    </xsl:if>

  </xsl:template>

  <xsl:template name="smallsitepicture">
    <img alt="" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:attribute name="title">
        <xsl:choose>
          <xsl:when test="smallpicturetitle">
            <xsl:value-of disable-output-escaping="yes" select="smallpicturetitle"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>Site picture</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:attribute name="src">
        <xsl:text>../Site%20Photos/</xsl:text>
        <xsl:value-of select="smallpicture"/>
      </xsl:attribute>
    </img>
  </xsl:template>
</xsl:stylesheet>
