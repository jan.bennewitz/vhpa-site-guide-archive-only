<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="application/vnd.google-earth.kml+xml"
				indent="yes"
				omit-xml-declaration="yes"
				cdata-section-elements="description" />
  <!-- doesn't seem to do anything? -->

  <xsl:param name="subset"/>
  <!-- 'All' if unspecified. freeflight, microlight, closed -->

  <xsl:key name="keyContactByName" match="contact" use="name"/>

  <xsl:template match="/">
    <kml xmlns="http://www.opengis.net/kml/2.2">
      <Document>
        <xsl:choose>
          <xsl:when test="$subset=''">
            <!--all-->
            <name>Australian National Site Guide</name>
            <open>1</open>
            <description><![CDATA[siteguide.org.au A guide to hang-gliding and paragliding sites.<br/>Ctrl-click (command-click on Mac) on a color-coded area or path (e.g. a green landing area or blue access path) for more information.]]></description>
            <xsl:call-template name="styles"/>
            <Folder>
              <name>Hang- and Paragliding Sites</name>
              <description>Hang-gliding and paragliding sites.</description>
              <xsl:apply-templates select="//siteFreeFlight[not(closed)]">
                <xsl:sort select="name"/>
              </xsl:apply-templates>
            </Folder>
            <!--<Folder>
              <name>Microlight Airfields</name>
              <description>Microlight-friendly airfields.</description>
              <xsl:apply-templates select="//siteMicrolight">
                <xsl:sort select="name"/>
              </xsl:apply-templates>
            </Folder>-->
            <Folder>
              <name>Closed Sites</name>
              <description>Closed hang-gliding and paragliding sites.</description>
              <xsl:apply-templates select="//siteFreeFlight[closed]">
                <xsl:sort select="name"/>
              </xsl:apply-templates>
            </Folder>
          </xsl:when>

          <xsl:when test="$subset='freeflight'">
            <name>Australian National Site Guide - Open hang-gliding and paragliding sites.</name>
            <description><![CDATA[siteguide.org.au A guide to hang-gliding and paragliding sites.<br/>Ctrl-click (command-click on Mac) on a color-coded area or path (e.g. a green landing area or blue access path) for more information.]]></description>
            <xsl:call-template name="styles"/>
            <xsl:apply-templates select="//siteFreeFlight[not(closed)]">
              <xsl:sort select="name"/>
            </xsl:apply-templates>
            <xsl:apply-templates select="//area/mapShape"/>
          </xsl:when>

          <!--<xsl:when test="$subset='microlight'">
            <name>Australian National Site Guide - Microlight-friendly airfields.</name>
            <description><![CDATA[siteguide.org.au A guide to hang-gliding and paragliding sites.<br/>Ctrl-click (command-click on Mac) on a color-coded area or path (e.g. a green landing area or blue access path) for more information.]]></description>
            <xsl:call-template name="styles"/>
            <xsl:apply-templates select="//siteMicrolight">
              <xsl:sort select="name"/>
            </xsl:apply-templates>
          </xsl:when>-->

          <xsl:when test="$subset='closed'">
            <name>Australian National Site Guide - Closed hang-gliding and paragliding sites.</name>
            <description><![CDATA[siteguide.org.au A guide to hang-gliding and paragliding sites.<br/>Ctrl-click (command-click on Mac) on a color-coded area or path (e.g. a green landing area or blue access path) for more information.]]></description>
            <xsl:call-template name="styles"/>
            <xsl:apply-templates select="//siteFreeFlight[closed]">
              <xsl:sort select="name"/>
            </xsl:apply-templates>
          </xsl:when>

          <xsl:otherwise>[Parameter 'subset' must be one of '', 'freeflight', 'closed']</xsl:otherwise>
        </xsl:choose>
      </Document>
    </kml>

  </xsl:template>

  <xsl:template name="styles">
    <xsl:if test="$subset='' or $subset='freeflight'">
      <Style xmlns="http://www.opengis.net/kml/2.2" id="styleLaunch">
        <IconStyle>
          <Icon>
            <href>http://siteguide.org.au/Images/Windsock.png</href>
          </Icon>
          <hotSpot x="51" y="5" xunits="pixels" yunits="pixels"/>
        </IconStyle>
        <xsl:call-template name="LabelStyle"/>
        <xsl:call-template name="BalloonStyle"/>
      </Style>
    </xsl:if>
    <xsl:if test="$subset='' or $subset='closed'">
      <Style xmlns="http://www.opengis.net/kml/2.2" id="styleClosedLaunch">
        <IconStyle>
          <Icon>
            <href>http://siteguide.org.au/Images/Windsock_closed.png</href>
          </Icon>
          <hotSpot x="51" y="5" xunits="pixels" yunits="pixels"/>
        </IconStyle>
        <xsl:call-template name="LabelStyle"/>
        <xsl:call-template name="BalloonStyle"/>
      </Style>
    </xsl:if>
    <!--<xsl:if test="$subset='' or $subset='microlight'">
      <Style xmlns="http://www.opengis.net/kml/2.2" id="styleAirfield">
        <IconStyle>
          <Icon>
            <href>http://siteguide.org.au/Images/WindsockOrange.png</href>
          </Icon>
          <hotSpot x="51" y="5" xunits="pixels" yunits="pixels"/>
        </IconStyle>
        <xsl:call-template name="LabelStyle"/>
        <xsl:call-template name="BalloonStyle"/>
      </Style>
    </xsl:if>-->
    <xsl:call-template name="mapShapeStyles"/>
  </xsl:template>

  <xsl:template name="LabelStyle">
    <LabelStyle xmlns="http://www.opengis.net/kml/2.2">
      <color>ffffffff</color>
      <scale>.8</scale>
    </LabelStyle>
  </xsl:template>

  <xsl:template name="BalloonStyle">
    <BalloonStyle xmlns="http://www.opengis.net/kml/2.2">
      <text>
        <!--<![CDATA[<table width="100%"><tr><td><h3>$[name]</h3></td><td align=right><a href="mailto:webmaster@vhpa.org.au?subject=Site%20guide%20update%20-%20$[name]">Submit update</a></td></tr></table>]]><![CDATA[<br/>]]>-->
      </text>
      <text>$[description]<![CDATA[<hr/>]]>$[geDirections]</text>
    </BalloonStyle>
  </xsl:template>

  <xsl:template name="mapShapeStyles">
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleLanding">
      <LineStyle>
        <color>ff00ff00</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>3300ff00</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleNoLanding">
      <LineStyle>
        <color>ff0000ff</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>330000ff</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleNoLaunching">
      <LineStyle>
        <color>ff007fff</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>33007fff</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleEmergencyLanding">
      <LineStyle>
        <color>ff007fff</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>33007fff</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleNoFlyZone">
      <LineStyle>
        <color>ff0000ff</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>330000ff</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleHazard">
      <LineStyle>
        <color>ff0000ff</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>330000ff</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleFeature">
      <LineStyle>
        <color>ffff0000</color>
        <width>1</width>
      </LineStyle>
      <PolyStyle>
        <color>33ff0000</color>
      </PolyStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="styleAccess">
      <LineStyle>
        <color>ffff0000</color>
        <width>1</width>
      </LineStyle>
    </Style>
    <Style xmlns="http://www.opengis.net/kml/2.2" id="stylePowerline">
      <LineStyle>
        <color>ff0000ff</color>
        <width>1</width>
      </LineStyle>
    </Style>
  </xsl:template>

  <xsl:template match="siteFreeFlight">
    <xsl:choose>
      <xsl:when test="count(launch|mapShape)=1">
        <xsl:apply-templates select="launch"/>
        <xsl:apply-templates select="mapShape"/>
      </xsl:when>
      <xsl:otherwise>
        <Folder xmlns="http://www.opengis.net/kml/2.2">
          <name>
            <xsl:value-of select="name"/>
          </name>
          <open>1</open>
          <Snippet>
            <xsl:value-of disable-output-escaping="yes" select="type"/> - <xsl:value-of select="conditions"/>
          </Snippet>
          <xsl:apply-templates select="launch"/>
          <xsl:apply-templates select="mapShape"/>
        </Folder>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="launch">
    <xsl:variable name="fullLaunchName">
      <xsl:value-of select="../name"/>
      <xsl:if test="name">
        <xsl:text> - </xsl:text>
        <xsl:value-of select="name"/>
      </xsl:if>
    </xsl:variable>
    <!--<xsl:variable name="launchName">
			<xsl:choose>
				<xsl:when test="name">
					<xsl:value-of select="name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../name"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>-->
    <xsl:variable name="conditions">
      <xsl:choose>
        <xsl:when test="conditions">
          <xsl:value-of select="conditions"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="../conditions"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <Placemark xmlns="http://www.opengis.net/kml/2.2">
      <!--<xsl:attribute name="id">
				<xsl:value-of select="$fullLaunchName"/>
			</xsl:attribute>-->
      <name>
        <xsl:value-of select="$fullLaunchName"/>
      </name>
      <Snippet>
        <xsl:value-of select="../type"/> - <xsl:value-of select="$conditions"/>
      </Snippet>
      <description>
        <xsl:text><![CDATA[<table width="100%"><tr><td><h3><a href="http://siteguide.org.au/Sites/]]></xsl:text>
        <xsl:value-of select="../name"/>
        <xsl:text><![CDATA[.html">]]></xsl:text>
        <xsl:value-of select="$fullLaunchName"/>
        <xsl:text><![CDATA[</a></h3></td><td align=right><a href="mailto:webmaster@vhpa.org.au?subject=Site%20guide%20update%20-%20]]></xsl:text>
        <xsl:value-of select="$fullLaunchName"/>
        <xsl:text><![CDATA[">Submit update</a></td></tr></table>]]></xsl:text>
        <xsl:if test="../closed">
          <xsl:text><![CDATA[<font color="red"><b>This site is closed.</b> </font>]]></xsl:text>
          <xsl:value-of select="../closed"/>
          <xsl:text><![CDATA[<br/>]]></xsl:text>
        </xsl:if>
        <xsl:text><![CDATA[<hr/><table width="100%"><tr><td>]]></xsl:text>
        <xsl:value-of select="../type"/>
        <xsl:text><![CDATA[</td><td>]]></xsl:text>
        <xsl:value-of select="../height"/>
        <xsl:text><![CDATA[</td></tr><tr><td>]]></xsl:text>
        <xsl:value-of select="$conditions"/>
        <xsl:text><![CDATA[</td><td>]]></xsl:text>
        <xsl:value-of select="../rating"/>
        <xsl:text><![CDATA[</td></tr></table><hr/>]]></xsl:text>
        <xsl:if test="not(../closed)">
          <xsl:value-of select="../description"/>
          <xsl:if test="../restrictions">
            <xsl:text><![CDATA[<br/><br/><img alt="Restrictions:" title="Restrictions" src="http://siteguide.org.au/Images/alert.png" style="margin-right: .3em;"/>]]></xsl:text>
            <xsl:value-of select="../restrictions"/>
          </xsl:if>
          <xsl:if test="../takeoff or description">
            <xsl:text><![CDATA[<br/><br/><b>Takeoff</b><br/>]]></xsl:text>
            <xsl:value-of select="../takeoff"/>
            <xsl:if test="description and ../takeoff">
              <xsl:text><![CDATA[<br/><br/>]]></xsl:text>
            </xsl:if>
            <xsl:if test="description">
              <xsl:text><![CDATA[<u>]]></xsl:text>
              <xsl:value-of select="name"/>
              <xsl:text><![CDATA[</u>: ]]></xsl:text>
              <xsl:value-of select="description"/>
            </xsl:if>
          </xsl:if>
          <xsl:if test="../landing">
            <xsl:text><![CDATA[<br/><br/><b>Landing</b><br/>]]></xsl:text>
            <xsl:value-of select="../landing"/>
          </xsl:if>
          <xsl:if test="../flightcomments">
            <xsl:text><![CDATA[<br/><br/><b>Flight</b><br/>]]></xsl:text>
            <xsl:value-of select="../flightcomments"/>
          </xsl:if>
          <xsl:if test="../hazardscomments">
            <xsl:text><![CDATA[<br/><br/><b>Hazards/Comments</b><br/>]]></xsl:text>
            <xsl:value-of select="../hazardscomments"/>
          </xsl:if>
          <!-- no need for smallmap inside Google Earth
						<![CDATA[<hr />
									<img alt="" title="Site map" src="http://siteguide.org.au/Site%20Maps/]]><xsl:value-of select="../smallmap"/><![CDATA["/>
									<br/>]]>-->
          <xsl:if test="../extendedlocation">
            <xsl:text><![CDATA[<br/><br/><b>Location</b><br/>]]></xsl:text>
            <xsl:value-of select="../extendedlocation"/>
          </xsl:if>
          <xsl:if test="estaEmergencyMarker">
            <xsl:text><![CDATA[<br/><br/><u>ESTA 000 Emergency Marker</u> (<a href="https://www.esta.vic.gov.au/emergency-markers" title="Emergency markers pinpoint your exact location during an emergency in public open spaces or a hard to define locality. Click to learn more.">?</a>): ]]></xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="estaEmergencyMarker"/>
          </xsl:if>
          <xsl:if test="../smallpicture">
            <xsl:text><![CDATA[<br/><br/><img alt="" title="]]></xsl:text>
            <xsl:choose>
              <xsl:when test="smallpicturetitle">
                <xsl:value-of select="../smallpicturetitle"/>
              </xsl:when>
              <xsl:otherwise>Site picture</xsl:otherwise>
            </xsl:choose>
            <xsl:text><![CDATA[" src="http://siteguide.org.au/Site%20Photos/]]></xsl:text>
            <xsl:value-of select="../smallpicture"/>
            <xsl:text><![CDATA["/><br/>]]></xsl:text>
          </xsl:if>
          <xsl:text><![CDATA[<hr>]]></xsl:text>
          <xsl:choose>
            <xsl:when test="../landowners">
              <xsl:text><![CDATA[<b>Landowners: </b>]]></xsl:text>
              <xsl:value-of select="../landowners"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text><![CDATA[<b>Landowners - ?</b>]]></xsl:text>
            </xsl:otherwise>
          </xsl:choose>

          <!--check if contact and responsible entries are the same - '***' acts as a spacer between contacts. Might be a better way for this? -->
          <xsl:variable name="contact">
            <xsl:for-each select="../contactName">
              <xsl:value-of select="." />
              <xsl:text>***</xsl:text>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="responsible">
            <xsl:for-each select="../responsibleName">
              <xsl:value-of select="." />
              <xsl:text>***</xsl:text>
            </xsl:for-each>
          </xsl:variable>

          <xsl:choose>
            <xsl:when test="$contact = $responsible">
              <xsl:call-template name="contactlist">
                <xsl:with-param name="title" select="'Contact/Responsible'"/>
                <xsl:with-param name="contacts" select="../contactName"/>
              </xsl:call-template>
            </xsl:when>

            <xsl:otherwise>
              <xsl:call-template name="contactlist">
                <xsl:with-param name="title" select="'Contact'"/>
                <xsl:with-param name="contacts" select="../contactName"/>
              </xsl:call-template>

              <xsl:call-template name="contactlist">
                <xsl:with-param name="title" select="'Responsible'"/>
                <xsl:with-param name="contacts" select="../responsibleName"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="../seeAlso">
            <![CDATA[<br/><b>See also</b>:&nbsp;]]>
            <a xmlns="http://www.w3.org/1999/xhtml">
              <xsl:attribute name="href">
                <xsl:value-of select="../seeAlso/link"/>
              </xsl:attribute>
              <xsl:value-of select="../seeAlso/name"/>
            </a>
          </xsl:if>
        </xsl:if>
      </description>
      <xsl:if test="view_range">
        <LookAt>
          <longitude>
            <xsl:value-of select="view_lon" />
          </longitude>
          <latitude>
            <xsl:value-of select="view_lat" />
          </latitude>
          <heading>
            <xsl:value-of select="view_heading" />
          </heading>
          <tilt>
            <xsl:value-of select="view_tilt" />
          </tilt>
          <range>
            <xsl:value-of select="view_range" />
          </range>
        </LookAt>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="../closed">
          <styleUrl>#styleClosedLaunch</styleUrl>
        </xsl:when>
        <xsl:otherwise>
          <styleUrl>#styleLaunch</styleUrl>
        </xsl:otherwise>
      </xsl:choose>
      <Point>
        <coordinates>
          <xsl:value-of select="lon" />,<xsl:value-of select="lat" />
        </coordinates>
      </Point>
    </Placemark>
  </xsl:template>

  <xsl:template match="mapShape">
    <xsl:for-each select="coordinates">
      <Placemark xmlns="http://www.opengis.net/kml/2.2">
        <name>
          <xsl:text>(</xsl:text><xsl:value-of select="../@category"/>)<xsl:if test="../name">
            <xsl:text> </xsl:text>
          </xsl:if><xsl:value-of select="../name"/>
        </name>
        <xsl:if test="../description or ../estaEmergencyMarker">
          <description>
            <xsl:if test="../description">
              <xsl:value-of select="../description"/>
            </xsl:if>
            <xsl:if test="../description and ../estaEmergencyMarker">
              <xsl:text><![CDATA[<br/><br/>]]></xsl:text>
            </xsl:if>
            <xsl:if test="../estaEmergencyMarker">
              <xsl:text><![CDATA[<b>ESTA 000 Emergency Marker</b> (<a href="https://www.esta.vic.gov.au/emergency-markers" title="Emergency markers pinpoint your exact location during an emergency in public open spaces or a hard to define locality. Click to learn more.">?</a>): ]]></xsl:text>
              <xsl:value-of select="../estaEmergencyMarker"/>
            </xsl:if>
          </description>
        </xsl:if>
        <styleUrl>
          <xsl:choose>
            <xsl:when test="../@category = 'No Landing'">#styleNoLanding</xsl:when>
            <xsl:when test="../@category = 'No Launching'">#styleNoLaunching</xsl:when>
            <xsl:when test="../@category = 'Emergency Landing'">#styleEmergencyLanding</xsl:when>
            <xsl:when test="../@category = 'No Fly Zone'">#styleNoFlyZone</xsl:when>
            <xsl:when test="../@category = 'Hazard'">#styleHazard</xsl:when>
            <xsl:when test="../@category = 'Feature'">#styleFeature</xsl:when>
            <xsl:otherwise>
              <xsl:text>#style</xsl:text>
              <xsl:value-of select="../@category"/>
            </xsl:otherwise>
          </xsl:choose>
        </styleUrl>
        <xsl:choose>
          <xsl:when test="../@category = 'Access' or ../@category = 'Powerline'">
            <LineString>
              <altitudeMode>
                <xsl:text>clampToGround</xsl:text>
              </altitudeMode>
              <coordinates>
                <xsl:value-of select="."/>
              </coordinates>
            </LineString>
          </xsl:when>
          <xsl:otherwise>
            <Polygon>
              <altitudeMode>clampToGround</altitudeMode>
              <outerBoundaryIs>
                <LinearRing>
                  <coordinates>
                    <xsl:value-of select="."/>
                  </coordinates>
                </LinearRing>
              </outerBoundaryIs>
            </Polygon>
          </xsl:otherwise>
        </xsl:choose>
      </Placemark>
    </xsl:for-each>
  </xsl:template>

  <!--<xsl:template match="siteMicrolight">
    <Placemark xmlns="http://www.opengis.net/kml/2.2">
      --><!--<xsl:attribute name="id">
				<xsl:value-of select="name"/>
			</xsl:attribute>--><!--
      <name>
        <xsl:value-of select="name"/>
      </name>
      <description>
        <xsl:value-of select="facilities"/>
      </description>
      <styleUrl>#styleAirfield</styleUrl>
      <Point>
        <coordinates>
          <xsl:value-of select="lon" />,<xsl:value-of select="lat" />
        </coordinates>
      </Point>
      <xsl:if test="view_range">
        <LookAt>
          <longitude>
            <xsl:value-of select="view_lon" />
          </longitude>
          <latitude>
            <xsl:value-of select="view_lat" />
          </latitude>
          <heading>
            <xsl:value-of select="view_heading" />
          </heading>
          <tilt>
            <xsl:value-of select="view_tilt" />
          </tilt>
          <range>
            <xsl:value-of select="view_range" />
          </range>
        </LookAt>
      </xsl:if>
    </Placemark>
  </xsl:template>-->

  <xsl:template name="contactlist">
    <xsl:param name="title"/>
    <xsl:param name="contacts"/>

    <xsl:choose>
      <xsl:when test="$contacts">
        <![CDATA[<br/><b>]]>
        <xsl:value-of select="$title"/>
        <![CDATA[</b>: ]]>
        <xsl:for-each select="key('keyContactByName', $contacts)">
          <xsl:apply-templates select="."/>
          <xsl:if test="not(position()=last())">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <![CDATA[<br/><b>]]>
        <xsl:value-of select="$title"/> - ?
        <![CDATA[</b>]]>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="contact">

    <xsl:choose>
      <xsl:when test="link">
        <![CDATA[<a href=']]><xsl:value-of select="link"/><![CDATA['>]]><xsl:value-of select="name"/><![CDATA[</a>]]>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="name"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="phone">
      <xsl:text> (</xsl:text>
      <xsl:for-each select="phone">
        <xsl:value-of select="."/>
        <xsl:if test="not(position()=last())">, </xsl:if>
      </xsl:for-each>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:if test="address">
      <xsl:choose>
        <xsl:when test="phone">, </xsl:when>
        <xsl:otherwise> </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="address"/>
    </xsl:if>


  </xsl:template>

</xsl:stylesheet>