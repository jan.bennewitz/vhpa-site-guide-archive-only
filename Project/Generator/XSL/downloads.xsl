<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>

  <xsl:param name="maxSitesPerPage" select="20"/>

  <xsl:include href="common.xsl"/>

  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <xsl:call-template name="common_head">
        <xsl:with-param name="title">Australian National Site Guide - Print or Download</xsl:with-param>
      </xsl:call-template>
      <body>
        <script>
          window.cookieconsent_options = {
          message: "Use of the Australian National Site Guide is covered by our disclaimer.",
          learnMore: 'Read the disclaimer here.',
          link: 'disclaimer.html',
          dismiss : "Got it.",
          theme: "dark-floating",
          expiryDays: 99999,
          };
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <xsl:call-template name="common_menu-top"/>
        <xsl:call-template name="common_header_site-guide"/>
        <div id="wrapper-content">
          <div id="wrapper-content2">
            <xsl:call-template name="common_menu-page_siteguide"/>

            <div class="content">
              <h2>Print or Download the Site Guide</h2>
              <p>To limit the size of individual files and printouts, the guide is broken up into the following sections:</p>
              <ul>
                <!--								<xsl:for-each select="siteGuideData//area[.//siteFreeFlight[not(closed)] and ((count(.//siteFreeFlight[not(closed)]) &lt;= $maxSitesPerPage and (count(..//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage or parent::siteGuideData)) or count(.//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage and not(area))]">-->
                <xsl:for-each select="siteGuideData/area">
                  <xsl:sort select="@name"/>
                  <xsl:call-template name="area"/>
                </xsl:for-each>
              </ul>
              <h2>Other downloads</h2>
              <ul>
                <li>
                  Kmz file to view the site guide in Google Earth: 
                  <a href="siteguide.kmz" title="Google Earth kmz">
                    <img alt="" src="Images/GEIcon.png"/>
                    <xsl:text>Google Earth kmz</xsl:text>
                  </a>
                </li>
                <li>
                  Landings, no landing zones etc. in OpenAir format for flight instruments, XCSoar etc.:
                  <a href="siteGuide.OpenAir.txt" download="siteGuide.OpenAir.txt" title="OpenAir file">
                    <img alt="" src="Images/OA.png" />
                    <xsl:text>OpenAir file</xsl:text>
                  </a>
                </li>
                <li>
                  Landings, no landing zones etc. in OziExplorer wpt format for flight instruments, XSSoar, GPS receivers etc.:
                  <a href="siteGuide.OziExplorer.wpt" download="siteGuide.OziExplorer.wpt" title="OziExplorer wpt file">
                    <img alt="" src="Images/ozi.png" />
                    <xsl:text>OziExplorer wpt file</xsl:text>
                  </a>
                </li>
              </ul>
              <xsl:call-template name="common_contribute"/>
            </div>
          </div>
        </div>
        <xsl:call-template name="common_footer"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="area">
    <xsl:choose>
      <xsl:when test=".//siteFreeFlight[not(closed)] and ((count(.//siteFreeFlight[not(closed)]) &lt;= $maxSitesPerPage and (count(..//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage or parent::siteGuideData)) or count(.//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage and not(area))">
        <li xmlns="http://www.w3.org/1999/xhtml">
          <a>
            <xsl:attribute name="href">
              <xsl:text>Site%20Guide%20-%20</xsl:text>
              <xsl:call-template name="common_areaQualifiedName"/>
              <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:call-template name="common_areaQualifiedName">
              <xsl:with-param name="full" select="'Y'"/>
            </xsl:call-template>
          </a>
          <xsl:text> (</xsl:text>
          <xsl:value-of select="count(.//siteFreeFlight[not(closed)])"/>
          <xsl:text> site</xsl:text>
          <xsl:if test="count(.//siteFreeFlight[not(closed)]) >1">
            <xsl:text>s</xsl:text>
          </xsl:if>
          <xsl:text>)</xsl:text>
        </li>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="area">
          <xsl:sort select="@name"/>
          <xsl:call-template name="area"/>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>