<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>

  <xsl:template name="common_head">
    <xsl:param name="title"/>
    <xsl:param name="clickableTableRows"/>

    <head xmlns="http://www.w3.org/1999/xhtml">
      <title>
        <xsl:value-of select="$title"/>
      </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

      <link rel="stylesheet" type="text/css" href="style.css" />
      <link rel="stylesheet" type="text/css" media="print" href="print.css" />
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
      <link rel="icon" href="favicon.ico" type="image/x-icon" />
      <meta name="description" content="Australian National Site Guide including schools and clubs" />
      <meta name="keywords" content="Australia Victoria paragliding hang gliding hanggliding microlight flying VHPA Australian National Site Guide clubs schools committee association" />

      <xsl:if test="$clickableTableRows">
        <script type="text/javascript" src="clickableTableRows.js"></script>
      </xsl:if>
    </head>
  </xsl:template>

  <xsl:template name="common_menu-top">
    <xsl:param name="path"/>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-65415760-2', 'auto');
      ga('send', 'pageview');
    </script>
    <div xmlns="http://www.w3.org/1999/xhtml" id="wrapper-menu-top">
      <div id="menu-top">
        <ul>
          <xsl:call-template name="common_menu-top-elem">
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title" select="'Home'"/>
            <xsl:with-param name="href" select="'index.html'"/>
          </xsl:call-template>
          <xsl:call-template name="common_menu-top-elem">
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title" select="'Map'"/>
            <xsl:with-param name="href" select="'siteguidemap.html'"/>
          </xsl:call-template>
          <xsl:call-template name="common_menu-top-elem">
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title" select="'Sites Index'"/>
            <xsl:with-param name="href" select="'sitesindex.html'"/>
          </xsl:call-template>
          <xsl:call-template name="common_menu-top-elem">
            <xsl:with-param name="path" select="$path"/>
            <xsl:with-param name="title" select="'Downloads'"/>
            <xsl:with-param name="href" select="'downloads.html'"/>
          </xsl:call-template>
        </ul>
      </div>
      <!--menu-top-->
    </div>
    <!--wrapper-menu-top-->
  </xsl:template>

  <xsl:template name="common_menu-top-elem">
    <xsl:param name="path"/>
    <xsl:param name="title"/>
    <xsl:param name="href"/>
    <li xmlns="http://www.w3.org/1999/xhtml">
      <a>
        <!--<xsl:attribute name="title"><xsl:value-of select="$title"/></xsl:attribute>-->
        <xsl:attribute name="href">
          <xsl:value-of select="$path"/>
          <xsl:value-of select="$href"/>
        </xsl:attribute>
        <span>
          <xsl:value-of select="$title"/>
        </span>
      </a>
    </li>
  </xsl:template>

  <xsl:template name="common_header_site-guide">
    <xsl:param name="title"/>
    <div xmlns="http://www.w3.org/1999/xhtml" id="wrapper-header">
      <div id="header">
        <div id="wrapper-header2">
          <div id="wrapper-header3">
            <h1>
              <xsl:text>Australian National Site Guide</xsl:text>
              <xsl:if test="$title">
                <xsl:text> - </xsl:text>
                <xsl:value-of select="$title"/>
              </xsl:if>
            </h1>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="common_contribute">
    <p xmlns="http://www.w3.org/1999/xhtml">
      Please contribute by sending updated or new site information to
      the <a href="mailto:webmaster@vhpa.org.au?subject=Site%20guide%20update">webmaster</a>.
    </p>
  </xsl:template>

  <!--<xsl:template name="common_menu-page_siteguide">
    <xsl:param name="mapType" select="'open'"/>
    <div xmlns="http://www.w3.org/1999/xhtml" id="wrapper-menu-page">
      <div id="menu-page">
        <xsl:call-template name="common_private_siteguide-index">
          <xsl:with-param name="mapType" select="$mapType"/>
        </xsl:call-template>
      </div>
      -->
  <!--menu-page-->
  <!--
    </div>
  </xsl:template>-->

  <xsl:template name="common_menu-page_siteguide">
    <xsl:param name="mapType" select="'open'"/>
    <div xmlns="http://www.w3.org/1999/xhtml" id="wrapper-menu-page">
      <div id="menu-page">
        <!--<xsl:call-template name="common_private_siteguide-index">
          <xsl:with-param name="mapType" select="$mapType"/>
        </xsl:call-template>
        <br />
        <br />-->
        <h3>
          External Links
        </h3>
        <ul>
          <li>
            <a title="Hang Gliding Federation of Australia" href="http://www.hgfa.asn.au">HGFA</a>
          </li>
          <li>
            <a title="Victorian Hang-gliding and Paragliding Association" href="http://www.vhpa.org.au">VHPA</a>
          </li>
          <li>
            <a title="NSW Hang-gliding and Paragliding Association" href="http://www.nswhpa.org">NSWHPA</a>
          </li>
          <li>
            <a title="North Queensland Hang-Gliding Association" href="http://www.cairnshangglidingclub.org">NQHGA</a>
          </li>
          <li>
            <a title="South Queensland Hang-Gliding Association" href="https://www.hgfa.asn.au/club-info/SEQLD">SQHGA</a>
          </li>
          <li>
            <a title="South Australian Hang-Gliding Association" href="https://sites.google.com/site/sahpgahome">SAHGA</a>
          </li>
          <li>
            <a title="Tasmanian Hang-gliding and Paragliding Association" href="http://www.thpa.org.au">THPA</a>
          </li>
          <li>
            <a title="Hang-Gliding Association of Western Australia" href="http://flywa.com.au/">HGAWA</a>
          </li>
          <li>
            <a href="https://wheretofly.info">"Where to Fly" guide</a>
          </li>
        </ul>
      </div>
      <!--menu-page-->
    </div>
  </xsl:template>

  <xsl:template name="common_footer">
    <xsl:param name="path"/>
    <div xmlns="http://www.w3.org/1999/xhtml" id="wrapper-footer">
      <div id="footer">
        <a href="mailto:Webmaster@vhpa.org.au">Webmaster</a>
        <xsl:text> | </xsl:text>
        <a>
          <xsl:attribute name="href">
            <xsl:if test="$path">
              <xsl:value-of select="$path"/>
              <xsl:text>/</xsl:text>
            </xsl:if>
            <xsl:text>credit.html</xsl:text>
          </xsl:attribute>
          <xsl:text>Credits</xsl:text>
        </a>
        <xsl:text> | </xsl:text>
        <a>
          <xsl:attribute name="href">
            <xsl:if test="$path">
              <xsl:value-of select="$path"/>
              <xsl:text>/</xsl:text>
            </xsl:if>
            <xsl:text>disclaimer.html</xsl:text>
          </xsl:attribute>
          <xsl:text>Disclaimer</xsl:text>
        </a>
      </div>
    </div>
  </xsl:template>

  <!--<xsl:template name ="common_private_siteguide-index">
    <xsl:param name="mapType" select="'open'"/>
    <h3 xmlns="http://www.w3.org/1999/xhtml">Site guide</h3>
    <ul xmlns="http://www.w3.org/1999/xhtml" id="siteguideIndex">
      <li>
        <a href="index.html" title="Site Guide Overview">
          <img alt="" src="Images/Transparent_16.png"/>
          <xsl:text>Overview</xsl:text>
        </a>
      </li>
      <li>
        <a title="Interactive map of all sites">
          <xsl:attribute name="href">
            <xsl:text>siteguidemap.html?type=</xsl:text>
            <xsl:value-of select="$mapType"/>
          </xsl:attribute>
          <img alt="" src="Images/mm_16_green.png"/>
          <xsl:text>Map</xsl:text>
        </a>
      </li>
      <li>
        <a href="sitesindex.html" title="List of all freeflying sites">
          <img alt="" src="Images/Windsock_16.png"/>
          <xsl:text>Sites Index</xsl:text>
        </a>
      </li>
      -->
  <!--<li>
        <a href="sitesmicrolightindex.html" title="List of all microlight sites">
          <img alt="" src="Images/WindsockOrange_16.png"/>
          <xsl:text>Microlight Sites Index</xsl:text>
        </a>
      </li>-->
  <!--
      <li>
        <a href="siteguide.kmz" title="View the site guide in Google Earth. You will need Google Earth to view this kmz file.">
          <img alt="" src="Images/GEIcon.png"/>
          <xsl:text>View in Google Earth</xsl:text>
        </a>
      </li>
      <li>
        <a href="downloads.html" title="All sites on one page - print the guide, or save it to your computer.">
          <img alt="" src="Images/PrintIcon.png"/>
          <xsl:text>Download or Print</xsl:text>
        </a>
      </li>
      <li>
        <a href="siteGuide.OpenAir.txt" download="siteGuide.OpenAir.txt" title="Landings, no landing zones etc. in OpenAir format for flight instruments, XCSoar etc.">
          <img alt="" src="Images/OA.png" />
          <xsl:text>OpenAir file</xsl:text>
        </a>
      </li>
      <li>
        <a href="siteGuide.OziExplorer.wpt" download="siteGuide.OziExplorer.wpt" title="Landings, no landing zones etc. in OziExplorer wpt format for flight instruments, GPS receivers etc.">
          <img alt="" src="Images/ozi.png" />
          <xsl:text>OziExplorer wpt file</xsl:text>
        </a>
      </li>

    </ul>
  </xsl:template>-->

  <xsl:template name="common_sitemap">
    <xsl:param name="path"/>
    <xsl:param name="zoomLevel" select="5"/>

    <a xmlns="http://www.w3.org/1999/xhtml">
      <xsl:attribute name="href">
        <xsl:call-template name="common_siteMapHref">
          <xsl:with-param name="path" select="$path"/>
        </xsl:call-template>
      </xsl:attribute>
      <img alt="Overview map" title="Overview map">
        <xsl:attribute name="class">
          <xsl:choose>
            <xsl:when test="insetMapPosition='Bottom Left'">insetMapBottomLeft</xsl:when>
            <xsl:otherwise>insetMap</xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="src">
          <xsl:text>https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&amp;zoom=</xsl:text>
          <xsl:value-of select="$zoomLevel"/>
          <xsl:text>&amp;size=80x80&amp;markers=icon:http://siteguide.org.au/Images/mm_20_green.png|</xsl:text>
          <xsl:call-template name="common_launchesCenterpoint"/>
        </xsl:attribute>
      </img>
      <img alt="Site map" title="Site map - click for interactive map">
        <xsl:attribute name="src">
          <xsl:choose>
            <xsl:when test="smallmap">
              <xsl:value-of select="$path"/>
              <xsl:text>Site%20Maps/</xsl:text>
              <xsl:value-of select="smallmap"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&amp;visible=</xsl:text>
              <xsl:call-template name="common_launchesVisible">
                <xsl:with-param name="minSpan" select=".1"/>
              </xsl:call-template>
              <xsl:text>&amp;size=270x200&amp;maptype=terrain&amp;markers=</xsl:text>
              <xsl:call-template name="common_launchesStaticMarkers"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </img>
    </a>
  </xsl:template>

  <xsl:template name="common_siteMapHref">
    <xsl:param name="path"/>

    <xsl:value-of select="$path"/>
    <xsl:text>siteguidemap.html?type=open</xsl:text>
    <xsl:if test="closed">,closed</xsl:if>
    <xsl:text>&amp;</xsl:text>
    <xsl:choose>
      <xsl:when test="count(launch)=1">
        <xsl:text>pin=</xsl:text>
        <xsl:value-of select="name"/>
        <xsl:if test="launch/name">
          <xsl:text> - </xsl:text>
          <xsl:value-of select="launch/name"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>spn=</xsl:text>
        <xsl:call-template name="common_launchesSpan">
          <xsl:with-param name="minSpan" select=".2"/>
        </xsl:call-template>
        <xsl:text>&amp;ll=</xsl:text>
        <xsl:call-template name="common_launchesCenterpoint"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="common_launchesCenterpoint">
    <!--<xsl:value-of select="(max(.//launch/lat) + min(.//launch/lat)) div 2" />,<xsl:value-of select="(max(.//launch/lon) + min(.//launch/lon)) div 2" />-->
    <xsl:variable name="minLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="minLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="round(($maxLat + $minLat) div 2 * 100000) div 100000"/>,<xsl:value-of select="round(($maxLon + $minLon) div 2 * 100000) div 100000"/>
  </xsl:template>

  <xsl:template name="common_launchesSpan">
    <xsl:param name="minSpan" select="0"/>

    <xsl:variable name="minLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="minLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="spanLat">
      <xsl:value-of select="round(($maxLat - $minLat) * 100000) div 100000"/>
    </xsl:variable>
    <xsl:variable name="spanLon">
      <xsl:value-of select="round(($maxLon - $minLon) * 100000) div 100000"/>
      <!--This will not deal with spans that straddle the 180 meridian-->
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$spanLat > $minSpan">
        <xsl:value-of select="$spanLat"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$minSpan"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>,</xsl:text>
    <xsl:choose>
      <xsl:when test="$spanLon > $minSpan">
        <xsl:value-of select="$spanLon"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$minSpan"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="common_launchesVisible">
    <xsl:param name="minSpan" select="0"/>

    <xsl:variable name="minLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLat">
      <xsl:for-each select=".//launch/lat">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="minLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="ascending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="maxLon">
      <xsl:for-each select=".//launch/lon">
        <xsl:sort data-type="number" order="descending" select="."/>
        <xsl:if test="position()=1">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="spanLat">
      <xsl:choose>
        <xsl:when test="$maxLat - $minLat > $minSpan">
          <xsl:value-of select="$maxLat - $minLat"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$minSpan"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="spanLon">
      <xsl:choose>
        <xsl:when test="$maxLon - $minLon > $minSpan">
          <!--This will not deal with spans that straddle the 180 meridian-->
          <xsl:value-of select="$maxLon - $minLon"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$minSpan"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="round(($maxLat + $minLat - $spanLat) div 2 * 100000) div 100000"/>,<xsl:value-of select="round(($maxLon + $minLon - $spanLon) div 2 * 100000) div 100000"/>
    <xsl:text>|</xsl:text>
    <xsl:value-of select="round(($maxLat + $minLat + $spanLat) div 2 * 100000) div 100000"/>,<xsl:value-of select="round(($maxLon + $minLon + $spanLon) div 2 * 100000) div 100000"/>
  </xsl:template>

  <xsl:template name="common_launchesStaticMarkers">
    <xsl:text>icon:http://siteguide.org.au/Images/mm_20_green.png|</xsl:text>
    <xsl:for-each select=".//launch">
      <xsl:if test="position()>1">|</xsl:if>
      <xsl:value-of select="lat" />,<xsl:value-of select="lon" />
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="common_areaQualifiedName">
    <xsl:param name="full"/>
    <xsl:param name="separator"  select="' - '"/>

    <xsl:if test="parent::area">
      <xsl:for-each select="..">
        <xsl:call-template name="common_areaQualifiedName">
          <xsl:with-param name="full" select="$full"/>
          <xsl:with-param name="separator" select="$separator"/>
        </xsl:call-template>
        <xsl:value-of select="$separator"/>
      </xsl:for-each>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="@fullName and $full">
        <xsl:value-of select="@fullName"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@name"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="common_areaOrSiteId">
    <xsl:variable name="fullName">
      <xsl:choose>
        <xsl:when test="self::area">
          <xsl:text>a</xsl:text>
          <!--Prefix 'a' for area-->
          <xsl:call-template name="common_areaQualifiedName">
            <xsl:with-param name="separator" select="'_'"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <!--assume site-->
          <xsl:text>s</xsl:text>
          <!--Prefix 's' for site-->
          <xsl:for-each select="..">
            <xsl:call-template name="common_areaQualifiedName">
              <xsl:with-param name="separator" select="'_'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>_</xsl:text>
          <xsl:value-of select="name"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="translateFrom">
      <xsl:text> (),'’</xsl:text>
    </xsl:variable>
    <xsl:value-of select="translate($fullName, $translateFrom, '_')"/>
  </xsl:template>

  <xsl:template name="common_areaFullName">
    <xsl:choose>
      <xsl:when test="@fullName">
        <xsl:value-of select="@fullName"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@name"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="common_mapShapeMap">
    <xsl:param name="size" select="445"/>
    <a xmlns="http://www.w3.org/1999/xhtml">
      <xsl:attribute name="href">
        <xsl:call-template name="common_siteMapHref">
          <xsl:with-param name="path" select="'../'"/>
        </xsl:call-template>
      </xsl:attribute>
      <img alt="map">
        <xsl:attribute name="src">
          <xsl:text>https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&amp;size=</xsl:text>
          <xsl:value-of select="$size"/>
          <xsl:text>x</xsl:text>
          <xsl:value-of select="$size"/>
          <xsl:text>&amp;maptype=hybrid</xsl:text>
          <!--          <xsl:choose>
            <xsl:when test="self::mapShape">
              <xsl:call-template name="mapShapeStaticMapPath">
                <xsl:with-param name="level" select="'detail'"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>-->
          <xsl:text>&amp;markers=icon:http://siteguide.org.au/Images/mm_20_green.png</xsl:text>
          <xsl:value-of select="mapShapesUrl"/>
          <!--              <xsl:for-each select="launch">
                <xsl:text>|</xsl:text>
                <xsl:value-of select="lat"/>
                <xsl:text>,</xsl:text>
                <xsl:value-of select="lon"/>
              </xsl:for-each>
              <xsl:for-each select="mapShape">
                <xsl:call-template name="mapShapeStaticMapPath">
                  <xsl:with-param name="level" select="'overview'"/>
                </xsl:call-template>
              </xsl:for-each>
-->
          <!--            </xsl:otherwise>
          </xsl:choose>-->
        </xsl:attribute>
      </img>
    </a>
  </xsl:template>

  <xsl:template name="mapShapeStaticMapPath">
    <xsl:param name="level"/>
    <xsl:variable name="shapeDim">
      <xsl:choose>
        <xsl:when test='@category = "Access" or @category = "Powerline"'>1</xsl:when>
        <xsl:otherwise>2</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="shapeColor">
      <xsl:choose>
        <xsl:when test="@category = 'Landing'">00ff00</xsl:when>
        <xsl:when test="@category = 'No Landing'">ff0000</xsl:when>
        <xsl:when test="@category = 'Emergency Landing'">ff7f00</xsl:when>
        <xsl:when test="@category = 'No Fly Zone'">ff0000</xsl:when>
        <xsl:when test="@category = 'Hazard'">ff0000</xsl:when>
        <xsl:when test="@category = 'Feature'">0000ff</xsl:when>
        <xsl:when test="@category = 'Access'">0000ff</xsl:when>
        <xsl:when test="@category = 'Powerline'">ff0000</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:for-each select="encodedCoordinates[@level=$level]">
      <xsl:text>&amp;path=weight:1%7C</xsl:text>
      <xsl:if test="$shapeDim = 2">
        <xsl:text>fillcolor:0x</xsl:text>
        <xsl:value-of select="$shapeColor"/>
        <xsl:text>33%7C</xsl:text>
      </xsl:if>
      <xsl:text>color:0x</xsl:text>
      <xsl:value-of select="$shapeColor"/>
      <xsl:choose>
        <xsl:when test="$shapeDim = 1">99</xsl:when>
        <xsl:otherwise>66</xsl:otherwise>
      </xsl:choose>
      <xsl:text>%7Cenc:</xsl:text>
      <xsl:value-of select="."/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="common_estaEmergencyMarker">
    <xsl:variable name="element"/>
    <li xmlns="http://www.w3.org/1999/xhtml">
      <xsl:choose>
        <xsl:when test="self::launch">
          <xsl:value-of disable-output-escaping="yes" select="estaEmergencyMarker"/>
          <xsl:text>: </xsl:text>
          <xsl:choose>
            <xsl:when test="name">
              <xsl:value-of disable-output-escaping="yes" select="name"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>Launch</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="self::mapShape">
          <xsl:value-of disable-output-escaping="yes" select="estaEmergencyMarker"/>
          <xsl:text>: </xsl:text>
          <xsl:value-of select="@category"/>
          <xsl:if test="name">
            <xsl:text> - </xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="name"/>
          </xsl:if>
        </xsl:when>
      </xsl:choose>
    </li>
  </xsl:template>

  <!--<xsl:template name="common_url-encode">
		<xsl:param name="str"/>

		-->
  <!-- ISO-8859-1 based URL-encoding demo
       Written by Mike J. Brown, mike@skew.org.
       Updated 2002-05-20.

       No license; use freely, but credit me if reproducing in print.

       Also see http://skew.org/xml/misc/URI-i18n/ for a discussion of
       non-ASCII characters in URIs.
  -->
  <!--

		-->
  <!-- The string to URL-encode.
       Note: By "iso-string" we mean a Unicode string where all
       the characters happen to fall in the ASCII and ISO-8859-1
       ranges (32-126 and 160-255) -->
  <!--

		-->
  <!-- Characters we'll support.
       We could add control chars 0-31 and 127-159, but we won't. -->
  <!--
		<xsl:variable name="ascii"> !"#$%&amp;'()*+,-./0123456789:;&lt;=&gt;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~</xsl:variable>
		<xsl:variable name="latin1"> ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ</xsl:variable>

		-->
  <!-- Characters that usually don't need to be escaped -->
  <!--
		<xsl:variable name="safe">!'()*-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~</xsl:variable>

		<xsl:variable name="hex">0123456789ABCDEF</xsl:variable>

		<xsl:if test="$str">
			<xsl:variable name="first-char" select="substring($str,1,1)"/>
			<xsl:choose>
				<xsl:when test="contains($safe,$first-char)">
					<xsl:value-of select="$first-char"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="codepoint">
						<xsl:choose>
							<xsl:when test="contains($ascii,$first-char)">
								<xsl:value-of select="string-length(substring-before($ascii,$first-char)) + 32"/>
							</xsl:when>
							<xsl:when test="contains($latin1,$first-char)">
								<xsl:value-of select="string-length(substring-before($latin1,$first-char)) + 160"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:message terminate="no">Warning: string contains a character that is out of range! Substituting "?".</xsl:message>
								<xsl:text>63</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="hex-digit1" select="substring($hex,floor($codepoint div 16) + 1,1)"/>
					<xsl:variable name="hex-digit2" select="substring($hex,$codepoint mod 16 + 1,1)"/>
					<xsl:value-of select="concat('%',$hex-digit1,$hex-digit2)"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="string-length($str) &gt; 1">
				<xsl:call-template name="common_url-encode">
					<xsl:with-param name="str" select="substring($str,2)"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>

	</xsl:template>-->
</xsl:stylesheet>
