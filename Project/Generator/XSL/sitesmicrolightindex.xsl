<!--<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"
				media-type="text/html"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				indent="yes"/>

	<xsl:include href="common.xsl"/>

	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<xsl:call-template name="common_head">
				<xsl:with-param name="title">Australian National Site Guide - List of all microlight sites</xsl:with-param>
			</xsl:call-template>
			<body>
      <script>
      window.cookieconsent_options = {
          message: "Use of the Australian National Site Guide is covered by our disclaimer.",
          learnMore: 'Read the disclaimer here.',
          link: 'disclaimer.html',
          dismiss : "Got it.",
          theme: "dark-floating",
          expiryDays: 99999,
      };
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
				<xsl:call-template name="common_menu-top"/>
				<xsl:call-template name="common_header_site-guide"/>
				<div id="wrapper-content">
					<div id="wrapper-content2">
						<xsl:call-template name="common_menu-page_siteguide_incl-external">
							<xsl:with-param name="mapType" select="'microlight'" />
						</xsl:call-template>

						<div class="content">
							<h2>Australian National Site Guide - Index of Microlight Sites</h2>
							<xsl:call-template name="common_contribute"/>
							<table id="index-table" rules="rows" frame="void">
								<tbody>
									<tr>
										<th>
											Name
										</th>
										<th>
											Location
										</th>
										<th>
											Facilities
										</th>
									</tr>
									<xsl:for-each select="//siteMicrolight">
										<xsl:sort select="name"/>
										<tr>
											<td>
												<xsl:value-of disable-output-escaping="yes" select="name"/>
											</td>
											<td>
												<xsl:value-of disable-output-escaping="yes" select="shortlocation"/>
											</td>
											<td>
												<xsl:value-of disable-output-escaping="yes" select="facilities"/>
											</td>
										</tr>
									</xsl:for-each>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="wrapper-footer">
					<div id="footer">
						<a href="mailto:Webmaster@vhpa.org.au">Webmaster</a> | <a href="credit.html">Credits</a> | <a href="disclaimer.html">Disclaimer</a>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>-->