﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Xsl;
using System.Xml.Schema;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

using Geo;
using System.Text.RegularExpressions;
using static VHPASiteGuide.DrawSettings;
using System.Diagnostics;
using System.Drawing;
using System.Xml.Linq;
using System.Web.Script.Serialization;

namespace VHPASiteGuide
{
    public class Generator
    {
        public event ProgressEventHandler ProgressEvent;

        private string
          sourceDir,
          dataDir,
          xslDir,
          targetDir,
          sitesDir;

        public Generator(string SourceDir, string TargetDir)
        {
            sourceDir = SourceDir;
            dataDir = Path.Combine(sourceDir, "SiteGuideData");
            xslDir = Path.Combine(sourceDir, "XSL");
            targetDir = TargetDir;
            if (targetDir.EndsWith("\\"))
                targetDir = targetDir.Remove(targetDir.Length - 1);
            sitesDir = targetDir + "\\Sites";
        }

        public void GenerateSpecificSites(string[] SiteNames)
        {
            Generate(SiteNames);
        }

        public void GenerateAll()
        {
            Generate();
        }

        public void DeleteSitePage(string SiteName)
        {
            File.Delete(SitePageFileName(SiteName));
        }

        private void Generate(string[] SiteNames = null)
        {
            XmlDocument xmlSites = LoadSiteGuideData();

            EnsureDirExists(targetDir);

            AddCalculatedData(xmlSites);

            //Generate index pages
            //all sites
            runSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesindex.html", null);
            //open sites
            XsltArgumentList argsOpenIndex = new XsltArgumentList();
            argsOpenIndex.AddParam("restrictToType", string.Empty, "open");
            runSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesopenindex.html", argsOpenIndex);
            //closed sites
            XsltArgumentList argsClosedIndex = new XsltArgumentList();
            argsClosedIndex.AddParam("restrictToType", string.Empty, "closed");
            runSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesclosedindex.html", argsClosedIndex);

            ////microlight sites
            //runSimpleXsl(xslDir + "\\sitesmicrolightindex.xsl", xmlSites, targetDir + "\\sitesmicrolightindex.html", null);

            //Generate site page(s)
            EnsureDirExists(sitesDir);
            XslCompiledTransform sitePageTransform = new XslCompiledTransform();
            sitePageTransform.Load(xslDir + "\\sitespage.xsl");
            if (SiteNames == null)
            {
                XmlNodeList sites = xmlSites.SelectNodes("//siteFreeFlight");
                raiseProgressEvent("Generate " + sites.Count + " site pages...");
                foreach (XmlNode site in sites)
                {
                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }
            else
            {
                foreach (string siteName in SiteNames)
                {
                    raiseProgressEvent("Generate site page for \"" + siteName + "\"");
                    XmlNode site = xmlSites.SelectSingleNode("//siteFreeFlight[name=\"" + siteName + "\"]");

                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }

            //Generate kmz
            // for Google Earth
            generateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesAll.kmz", null);

            // for Google Earth - freeflight only
            XsltArgumentList argsFreeflight = new XsltArgumentList();
            argsFreeflight.AddParam("subset", string.Empty, "freeflight");
            generateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesFreeflight.kmz", argsFreeflight);

            //// for Google Earth - microlight only
            //XsltArgumentList argsMicrolight = new XsltArgumentList();
            //argsMicrolight.AddParam("subset", string.Empty, "microlight");
            //generateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesMicrolight.kmz", argsMicrolight);

            // for Google Earth - closed only
            XsltArgumentList argsClosed = new XsltArgumentList();
            argsClosed.AddParam("subset", string.Empty, "closed");
            generateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesClosed.kmz", argsClosed);

            // for Google Earth - CAR166
            string Car166Json = dataDir + "\\VHFAerodromes.json";
            GenCAR166Kml(Car166Json, targetDir + "\\CAR166.kmz");
            // Also copy JSON for site guide map
            FileInfo fi = new FileInfo(Car166Json);
            fi.CopyTo(Path.Combine(targetDir, fi.Name), true);

            // for map page
            runSimpleXsl(xslDir + "\\sitesForGMaps.xsl", xmlSites, targetDir + "\\sitesForGMaps.xml", null);

            //Print/download guide
            int maxSitesPerPage = 20; // max sites per 'download or print' page
            string areasWithPrintGuidesXPath = "siteGuideData//area[" +
              ".//siteFreeFlight[not(closed)] and " +
              "(" +
                "(" +
                  "count(.//siteFreeFlight[not(closed)]) <= " + maxSitesPerPage + " and " +
                  "(count(..//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " or parent::siteGuideData)" +
                ") " +
                "or count(.//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " and not(area)" +
              ")" +
            "]";
            XmlNodeList areasWithPrintGuides = xmlSites.SelectNodes(areasWithPrintGuidesXPath);

            //Generate print index
            XsltArgumentList argsPrintIndex = new XsltArgumentList();
            argsPrintIndex.AddParam("maxSitesPerPage", string.Empty, maxSitesPerPage);
            runSimpleXsl(xslDir + "\\downloads.xsl", xmlSites, targetDir + "\\downloads.html", argsPrintIndex);

            //Generate downloadable/printable site guides
            XslCompiledTransform printGuideTransform = new XslCompiledTransform();
            printGuideTransform.Load(xslDir + "\\sitesForPrint.xsl");
            GeneratePrintGuides(xmlSites, printGuideTransform, areasWithPrintGuides);

            //Include site guide data
            FileInfo siteGuideDataXml = new FileInfo(dataDir + "\\siteGuideData.xml");
            siteGuideDataXml.CopyTo(Path.Combine(targetDir, siteGuideDataXml.Name), true);
            FileInfo siteGuideDataXsd = new FileInfo(dataDir + "\\siteGuideData.xsd");
            siteGuideDataXsd.CopyTo(Path.Combine(targetDir, siteGuideDataXsd.Name), true);

            //Generate OpenAir file
            GenerateOpenAirFile(xmlSites, Path.Combine(targetDir, "siteGuide.OpenAir.txt"));
            //Generate OziExplorer wpt file
            GenerateOziExplorerWptFile(xmlSites, Path.Combine(targetDir, "siteGuide.OziExplorer.wpt"));

            raiseProgressEvent("Generated site guide.");
        }

        private void GenerateOpenAirFile(XmlDocument xmlSites, string outFile)
        {
            //Closest to an OpenAir spec I could find: http://www.winpilot.com/UsersGuide/UserAirspace.asp
            var result = new StringBuilder();
            XmlNodeList mapshapes = xmlSites.SelectNodes("siteGuideData//mapShape[coordinates and not(@category='Access')]");
            foreach (XmlNode mapshape in mapshapes)
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                string category = mapshape.Attributes["category"].Value;
                //                string description = mapshape.SelectSingleNode("description")?.InnerText;

                var drawSettings = DrawSettings.ShapeColours[category];

                //result.AppendLine("T" + (drawSettings.Dimension == 1 ? "O" : "C") + " "
                //    + category
                //    + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                //var color = System.Drawing.ColorTranslator.FromHtml("#" + drawSettings.Colour);
                //result.AppendLine("SP 0 1 " + color.R + " " + color.G + " " + color.B); // 0 =Solid line, 1 pxel wide
                //if (drawSettings.Dimension == 2)
                //    result.AppendLine("SB -1,-1,-1"); // Make polygon interior transparent
                //result.AppendLine("V Z=100"); // Show these shapes at zoom levels up to 100km. TODO: Make this smarter and dependent on mapShape size

                string openAirEquivAirspaceClass;
                switch (category)
                {
                    case "Landing":
                    case "Access":
                    case "Feature":
                        openAirEquivAirspaceClass = "G";
                        break;

                    case "No Landing":
                        openAirEquivAirspaceClass = "GP"; // Glider prohibited
                        break;

                    case "Powerline":
                    case "Hazard":
                        openAirEquivAirspaceClass = "Q"; //Danger
                        break;

                    case "No Launching":
                    case "Emergency Landing":
                        openAirEquivAirspaceClass = "R"; //Restricted
                        break;

                    case "No Fly Zone":
                        openAirEquivAirspaceClass = "P"; //Prohibited
                        break;

                    default:
                        throw new ApplicationException("Unrecognised category: " + category);
                }

                XmlNodeList polys = mapshape.SelectNodes("coordinates");
                foreach (XmlNode poly in polys)
                {
                    result.AppendLine("AC " + openAirEquivAirspaceClass);
                    result.AppendLine("AN " + category + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                    result.AppendLine("AL SFC");
                    result.AppendLine("AH 100 ft");
                    var coords = poly.InnerText.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string coord in coords)
                        AddOpenAirCoord(result, coord);
                    if (drawSettings.Dimension == 1) // OpenAir has no line element in the airspace format, so close the polygon by reversing the path
                        foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                            AddOpenAirCoord(result, coordString);
                    result.AppendLine();
                }
            }

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            File.WriteAllText(outFile, result.ToString());
        }

        private void GenerateOziExplorerWptFile(XmlDocument xmlSites, string outFile)
        {
            //http://www.oziexplorer3.com/eng/help/fileformats.html
            var result = new StringBuilder();
            result.AppendLine("OziExplorer Waypoint File Version 1.1");
            result.AppendLine("WGS 84");
            result.AppendLine("Reserved");
            result.AppendLine("Reserved");

            XmlNodeList mapshapes = xmlSites.SelectNodes("siteGuideData//mapShape[coordinates]");
            foreach (XmlNode mapshape in mapshapes)
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                if (name != null)
                    name = OziExplorerWptFileTextClean(name);
                string category = mapshape.Attributes["category"].Value;
                //                string description = mapshape.SelectSingleNode("description")?.InnerText;

                ShapeDrawSetting drawSettings = DrawSettings.ShapeColours[category];
                if (drawSettings.Dimension != 1)
                {
                    int color = OziExplorerColour(drawSettings.Colour);

                    //switch (category)
                    //{
                    //    case "Landing":
                    //    case "Access":
                    //    case "Feature":
                    //        openAirEquivAirspaceClass = "G";
                    //        break;

                    //    case "No Landing":
                    //        openAirEquivAirspaceClass = "GP"; // Glider prohibited
                    //        break;

                    //    case "Powerline":
                    //    case "Hazard":
                    //        openAirEquivAirspaceClass = "Q"; //Danger
                    //        break;

                    //    case "No Launching":
                    //    case "Emergency Landing":
                    //        openAirEquivAirspaceClass = "R"; //Restricted
                    //        break;

                    //    case "No Fly Zone":
                    //        openAirEquivAirspaceClass = "P"; //Prohibited
                    //        break;

                    //    default:
                    //        throw new ApplicationException("Unrecognised category: " + category);
                    //}

                    XmlNodeList polys = mapshape.SelectNodes("coordinates");
                    foreach (XmlNode poly in polys)
                    {
                        Track track = Track.CreateFromPointsData(poly.InnerText.Trim(), false);
                        if (track.Trackpoints.Count > 2)
                        {
                            LatLng centroid = track.GetCentroid();
                            //result.AppendLine("AC " + openAirEquivAirspaceClass);
                            //result.AppendLine("AN " + category + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                            //result.AppendLine("AL SFC");
                            //result.AppendLine("AH 100 ft");
                            //var coords = poly.InnerText.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                            //foreach (string coord in coords)
                            //    AddOpenAirCoord(result, coord);
                            //if (drawSettings.Dimension == 1) // OpenAir has no line element in the airspace format, so close the polygon by reversing the path
                            //    foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                            //        AddOpenAirCoord(result, coordString);
                            result.AppendLine(String.Format("-1,{0},{1},{2},,0,1,3,0,{4},{3},0,0,0,-777,6,0,17", category, centroid.lat, centroid.lng, name, color));
                        }
                    }
                }
            }

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            File.WriteAllText(outFile, result.ToString());
        }

        private static int OziExplorerColour(string colour)
        {
            return int.Parse(colour.Substring(4, 2) + colour.Substring(2, 2) + colour.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        }

        private static string OziExplorerWptFileTextClean(string text)
        {
            return text.Replace("\r", "").Replace("\n", "").Replace(",", ((char)209).ToString());
        }
        private void AddOpenAirCoord(StringBuilder result, string coord)
        {
            var latLng = coord.Split(',');
            float lngDeg = float.Parse(latLng[0]);
            float latDeg = float.Parse(latLng[1]);
            result.AppendLine("DP " + CoordToDMMSSH(latDeg, "N", "S") + " " + CoordToDMMSSH(lngDeg, "E", "W")); //e.g. 38:56:00 N 120:02:00 W
        }

        private string CoordToDMMSSH(float coord, string posHemisphere, string negHemisphere)
        {
            //http://stackoverflow.com/questions/3249700/convert-degrees-minutes-seconds-to-decimal-coordinates
            int sec = (int)Math.Round(coord * 3600);
            int deg = sec / 3600;
            sec = Math.Abs(sec % 3600);
            int min = sec / 60;
            sec %= 60;
            return Math.Abs(deg) + ":" + min.ToString("D2") + ":" + sec.ToString("D2") + " " + (coord < 0 ? negHemisphere : posHemisphere);
        }

        private static void AddCalculatedData(XmlDocument xmlSites)
        {
            PolylineEncoder polyEncoderDetail = new PolylineEncoder(18, 2, 1e-5, true);

            var allMapShapeNodes = xmlSites.SelectNodes("//mapShape");
            var mapShapeBounds = new LatLngBounds[allMapShapeNodes.Count];

            //Assign an id to each mapShape and calculate bounds of each shape
            int mapShapeId = 0;
            foreach (XmlNode mapShapeNode in allMapShapeNodes)
            {
                mapShapeNode.Attributes.Append(xmlSites.CreateAttribute("id")).Value = mapShapeId.ToString();

                mapShapeBounds[mapShapeId] = new LatLngBounds();

                foreach (XmlNode coordNode in mapShapeNode.SelectNodes("coordinates"))
                {
                    Track track = Track.CreateFromPointsData(coordNode.InnerText.Trim(), false);

                    //encode mapShape coordinates
                    XmlNode encodedCoordsNode = mapShapeNode.AppendChild(xmlSites.CreateElement("encodedCoordinates"));
                    encodedCoordsNode.InnerText = polyEncoderDetail.DPEncode(track)["encodedPoints"];
                    encodedCoordsNode.Attributes.Append(xmlSites.CreateAttribute("level")).InnerText = "detail";

                    mapShapeBounds[mapShapeId].expand(track.GetBounds());
                }

                mapShapeId++;
            }

            //Process maps for each site with relevant mapShapes
            foreach (XmlNode siteNode in xmlSites.SelectNodes("//siteFreeFlight[mapShape|mapShapeRef]"))
            {
                var siteMapShapeBounds = new List<LatLngBounds>();
                foreach (XmlNode mapShapeNode in GetMapShapesInclRef(siteNode))
                {
                    siteMapShapeBounds.Add(mapShapeBounds[Convert.ToInt32(mapShapeNode.Attributes["id"].Value)]);
                    //XmlNode mapShapeBoundsNode = mapShapeNode.AppendChild(xmlSites.CreateElement("boundingBox"));
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("latMin")).InnerText = mapShapeBounds[i].latMin.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("latMax")).InnerText = mapShapeBounds[i].latMax.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("lngMin")).InnerText = mapShapeBounds[i].lngMin.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("lngMax")).InnerText = mapShapeBounds[i].lngMax.ToString();

                }
                var maps = GetMaps(siteMapShapeBounds, 445, 445);
                LatLngBounds overviewMap = maps[0];

                int overviewMapZoom = overviewMap.GetGMapZoom(445, 445);
                //siteNode.Attributes.Append(xmlSites.CreateAttribute("overviewMapZoom")).InnerText = overviewMapZoom.ToString();

                PolylineEncoder polyEncoderOverviewMap = new PolylineEncoder(18, 2, 8e-6 * Math.Pow(2, 19 - overviewMapZoom), true);

                //XmlNode overviewMapNode = siteNode.AppendChild(xmlSites.CreateElement("overviewMap"));
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("centerLat")).InnerText = allShapesBounds.Center.lat.ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("centerLng")).InnerText = allShapesBounds.Center.lng.ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("spanLat")).InnerText = (allShapesBounds.latMax - allShapesBounds.latMin).ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("spanLng")).InnerText = (allShapesBounds.lngMax - allShapesBounds.lngMin).ToString();


                String staticMapUrl = getStaticMapUrl(siteNode, polyEncoderOverviewMap);
                //if (staticMapUrl.Length > 2000) //2048 length limit - the xslt will add further to this.
                //{
                //    // Try again using a larger value of verysmall:
                //    PolylineEncoder polyEncoderOverviewMapLessDetail = new PolylineEncoder(1, 2, 1e-5 * Math.Pow(2, 19 - overviewMapZoom), true);
                //    staticMapUrl = getStaticMapUrl(siteNode, polyEncoderOverviewMapLessDetail);
                if (staticMapUrl.Length > 8000) // limit has benn increased from 2048
                    throw new ApplicationException("Static Maps API URL too long.");
                //}
                siteNode.AppendChild(xmlSites.CreateElement("mapShapesUrl")).InnerText = staticMapUrl;
            }

            //encode launch coordinates ** currently not used **
            //Track launchCoords = new Track();
            //foreach (XmlNode markerNode in xmlSites.SelectNodes("//siteFreeFlight/launch|//siteMicrolight"))
            //{
            //    launchCoords.AddTrackpoint(markerNode.SelectSingleNode("lat").InnerText, markerNode.SelectSingleNode("lon").InnerText);
            //}
            //string encodedCoords = PolylineEncoder.CreateEncodings(launchCoords, 0, 1)["encodedPoints"];
            //xmlSites.DocumentElement.AppendChild(xmlSites.CreateElement("encodedMarkerCoordinates")).InnerText = encodedCoords;
        }

        private static String getStaticMapUrl(XmlNode siteNode, PolylineEncoder polyEncoderOverviewMap)
        {
            var result = new StringBuilder();
            //            result.Append(String.Format("https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&size={0}x{0}&maptype=hybrid&markers=icon:http:/siteguide.org.au/Images/mm_20_green.png", size));
            foreach (XmlNode launchNode in siteNode.SelectNodes("launch"))
            {
                String lat = launchNode.SelectSingleNode("lat").InnerText;
                String lon = launchNode.SelectSingleNode("lon").InnerText;

                result.Append(String.Format("|{0},{1}", lat, lon));
            }
            //TODO: Add any other launches in view, e.g. Ben Nevis to Sugarloaf?

            var mapShapeNodes = GetMapShapesInclRef(siteNode);
            foreach (XmlNode mapShapeNode in mapShapeNodes)
            {
                BuildGMapUrl(polyEncoderOverviewMap, result, mapShapeNode);
            }

            return result.ToString();
        }

        //Get all mapShapes of this siteFreeFlight as well as all referenced mapShapes
        private static IEnumerable<XmlNode> GetMapShapesInclRef(XmlNode siteNode)
        {
            IEnumerable<XmlNode> mapShapeNodes = siteNode.SelectNodes("mapShape").Cast<XmlNode>();
            foreach (XmlNode mapShapeRefNode in siteNode.SelectNodes("mapShapeRef"))
            {
                if ((mapShapeRefNode.Attributes["site"] == null && mapShapeRefNode.Attributes["mapShape"] == null)
                || (mapShapeRefNode.Attributes["site"] != null && mapShapeRefNode.Attributes["mapShape"] != null))
                    throw new ApplicationException("MapshapeRef must specify exactly one of mapShape or site.");

                XmlNodeList refMapShapeNodes;
                if (mapShapeRefNode.Attributes["site"] != null)
                {
                    refMapShapeNodes = siteNode.SelectNodes(string.Format("//siteFreeFlight[name=\"{0}\"]/mapShape", mapShapeRefNode.SelectSingleNode("@site").Value));
                }
                else
                {
                    refMapShapeNodes = siteNode.SelectNodes(string.Format("//mapShape[name=\"{0}\"]", mapShapeRefNode.SelectSingleNode("@mapShape").Value));

                    if (refMapShapeNodes.Count == 0)
                        throw new ApplicationException("mapShapeRef not found.");
                    if (refMapShapeNodes.Count > 1)
                        throw new ApplicationException("Multiple mapShapeRef matches found.");
                }
                mapShapeNodes = mapShapeNodes.Cast<XmlNode>().Concat(refMapShapeNodes.Cast<XmlNode>());
            }
            return mapShapeNodes;
        }

        private static void BuildGMapUrl(PolylineEncoder polyEncoderOverviewMap, StringBuilder result, XmlNode mapShapeNode)
        {
            var shapeColour = DrawSettings.ShapeColours[mapShapeNode.Attributes["category"].Value];

            foreach (XmlNode coords in mapShapeNode.SelectNodes("coordinates"))
            {
                Track track = Track.CreateFromPointsData(coords.InnerText.Trim(), false);
                Dictionary<String, String> encodedTrack = polyEncoderOverviewMap.DPEncode(track);

                result.Append("&path=weight:1|");
                if (shapeColour.Dimension == 2 && encodedTrack["encodedLevels"].Length > 2) // Don't bother filling the area in if it's been reduced to a line - shorter url this way
                    result.Append(String.Format("fillcolor:0x{0}33|", shapeColour.Colour));
                result.Append(String.Format("color:0x{0}{1}|enc:{2}", shapeColour.Colour, shapeColour.Dimension == 1 ? "99" : "66", encodedTrack["encodedPoints"]));
            }
        }

        private static List<LatLngBounds> GetMaps(List<LatLngBounds> shapesBounds, int pixelWidth, int pixelHeight)
        {
            var result = new List<LatLngBounds>();

            //overview map
            var overviewMap = new LatLngBounds();
            foreach (var bounds in shapesBounds)
            {
                overviewMap.expand(bounds);
            }
            result.Add(overviewMap);

            var unrepresented = GetUnrepresented(shapesBounds, overviewMap, pixelWidth, pixelHeight);

            //Sort shapes by size descending
            unrepresented.Sort((bounds1, bounds2) => Math.Sign(Math.Max(bounds2.SizeLat, bounds2.SizeLng) - Math.Max(bounds1.SizeLat, bounds1.SizeLng)));

            while (unrepresented.Count > 0)
            {
                var detailMap = unrepresented[0]; //Start with largest bounds
                var includedShapes = new List<LatLngBounds>();
                includedShapes.Add(detailMap);

                unrepresented.RemoveAt(0);

                //Expand to include next largest shape as long as this doesn't drop any contained shape under minimum size
                int i = 0;
                while (i < unrepresented.Count())
                {
                    var expandedMap = new LatLngBounds();
                    expandedMap.expand(detailMap);
                    expandedMap.expand(unrepresented[i]);
                    if (GetUnrepresented(includedShapes, expandedMap, pixelWidth, pixelHeight).Count == 0)
                    {
                        detailMap = expandedMap;
                        includedShapes.Add(unrepresented[i]);
                        unrepresented.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                result.Add(detailMap);
            }
            return result;
        }

        //Return the list of bounds which would be drawn too small on the specified map
        private static List<LatLngBounds> GetUnrepresented(List<LatLngBounds> shapesBounds, LatLngBounds mapBounds, int pixelWidth, int pixelHeight)
        {
            //detail maps
            const int MinimumShapeSizePixels = 20;
            var detailShapeBounds = new List<LatLngBounds>();
            foreach (var shapeBounds in shapesBounds)
            {
                if (shapeBounds.GMapSizeInPixel(mapBounds.GetGMapZoom(pixelWidth, pixelHeight)) < MinimumShapeSizePixels)
                    detailShapeBounds.Add(shapeBounds);
            }
            return detailShapeBounds;
        }

        private void GeneratePrintGuides(XmlDocument xmlSites, XslCompiledTransform printGuideTransform, XmlNodeList areas)
        {
            foreach (XmlNode area in areas)
            {
                XmlWriter pageResult = XmlWriter.Create(targetDir + "\\Site Guide - " + AreaFullName(area) + ".html", printGuideTransform.OutputSettings);
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("area", string.Empty, area);
                printGuideTransform.Transform(xmlSites, args, pageResult);
                pageResult.Close();
            }
        }

        public void ExtractData()
        {
            XmlDocument xmlSites = LoadSiteGuideData();

            string targetDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); //@"D:\Desktop";

            EnsureDirExists(targetDir);

            StringBuilder result = new StringBuilder();
            result.AppendLine(string.Join(",", new string[]{
                    "Site name",
                    //"Site short location",
                    //"Site extended location",
                    "Site land owners",
                    //"Site contact",
                    "Site responsible",
                    "Launch name",
                    //"Launch description",
                    "Launch lat",
                    "Launch lon"
                    }));

            //XmlNodeList launches = xmlSites.SelectNodes("//area[@name='VIC']//launch");
            XmlNodeList launches = xmlSites.SelectNodes("//area//launch");
            foreach (XmlNode launch in launches)
            {
                result.AppendLine(string.Join(",", fieldsContents(launch, new string[]{
                    "../name",
                    //"../shortlocation",
                    //"../extendedlocation",
                    "../landowners",
                    //"../contactName",
                    "../responsibleName",
                    "name",
                    //"description",
                    "lat",
                    "lon"
                    })));
            }

            raiseProgressEvent("Extracted data.");

            using (StreamWriter outfile = new StreamWriter(targetDir + @"\Launches.csv"))
            {
                outfile.Write(result.ToString());
            }
        }

        private string[] fieldsContents(XmlNode node, string[] selectors)
        {
            string[] result = new string[selectors.Count()];
            for (int i = 0; i < selectors.Count(); i++)
            {
                string selector = selectors[i];
                XmlNode field = node.SelectSingleNode(selector);
                if (field != null)
                {
                    result[i] = "\"" + field.InnerText.Replace("\n", "").Replace("\"", "'").Trim() + "\"";
                }
                else
                    result[i] = "";
            }
            return result;
        }

        private static string AreaFullName(XmlNode area)
        {
            string areaName = area.Attributes.GetNamedItem("name").InnerText;
            if (area.ParentNode.Name == "area")
                return AreaFullName(area.ParentNode) + " - " + areaName;
            else
                return areaName;
        }

        private void GenerateSitePage(XmlDocument xmlSites, XslCompiledTransform sitePageTransform, XmlNode site)
        {
            XmlWriter pageResult = XmlWriter.Create(SitePageFileName(site.SelectSingleNode("name").InnerText), sitePageTransform.OutputSettings);
            GenerateSitePage(xmlSites, sitePageTransform, site, pageResult);
        }

        public static void GenerateSitePage(XmlDocument xmlSites, XslCompiledTransform sitePageTransform, XmlNode site, XmlWriter pageResult)
        {
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("siteName", string.Empty, site.SelectSingleNode("name").InnerText);
            sitePageTransform.Transform(xmlSites, args, pageResult);
            pageResult.Close();
        }

        private string SitePageFileName(string siteName)
        {
            return sitesDir + "\\" + siteName + ".html";
        }

        private XmlDocument LoadSiteGuideData()
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(siteGuideDataType));
            var siteGuideData = (siteGuideDataType)ser.Deserialize(new StreamReader(dataDir + "\\siteGuideData.xml"));

            //Load and validate
            raiseProgressEvent("Load data ...");
            XmlReaderSettings settings = new XmlReaderSettings() { ValidationType = ValidationType.Schema };
            settings.Schemas.Add(null, dataDir + "\\siteGuideData.xsd");
            //settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);

            XmlReader reader = XmlReader.Create(dataDir + "\\siteGuideData.xml", settings);
            XmlDocument xmlSites = new XmlDocument();
            xmlSites.Load(reader);
            reader.Close();

            xmlSites.Validate(new ValidationEventHandler(ValidationEventHandler));
            raiseProgressEvent("Data loaded and valid.");
            return xmlSites;
        }

        private void generateKmz(string transformFile, XmlDocument xmlSites, string targetFile, XsltArgumentList args)
        {
            raiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(transformFile);
            MemoryStream kml = new MemoryStream();
            XmlWriter result = XmlWriter.Create(kml, transform.OutputSettings);
            transform.Transform(xmlSites, args, result);
            result.Close();
            byte[] buffer = new byte[(int)kml.Length];
            kml.Position = 0;
            kml.Read(buffer, 0, (int)kml.Length);
            kmlSaveAsKmz(buffer, targetFile);
        }

        private void runSimpleXsl(string transformFile, XmlDocument source, string targetFile, XsltArgumentList args)
        {
            raiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(transformFile);
            XmlWriter result = XmlWriter.Create(targetFile, transform.OutputSettings);
            transform.Transform(source, args, result);
            result.Close();
        }

        public void CopyIncludeDir(string includeDir)
        {
            raiseProgressEvent("Copy Include directory");
            copyDir(new DirectoryInfo(includeDir), new DirectoryInfo(targetDir));
        }

        private static void copyDir(DirectoryInfo source, DirectoryInfo target)
        {
            //A simple directory copy.

            //ignore svn source control dirs
            if (source.Name == ".svn") return;

            //Copy all files
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy all subdirectories
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                copyDir(dir, target.CreateSubdirectory(dir.Name));
            }
        }

        public static void EnsureDirExists(string dir)
        {
            //TODO: Maybe get user confirmation for create
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public int ReadCanungraAreas(string sourceFile, string outputFile)
        {
            //Load and validate
            raiseProgressEvent("Load data ...");
            XmlReader reader = XmlReader.Create(sourceFile);
            XmlDocument xmlSource = new XmlDocument();
            xmlSource.Load(reader);
            reader.Close();

            var xmlOut = new XDocument(new XElement("root"));

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlSource.NameTable);
            nsmgr.AddNamespace("x", xmlSource.DocumentElement.NamespaceURI);
            var polyFolder = xmlSource.SelectSingleNode("//x:Folder[x:name=\"Landing and No Landing Polygons\"]", nsmgr);
            var allPolys = polyFolder.SelectNodes("x:Placemark", nsmgr);
            foreach (XmlNode poly in allPolys)
            {
                string name = poly.SelectSingleNode("x:name", nsmgr).InnerText;

                var mapShape = new XElement("mapShape",
                        new XElement("name", name),
                        new XElement("description", new XCData("Imported from the 'Landing and No Landing Polygons' section of <a href='http://www.chgc.asn.au/Resources/Online-Maps'>http://www.chgc.asn.au/Resources/Online-Maps</a>"))
                    );

                double area = 0;
                foreach (XmlNode coords in poly.SelectNodes(".//x:coordinates", nsmgr))
                {
                    string coordinates = coords.InnerText;
                    mapShape.Add(new XElement("coordinates", coordinates));
                    area += Track.CreateFromPointsData(coordinates, false).GetArea();
                }

                var styleMapUrl = poly.SelectSingleNode("x:styleUrl", nsmgr).InnerText;
                Debug.Assert(styleMapUrl.First() == '#');

                var styleMap = xmlSource.SelectSingleNode("//x:StyleMap[@id='" + styleMapUrl.Substring(1) + "']", nsmgr);
                string normalStyleUrl = styleMap.SelectSingleNode("x:Pair[x:key='normal']/x:styleUrl", nsmgr).InnerText;

                var style = xmlSource.SelectSingleNode("//x:Style[@id='" + normalStyleUrl.Substring(1) + "']", nsmgr);
                string lineColorHex = style.SelectSingleNode("x:LineStyle/x:color", nsmgr).InnerText;
                string a = lineColorHex.Substring(0, 2);
                string b = lineColorHex.Substring(2, 2);
                string g = lineColorHex.Substring(4, 2);
                string r = lineColorHex.Substring(6, 2);

                var lineColor = Color.FromArgb((int)Convert.ToUInt32(a, 16), (int)Convert.ToUInt32(r, 16), (int)Convert.ToUInt32(g, 16), (int)Convert.ToUInt32(b, 16));
                double distanceFromRed = BasicColorDistance(lineColor, Color.Red);
                double distanceFromYellow = BasicColorDistance(lineColor, Color.Yellow);

                string category;
                if (distanceFromYellow < 90)
                {
                    Debug.Assert(area > 0);
                    category = "Landing";
                }
                else if (distanceFromRed < 90)
                {
                    if (area == 0)
                        category = "Powerline";
                    else
                        category = "No Landing";
                }
                else
                    throw new ArgumentException("Could not map polygon color to mapshape category.");

                mapShape.Add(new XAttribute("category", category));

                xmlOut.Root.Add(mapShape);
            }

            xmlOut.Save(outputFile);
            return allPolys.Count;
        }

        private double BasicColorDistance(Color color1, Color color2)
        {
            return Math.Sqrt(Math.Pow(color1.B - color2.B, 2) + Math.Pow(color1.G - color2.G, 2) + Math.Pow(color1.R - color2.R, 2));
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    break;
            }

        }

        void raiseProgressEvent(string description)
        {
            if (ProgressEvent != null)
                ProgressEvent(this, new ProgressEventArgs(description));
        }

        static void kmlSaveAsKmz(byte[] kmlBytes, string outFile)
        {
            using (System.IO.MemoryStream ms = new MemoryStream())
            using (ZipOutputStream zs = new ZipOutputStream(ms))
            {
                zs.SetLevel(6);
                zs.IsStreamOwner = true;

                //create zipped kml entry
                ZipEntry zipEntry = new ZipEntry("Doc.kml");

                zipEntry.DateTime = DateTime.Now;
                zipEntry.Size = kmlBytes.Length;

                zs.PutNextEntry(zipEntry);
                zs.Write(kmlBytes, 0, kmlBytes.Length);
                zs.CloseEntry();

                zs.Finish();

                //write to disk
                using (FileStream kmz = new FileStream(outFile, FileMode.Create))
                {
                    kmz.Write(ms.ToArray(), 0, (int)ms.Length);
                    zs.Close();
                }
            }
            // For debug/comparison purposes, also save as kml
            string kmlFilename;
            if (outFile.EndsWith(".kmz"))
                kmlFilename = outFile.Substring(0, outFile.Length - 4) + ".kml";
            else
                kmlFilename = outFile + ".kml";

            using (FileStream kml = new FileStream(kmlFilename, FileMode.Create))
            {
                kml.Write(kmlBytes, 0, (int)kmlBytes.Length);
            }
        }

        public void GenCAR166Kml(string JSONFilePath, string targetFileName)
        {
            TextReader reader = new StreamReader(JSONFilePath);
            string json = reader.ReadToEnd();
            reader.Close();

            var aerodromes = new JavaScriptSerializer().Deserialize<List<Dictionary<String, Object>>>(json);

            var kmlBuilder = new StringBuilder();
            kmlBuilder.AppendLine("<?xml version='1.0' encoding='UTF-8'?>");
            kmlBuilder.AppendLine("<kml xmlns='http://www.opengis.net/kml/2.2'>");
            kmlBuilder.AppendLine("<Folder>");
            kmlBuilder.AppendLine("<Style id='CAR166Disk'><PolyStyle><color>7f1767E5</color><colorMode>normal</colorMode><outline>0</outline></PolyStyle></Style>");
            kmlBuilder.AppendLine("<name>CAR 166</name><open>1</open>");
            kmlBuilder.AppendLine("<description>10 nm circles around CAR 166 Aerodromes. Inofficial, no claim to accuracy. Not for navigation.</description>");

            foreach (Dictionary<String, Object> aerodrome in aerodromes)
            {
                LatLng center = new LatLng(double.Parse(aerodrome["Lat"].ToString()), double.Parse(aerodrome["Lng"].ToString()));
                List<LatLng> circlePoints = center.CirclePoints(18520, 40);

                kmlBuilder.AppendLine("<Placemark id='" + aerodrome["Code"] + "'><name>" + aerodrome["Name"] + " (" + aerodrome["Code"] + ")</name><description><![CDATA[CTAF " + aerodrome["CTAF"] + "<br/>" + aerodrome["Category"] + "<br/><a href='http://www.airservicesaustralia.com/aip/current/ersa/FAC_" + aerodrome["Code"] + "_09-Nov-2017.pdf'>FAC</a>]]></description><styleUrl>#CAR166Disk</styleUrl><Polygon><outerBoundaryIs><LinearRing><coordinates>");
                foreach (LatLng circlePoint in circlePoints)
                    kmlBuilder.AppendLine(circlePoint.lng + "," + circlePoint.lat);
                kmlBuilder.AppendLine("</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark>");
            }
            kmlBuilder.AppendLine("</Folder></kml>");

            byte[] buffer = new System.Text.UTF8Encoding().GetBytes(kmlBuilder.ToString());
            kmlSaveAsKmz(buffer, targetFileName);
        }

        public int UpdateCAR166DataFromFile(string openAirSourceFileName)
        {
            string source = System.IO.File.ReadAllText(openAirSourceFileName);
            string target = dataDir + "\\VHFAerodromes.json";

            string pattern = @"\r\n\r\n(?:.+\r\n)+AN (?<name>.+) (?<CTAF>[0-9]*\.?[0-9]+) (?:\*TWR HRS-SEE ERSA\* )?(?<state>.+) \((?<code>.+)\) (?<category>.+)\r\n(?:.+\r\n)+V X=(?<latDeg>.+):(?<latMin>.+):(?<latSec>.+) (?<latHem>.)  (?<lngDeg>.+):(?<lngMin>.+):(?<lngSec>.+) (?<lngHem>.)\r\n";

            //var result = new List<object>();
            var json = new StringBuilder("[");

            var matches = Regex.Matches(source, pattern);
            foreach (Match match in matches)
            {
                // Workaround issue in source file:
                var category = match.Groups["category"].Value.Trim();
                switch (category)
                {
                    case "CERT":
                    case "REG":
                    case "MIL":
                    case "UNCR":
                    case "JOINT": // E.g. Townsville
                        break;
                    case "WREG":
                        category = "REG";
                        break;
                    case "WCERT":
                        category = "CERT";
                        break;
                    default:
                        throw new ApplicationException("Unrecognised category.");
                }

                if (category != "UNCR")
                {
                    switch (match.Groups["state"].Value)
                    {
                        case "NSW":
                        case "VIC":
                        case "ACT":
                        case "QLD":
                        case "SA":
                        case "NT":
                        case "WA":
                        case "TAS":
                        case "OTH": // "Other"? Chrismas Island
                            break;
                        default:
                            throw new ArgumentException("State not recognised. Value was: " + match.Groups["state"].Value);
                    }
                    var latLng = new LatLng(
                        match.Groups["latHem"].Value[0],
                        int.Parse(match.Groups["latDeg"].Value),
                        int.Parse(match.Groups["latMin"].Value),
                        int.Parse(match.Groups["latSec"].Value),
                        match.Groups["lngHem"].Value[0],
                        int.Parse(match.Groups["lngDeg"].Value),
                        int.Parse(match.Groups["lngMin"].Value),
                        int.Parse(match.Groups["lngSec"].Value)
                        );
                    //result.Add(new
                    //{
                    //    Name = match.Groups["name"].Value,
                    //    CTAF = match.Groups["CTAF"].Value,
                    //    Category = match.Groups["category"].Value,
                    //    Lat = latLng.lat,
                    //    Lng = latLng.lng
                    //});
                    if (json.Length > 1)
                        json.Append(",\r\n");
                    json.Append(String.Format("{{\"Code\":\"{5}\",\"Name\":\"{0}\",\"CTAF\":\"{1}\",\"Category\":\"{2}\",\"Lat\":\"{3}\",\"Lng\":\"{4}\"}}",
                            match.Groups["name"].Value,
                            match.Groups["CTAF"].Value,
                            category,
                            Math.Round(latLng.lat, 4),
                            Math.Round(latLng.lng, 4),
                            match.Groups["code"].Value
                        ));
                }
            }
            json.Append("]");
            File.WriteAllText(target, json.ToString());

            return matches.Count;
        }
    }

    public class ProgressEventArgs : EventArgs
    {
        public ProgressEventArgs(string text) { Text = text; }
        public String Text { get; private set; }
    }

    public delegate void ProgressEventHandler(object sender, ProgressEventArgs e);
}
