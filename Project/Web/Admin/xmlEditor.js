﻿// TODO: initialise docNode to something when creating a dropdown field

var structure;
var instance;
var instanceName;
var instanceType;

function load() {
  var site = urlParam("site");
  if (site!="") {
    instanceName = site;
    instanceType = 'site';
  } else {
    instanceName = urlParam("contact");
    instanceType = 'contact';
  }
  status('Loading data...');
  makeRequest("GET", "siteData.aspx?get=" + instanceType + "structure", structureLoaded);
  makeRequest("GET", "siteData.aspx?get=" + instanceType + "data&name=" + instanceName, instanceLoaded);
}

function structureLoaded(request) {
  structure = request.responseXML; 
  if (instance)
    dataLoaded();
}

function instanceLoaded(request) {
  instance = request.responseXML;
  if (structure)
    dataLoaded();
}

function dataLoaded() {
  if (instance.documentElement.nodeName == 'error') {
    var errorText = instance.documentElement.firstChild.data;
    status(errorText);
    alert(errorText);
    return;
  }
  status('');
  if (instanceName == 'new') {
    var newInstance = createInstance(structure.documentElement);
    instance.documentElement.insertBefore(newInstance, instance.documentElement.firstChild);
  }
  var docNode=instance.documentElement.firstChild;

//  document.body.structureNode = structure.documentElement;
//  displayNodeInstance(docNode, document.body);
  
  var dataDiv = document.getElementById('data')
  dataDiv.docNode = docNode;
  displayChildNodes(structure.documentElement, dataDiv)
}

function saveToServer() {
  var submitButton = document.getElementById('Submit');
  submitButton.disabled = true;
  status('Saving...');
  var instanceXml = instance.xml;
  if (instance.xml == null)
     instanceXml = (new XMLSerializer()).serializeToString(instance);
  makeRequest('POST', 'siteData.aspx?post=' + instanceType + 'data&name=' + instanceName, savedToServer, instanceXml)
}

function savedToServer(request) {
  var submitButton = document.getElementById('Submit');
  submitButton.disabled = false;
  var errorText;
  if (request.responseXML != null)
    if (request.responseXML.firstChild != null)
      if (request.responseXML.firstChild.nodeName == 'error')
        errorText = request.responseXML.firstChild.firstChild.data;
  if (errorText == null) {
    status('Saved.');
    var newInstanceName = instance.documentElement.firstChild.firstChild.firstChild.data;
    if (urlDecode(instanceName) != urlDecode(newInstanceName))
      window.location = "editSiteData.aspx?" + instanceType + "=" + newInstanceName;
  } else {
    status("Not saved: " + errorText);
//    alert("Not saved:\n" + errorText);
  }
}

function displayChildNodes(structureParentNode, parentInstanceDiv) {
  var structureNode = structureParentNode.firstChild;
  var docParentNode = parentInstanceDiv.docNode;
  var docNode = docParentNode.firstChild;
  do {
    // create structure div
    var structDiv = document.createElement('div');
    structDiv.className = 'structure';
    structDiv.structureNode = structureNode; // NB
    parentInstanceDiv.appendChild(structDiv);
    structDiv.instanceCount = 0;

    // atttach any instance nodes
    if (docNode) {
      while (structureNode.nodeName == docNode.nodeName) {
        displayNodeInstance(docNode, structDiv);

        docNode = docNode.nextSibling;
        if (docNode == null) break;
      }
    }

    setAddRemoveButtons(structDiv);
    
    structureNode = structureNode.nextSibling;

    if (structureNode == null && docNode != null) {
      debug("Validation failure: unexpected element '" + docNode.nodeName + "'");
    }
  } while (structureNode != null)
}

function displayNodeInstance(docNode, structDiv) {
  var instanceDiv = document.createElement('div');
  instanceDiv.className = 'instance';
  instanceDiv.innerHTML = docNode.nodeName;
  instanceDiv.docNode = docNode; // NB
  
  if (structDiv.lastChild == null)
    structDiv.appendChild(instanceDiv);
  else
    if (structDiv.lastChild.nodeName == 'BUTTON')
      structDiv.insertBefore(instanceDiv, structDiv.lastChild);  
    else
      structDiv.appendChild(instanceDiv);
  
  structDiv.instanceCount++;

  if (structDiv.structureNode.childNodes.length > 0) {
    displayChildNodes(structDiv.structureNode, instanceDiv);
  } else {
    var ref = structDiv.structureNode.getAttribute('ref');
    if (ref) {
      var dropdown = document.createElement('select');
      var refs = docNode.ownerDocument.documentElement.getElementsByTagName(ref + 's')[0];
      for (var i = 0; i < refs.childNodes.length; i++) {
        var option = document.createElement("OPTION");
        option.text = refs.childNodes[i].firstChild.data;
        option.value = option.text;
        if (option.value == docNode.firstChild.data) 
          option.selected = true;
        dropdown.options.add(option);
      }
      dropdown.value = trim(docNode.firstChild.data);
      if (dropdown.value != trim(docNode.firstChild.data))
        docNode.firstChild.data = dropdown.value; // newly created doc node - initialise to random legal value
      dropdown.onchange = function() {this.parentNode.docNode.firstChild.data = dropdown.value}      
      instanceDiv.appendChild(dropdown);
    } else {
      var textInput = document.createElement('textarea');
      textInput.onkeyup = function() {textareaSetRows(this)};
      textInput.onmouseup = textInput.onkeyup;
      textInput.value = trim(docNode.firstChild.data);
      textInput.onchange = function() {this.parentNode.docNode.firstChild.data = textInput.value};
      instanceDiv.appendChild(textInput);
      textareaSetRows(textInput);
    }
  }
}

function setAddRemoveButtons(structDiv) {
  var structureNode = structDiv.structureNode;
  var min = structureNode.getAttribute('min');
  if (min == null) min = 1;
  var max = structureNode.getAttribute('max');
  if (max == null) max = 1;

  // valid number of instances?
  if (structDiv.instanceCount < min) debug('Validation failure: expected at least ' + min + " of element '" + structureNode.nodeName + "'");
  if (structDiv.instanceCount > max) debug('Validation failure: expected at most ' + max + " of element '" + structureNode.nodeName + "'");
  
  //set add & remove buttons 
  var i; var thisInstanceDiv;
  if (structDiv.instanceCount > min) { // add 'remove' buttons if not present
    for (i = 0; i < structDiv.instanceCount; i++) {
      thisInstanceDiv = structDiv.childNodes[i];
      if (thisInstanceDiv.lastChild.nodeName != 'BUTTON') {
        var rmvBtn = document.createElement('BUTTON');
        rmvBtn.appendChild(document.createTextNode("Remove '" + structureNode.nodeName + "'"));
        rmvBtn.onclick = function() {removeInstance(this.parentNode)};
        thisInstanceDiv.appendChild(rmvBtn);
      }
    }
  } else if (structDiv.instanceCount > 0) // remove 'remove' buttons if present
      for (i = 0; i < structDiv.instanceCount; i++) {
        thisInstanceDiv = structDiv.childNodes[i];
        if (thisInstanceDiv.lastChild.nodeName == 'BUTTON')
          thisInstanceDiv.removeChild(thisInstanceDiv.lastChild);
      }

  if ((structDiv.instanceCount < max) || (max == 'unbounded')) { // add 'add' button if not present
    var addButtonPresent = false;
    if (structDiv.lastChild != null) if (structDiv.lastChild.nodeName == 'BUTTON')
      addButtonPresent = true;
    
    if (!addButtonPresent) {
      var addBtn = document.createElement('BUTTON');
      addBtn.appendChild(document.createTextNode("Add '" + structureNode.nodeName + "'"));
      addBtn.onclick = function() {addInstance(this.parentNode)};
      structDiv.appendChild(addBtn);
    }
  } else { // remove 'add' button if present
    if (structDiv.lastChild.nodeName == 'BUTTON') {
      structDiv.removeChild(structDiv.lastChild);
    }
  }
}

function trim(str) {
  return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function textareaSetRows(textarea) {
  if (textarea.rows == -1) textarea.rows = 1;
  while (textarea.rows > 1 && textarea.scrollHeight == textarea.clientHeight) {
    textarea.rows--;
  }
  
  while (textarea.scrollHeight > textarea.clientHeight) {
    textarea.rows++;
  }
}

function addInstance(structDiv) {
  var insertBeforeDocNode = null;
  var candidateStructDiv = structDiv.nextSibling;  
  while (candidateStructDiv != null && insertBeforeDocNode == null) {
    if (candidateStructDiv.instanceCount > 0) {
      insertBeforeDocNode = candidateStructDiv.firstChild.docNode;
    }
    candidateStructDiv = candidateStructDiv.nextSibling;
  }

  var newDocNode = createInstance(structDiv.structureNode);

  var docParentNode = structDiv.parentNode.docNode;
  if (insertBeforeDocNode) {
    docParentNode.insertBefore(newDocNode, insertBeforeDocNode);
  } else {
    docParentNode.appendChild(newDocNode);
  }
  displayNodeInstance(newDocNode, structDiv);
  setAddRemoveButtons(structDiv);
}

function removeInstance(instanceDiv) {
  var parentStructNode = instanceDiv.parentNode;
  instanceDiv.docNode.parentNode.removeChild(instanceDiv.docNode);
  parentStructNode.removeChild(instanceDiv);
  parentStructNode.instanceCount--;
  setAddRemoveButtons(parentStructNode);
}

function createInstance(structureNode) {
  var newDocNode = structureNode.cloneNode(false);
  while (newDocNode.childNodes[0]) {
    newDocNode.removeChild(newDocNode.childNodes[0]);
  }
  while (newDocNode.attributes[0]) {
    newDocNode.removeAttribute(newDocNode.attributes[0].name);
  }
  
  var structureNodeChildrenCount = structureNode.childNodes.length;
  if (structureNodeChildrenCount > 0) {
    //create all required children
    for (var i = 0; i < structureNodeChildrenCount; i++) {
      var structureCurrentChild = structureNode.childNodes[i];
      var min = structureCurrentChild.getAttribute('min');
      if (min == null) min = 1;
      for (var j = 0; j < min; j++) {
        newDocNode.appendChild(createInstance(structureCurrentChild));
      }
    }
  } else {
    newDocNode.appendChild(newDocNode.ownerDocument.createTextNode(''));
  }

  return newDocNode;
}

function debug(text) {
  document.getElementById('debug').innerHTML += text + '<br />';
}

function status(text) {
  document.getElementById('status').innerHTML = text;
}

function makeRequest(type, url, handler, content) {
  var http_request = createHttpRequest();
  http_request.onreadystatechange = function() {if (http_request.readyState==4) {handler(http_request)}};
  http_request.open(type, url, true);
  if (type == 'GET')
    http_request.send(null)
  else if (type == 'POST') {
    http_request.setRequestHeader("Content-Type", "text/xml");
    //http_request.send("<?xml version='1.0' encoding='UTF-8'?>" + encodeURI(content));
    http_request.send(content);
  } else
    debug("type must be 'GET' or 'POST'")
}

function createHttpRequest () {
  var http_request = false;
  if (window.XMLHttpRequest) { // Mozilla, Safari,...
    http_request = new XMLHttpRequest();
    if (http_request.overrideMimeType) {
      http_request.overrideMimeType('text/xml');
    }
  } else if (window.ActiveXObject) { // IE
    try {
      http_request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        http_request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {}
    }
  }
  if (!http_request) {
    alert('Sorry, but your browser does not support AJAX');
    return false;
  }
  return http_request;
}

function urlParam(name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return results[1];
}

function urlDecode(str) {
  str = str.replace('+', ' ');
  str = unescape(str);
  return str;
}