﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editSiteData.aspx.cs" Inherits="VHPASiteGuide.editSite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link rel="stylesheet" type="text/css" href="editor.css" />
    <script type="text/javascript" src="xmlEditor.js"></script>
    <title>Site Editor</title>
</head>
<body onload="load()">
  <div id='header'>
    <div id='status'></div>
    <input id="Submit" type="submit" value="Save" onclick="saveToServer()"/>
  </div>
  <div id='data'></div>
  <div id='debug'></div>
</body>
</html>
