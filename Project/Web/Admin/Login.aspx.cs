﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace VHPASiteGuide
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //If no accounts exist yet, create the admin account
            if (Membership.GetAllUsers().Count == 0)
            {
                Membership.CreateUser("admin", "x6ddR4ja");
            }
        }

    }
}
