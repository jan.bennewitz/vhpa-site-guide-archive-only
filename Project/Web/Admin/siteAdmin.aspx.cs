﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace VHPASiteGuide
{
  public partial class _Default : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!User.Identity.IsAuthenticated)
        Response.Redirect("Login.aspx");
      else if (User.Identity.Name == "admin")
      {
        Sites.XPath = "//siteFreeFlight";
        Contacts.XPath = "//contact";
      }
      else
      {
        Sites.XPath = "//siteFreeFlight[responsibleName='" + User.Identity.Name + "']";
        Contacts.XPath = "//contact[name=//area[admin=\"" + User.Identity.Name + "\"]/siteFreeFlight/contactName or name=//area[admin=\"" + User.Identity.Name + "\"]/siteFreeFlight/responsibleName]";
      }
      btnCreateUserAcct.Visible = (User.Identity.Name == "admin");
      btnGenerate.Visible = (User.Identity.Name == "admin");
    }

    protected void btnCreateUserAcct_Click(object sender, EventArgs e)
    {
      if (User.Identity.Name == "admin")
        Response.Redirect("AddUserAcct.aspx");
    }

    protected void btnGenerate_Click(object sender, EventArgs e)
    {
      if (User.Identity.Name == "admin")
      {
        string appDataPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_data");
        Generator generator = new Generator(appDataPath, AppDomain.CurrentDomain.BaseDirectory);
        generator.GenerateAll();
//        generator.CopyIncludeDir(System.IO.Path.Combine(appDataPath, "Include"));   Easier to do by hand, else we'd have to set permissions
      }
    }

    protected string ContactsList()
    {
      return "Blah test 123";
    }
  }
}
