﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="siteAdmin.aspx.cs" Inherits="VHPASiteGuide._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Site guide administration</title>
</head>
<body>
    <form id="form1" runat="server">
    <p>
        <asp:Button ID="btnCreateUserAcct" runat="server" 
            onclick="btnCreateUserAcct_Click" Text="Create User Account" Visible="False" />
    
        <asp:Button ID="btnGenerate" runat="server" onclick="btnGenerate_Click" 
          Text="Generate Site Guide" />
    
        <br /><br />
        Logged in as <asp:LoginName ID="LoginName1" runat="server" />&nbsp;
        <asp:LoginStatus ID="LoginStatus1" runat="server" />
            <asp:DataList ID="SitesList" runat="server" CellPadding="4" 
            DataSourceID="Sites" ForeColor="#333333">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#EFF3FB" />
            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
                <a href="../Sites/<%# XPath("name") %>.html"><%# XPath("name") %></a>
                <br />
                <a href="editSiteData.aspx?site=<%# XPath("name") %>">edit</a>
                <br />
                <%#XPath("shortlocation")%><br />
                <%#XPath("rating")%><br />
            </ItemTemplate>
        </asp:DataList>
        <a href="editSiteData.aspx?site=new">Create new site</a>
        
            <asp:DataList ID="DataListContacts" runat="server" CellPadding="4" 
            DataSourceID="Contacts" ForeColor="#333333">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#EFF3FB" />
            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
                <a href="editSiteData.aspx?contact=<%# XPath("name") %>"><%# XPath("name") %></a>
                <br />
                <%#XPath("fullname")%><br />
                <%#XPath("link")%><br />
                <%#XPath("address")%><br />
                <%#XPath("phone")%><br />
            </ItemTemplate>
        </asp:DataList>
        <a href="editSiteData.aspx?contact=new">Create new contact</a>
        
        <asp:XmlDataSource ID="Sites" runat="server" DataFile="~/App_Data/siteGuideData.xml"/>
        <asp:XmlDataSource ID="Contacts" runat="server" DataFile="~/App_Data/siteGuideData.xml"/>
    </p>
    <% Response.Write(ContactsList());%>
    </form>
</body>
</html>
