﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Xml;
using System.Xml.Schema;
using System.IO;
using System.Diagnostics;

namespace VHPASiteGuide
{
  public partial class siteDataPage : System.Web.UI.Page
  {
    private static string validationMessage;

    private String xsdPath, xmlPath;
    private XmlDocument siteGuideData;

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        if (!User.Identity.IsAuthenticated)
        {
          throw new ApplicationException("You are not logged in. Please <a href='Login.aspx' target='_blank'>log in.</a>");
        }

        xsdPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_data\\siteGuideData.xsd");
        xmlPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_data\\siteGuideData.xml");

        populateSiteGuideData();

        string postedData = Request.QueryString["post"]; // must equal 'sitedata' or 'contactdata'
        string requestedData = Request.QueryString["get"]; // must equal 'sitestructure', 'contactstructure', 'sitedata' or 'contactdata'
        string instanceName = Request.QueryString["name"]; // site name or contact name

        if (postedData != null)
        {
          XmlDocument postedDoc = new XmlDocument();
          postedDoc.LoadXml(stringFromStream(Request.InputStream, new System.Text.UTF8Encoding()));
          switch (postedData)
          {
            case "sitedata":
              updateSite(instanceName, postedDoc);
              break;
            case "contactdata":
//              updateContact(instanceName, postedDoc);
              break;
            default:
              throw new ApplicationException("'post' parameter must equal 'sitedata' or 'contactdata'");
          }
        }

        if (requestedData != null)
        {
          XmlTextWriter writer = new XmlTextWriter(Response.OutputStream, System.Text.Encoding.UTF8);
          switch (requestedData)
          {
            case "sitestructure":
              getStructure(siteGuideData, "siteFreeFlight").WriteTo(writer);
              break;
            case "contactstructure":
              getStructure(siteGuideData, "contact").WriteTo(writer);
              break;
            case "sitedata":
              getSiteAndContacts(instanceName).WriteTo(writer);
              break;
            case "contactdata":
              getContact(instanceName).WriteTo(writer);
              break;
            default:
              throw new ApplicationException("'get' parameter must equal 'sitestructure', 'contactstructure', 'sitedata' or 'contactdata'");
          }
          writer.Close();
        }
      }
      catch (Exception ex) // TODO make this more specific
      {
        Response.Write("<error><![CDATA[" + ex.Message + "]]></error>");
      }
      return;
    }

    private void populateSiteGuideData()
    {
      XmlReaderSettings settings = new XmlReaderSettings();
      settings.Schemas.Add(null, xsdPath);
      settings.ValidationType = ValidationType.Schema;
      //settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);

      siteGuideData = new XmlDocument();
      XmlReader reader = XmlReader.Create(xmlPath, settings);
      siteGuideData.Load(reader);
      reader.Close();

      siteGuideData.Validate(new ValidationEventHandler(ValidationEventHandler));
    }

    private XmlDocument getSiteAndContacts(string siteName)
    {
      XmlDocument doc = new XmlDocument();
      doc.AppendChild(doc.CreateElement("data"));

      if (siteName != "new")
      {
        doc.DocumentElement.AppendChild(doc.ImportNode(siteData(siteName), true));
      }
      XmlNode xmlContacts = doc.CreateElement("contacts");
      doc.DocumentElement.AppendChild(xmlContacts);
      foreach (XmlNode contact in administeredContacts(User.Identity.Name))
      {
        xmlContacts.AppendChild(doc.ImportNode(contact, true));
      }
      return doc;
    }

    private XmlDocument getContact(string name)
    {
      XmlDocument doc = new XmlDocument();
      doc.AppendChild(doc.CreateElement("data"));

      if (name != "new")
      {
        doc.DocumentElement.AppendChild(doc.ImportNode(contactData(name), true));
      }
      return doc;
    }
    
    private XmlNodeList administeredContacts(string user)
    {
      if (user == "admin")
        return siteGuideData.SelectNodes("//contact/name");
      else
        return siteGuideData.SelectNodes("//contact[name=//area[admin=\"" + user + "\"]/siteFreeFlight/contactName or name=//area[admin=\"" + user + "\"]/siteFreeFlight/responsibleName]/name");
    }

    private static XmlDocument getStructure(XmlDocument document, string elementName)
    {
      XmlDocument structure = new XmlDocument();
      structure.AppendChild(structure.CreateElement(elementName));
      XmlSchemaComplexType type = document.SelectSingleNode("//" + elementName).SchemaInfo.SchemaType as XmlSchemaComplexType;
      generateStructureXml(type.ContentTypeParticle, structure.DocumentElement);
      return structure;
    }

    private void updateSite(string postedSiteName, XmlDocument postedData)
    {
      XmlNode newSite = postedData.SelectSingleNode("//siteFreeFlight");
      if (postedSiteName == "new")
      {
        // add new site - assumption for the moment: only one area is administered by this admin - TODO: will have to change this.
        XmlNode area;
        if (User.Identity.Name == "admin")
          area = siteGuideData.SelectSingleNode("//area");
        else
          area = siteGuideData.SelectSingleNode("//area[admin=\"" + User.Identity.Name + "\"]");
        if (area == null)
        {
          throw new ApplicationException("You do not have admin rights for any area.");
        }
        area.AppendChild(siteGuideData.ImportNode(newSite, true));
      }
      else
      {
        XmlNode oldSite = siteData(postedSiteName);
        string oldXml = siteGuideData.InnerXml;
        oldSite.ParentNode.ReplaceChild(siteGuideData.ImportNode(newSite, true), oldSite);
        if (siteGuideData.InnerXml == oldXml)
        {
          Response.Write("<ok>No changes.</ok>");
          return;
        }
      }

      validationMessage = "";
      siteGuideData.Validate(new ValidationEventHandler(ValidationEventHandlerToMessage));
      if (validationMessage != "")
      {
        throw new ApplicationException("Your changes did not validate. " + validationMessage);
      }

      // save old version
      string backupDir = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_data\\History");
      FileInfo oldSiteGuideDataFile = new FileInfo(xmlPath);
      string newSiteGuideDataFileName = oldSiteGuideDataFile.Name + "_" + User.Identity.Name + "_UTC" + System.DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss.ffff");
      Generator.EnsureDirExists(backupDir);
      oldSiteGuideDataFile.CopyTo(Path.Combine(backupDir, newSiteGuideDataFileName));

      // save new version
      siteGuideData.Save(xmlPath);

      // re-generate site guide
      string newSiteName = postedData.SelectSingleNode("//siteFreeFlight/name").InnerText;
      Generator generator = new Generator(
          System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_data"),
          AppDomain.CurrentDomain.BaseDirectory);
      string[] siteNames = {newSiteName};
      generator.GenerateSpecificSites(siteNames);

      if (newSiteName != postedSiteName && postedSiteName != "new")
      {
        generator.DeleteSitePage(postedSiteName); // TODO: Maybe replace with a redirect page?
      }

      Response.Write("<ok/>");
    }

    private XmlNode siteData(string siteName)
    {
      XmlNode site;
      if (User.Identity.Name == "admin")
        site = siteGuideData.SelectSingleNode("//area/siteFreeFlight[name=\"" + siteName + "\"]");
      else
        site = siteGuideData.SelectSingleNode("//area[admin=\"" + User.Identity.Name + "\"]/siteFreeFlight[name=\"" + siteName + "\"]");
      if (site == null)
      {
        site = siteGuideData.SelectSingleNode("//area/siteFreeFlight[name=\"" + siteName + "\"]");
        if (site == null)
          throw new ApplicationException("This site does not exist.");
        else
          throw new ApplicationException("You do not have admin rights for this site.");
      }
      return site;
    }

    private XmlNode contactData(string name)
    {
      XmlNode contact = siteGuideData.SelectSingleNode("//contact[name=\"" + name + "\"]");
      if (contact == null)
        throw new ApplicationException("This contact does not exist.");
      if (User.Identity.Name != "admin")
        if (siteGuideData.SelectNodes("//area[admin=\"" + User.Identity.Name + "\"]/siteFreeFlight[contactName=\"" + name + "\" or responsibleName=\"" + name + "\"]").Count == 0)
          throw new ApplicationException("You do not have admin rights for this contact.");
      return contact;
    }

    private static string stringFromStream(Stream source, System.Text.Encoding encoding)
    {
      Int32 strLen = Convert.ToInt32(source.Length);
      byte[] strArr = new byte[strLen];
      source.Read(strArr, 0, strLen);
      return encoding.GetString(strArr);
    }

    static void generateStructureXml(XmlSchemaParticle particle, XmlElement parent)
    {
      if (particle is XmlSchemaElement)
      {
        XmlSchemaElement elem = particle as XmlSchemaElement;
        XmlElement child = parent.OwnerDocument.CreateElement(elem.Name);
        if (particle.MinOccursString != null)
          child.Attributes.Append(parent.OwnerDocument.CreateAttribute("min")).Value = particle.MinOccursString;
        if (particle.MaxOccursString != null)
          child.Attributes.Append(parent.OwnerDocument.CreateAttribute("max")).Value = particle.MaxOccursString;
        if (elem.ElementSchemaType.TypeCode != XmlTypeCode.String && elem.ElementSchemaType.TypeCode != XmlTypeCode.None)
          child.Attributes.Append(parent.OwnerDocument.CreateAttribute("type")).Value = elem.ElementSchemaType.TypeCode.ToString();
        parent.AppendChild(child);
        
        if (elem.Constraints.Count > 0) {
          string refers = ((System.Xml.Schema.XmlSchemaKeyref) (elem.Constraints[0]) ).Refer.Name;
          if (refers == "keyContactName") {
            child.Attributes.Append(parent.OwnerDocument.CreateAttribute("ref")).Value = "contact";
          }
        }

        if (elem.RefName.IsEmpty)
        {
          XmlSchemaType type = (XmlSchemaType)elem.ElementSchemaType;
          XmlSchemaComplexType complexType = type as XmlSchemaComplexType;
          if (complexType != null)// for complex types, show details
          {

            foreach (XmlSchemaAttribute attribute in complexType.Attributes)
            {
              child.Attributes.Append(parent.OwnerDocument.CreateAttribute("attribute")).Value = attribute.Name;
            }
            generateStructureXml(complexType.ContentTypeParticle, child);
          }
        }
        else
          Debug.WriteLine("!");
      }
      else if (particle is XmlSchemaGroupBase)
      { //xs:all, xs:choice, xs:sequence
        XmlSchemaGroupBase baseParticle = particle as XmlSchemaGroupBase;
        foreach (XmlSchemaParticle subParticle in baseParticle.Items)
          generateStructureXml(subParticle, parent);
      }
      else
      {
        Debug.WriteLine("!");
      }
    }

    static void ValidationEventHandler(object sender, ValidationEventArgs e)
    {
      switch (e.Severity)
      {
        case XmlSeverityType.Error:
          Console.WriteLine("Error: {0}", e.Message);
          break;
        case XmlSeverityType.Warning:
          Console.WriteLine("Warning {0}", e.Message);
          break;
      }
    }

    static void ValidationEventHandlerToMessage(object sender, ValidationEventArgs e)
    {
      if (validationMessage.Length > 0)
        validationMessage += "<br>";
      switch (e.Severity)
      {
        case XmlSeverityType.Error:
          validationMessage += "Error: " + e.Message;
          break;
        case XmlSeverityType.Warning:
          validationMessage += "Warning: " + e.Message;
          break;
      }
    }
  }
}
