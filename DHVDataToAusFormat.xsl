<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"
				media-type="text/xml"
			indent="yes"
			cdata-section-elements="description"/>

	<xsl:template match="/">
		<siteGuideData>
			<area name="D" fullName="Germany">
				<xsl:apply-templates select="//Row[@subtype!=2]">
					<xsl:sort select="@cid"/>
					<xsl:sort select="@subtype"/>
				</xsl:apply-templates>
			</area>
		</siteGuideData>
	</xsl:template>

	<xsl:template match="Row">
		<siteFreeFlight>
			<name><xsl:value-of select="@label"/>(<xsl:value-of select="@id"/>.<xsl:value-of select="@cid"/>)</name>
			<description><xsl:value-of select="@html"/></description>
			<launch>
				<lat><xsl:value-of select="@lat"/></lat>
				<lon><xsl:value-of select="@lng"/></lon>
			</launch>
		</siteFreeFlight>
	</xsl:template>
</xsl:stylesheet>