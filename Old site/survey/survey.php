<?php

    $file_name = "survey.txt";

    $file_out = fopen($file_name, "a");

    if (!$file_out)
    {
      echo "<h2 style=\"text-align: center\">".
           "There is a problem opening the survey file!<br>".
           "Systems Administrator has been informed, Please try again Later.</h2>";

      $subject = "VHPA survey program";
      $content = "Problems opening survey file\n\n".
           "Check that file exists in directory and permission are correct\n".
         $from = "Webmaster@vhpa.org.au";
         $to = "Webmaster@vhpa.org.au";
         mail ($to, $subject, $content, $from);

      exit;
    }
    $todays_date = date("d/m/y");
    $output = $name."|".$hgfa."|".$state."|".$email."|".$todays_date."|".
              $aircraft."|".$hours."|".$Apollo_Bay."|".$Barwon_Heads."|".$Bells_Beach."|".
              $Berrys_Beach."|".$Ben_More."|".$Ben_Nevis."|".$Birchip."|".$Boort."|".
              $Buangor."|".$Buffalo."|".$Buncles."|".$Buninyong."|".$Cairns_Bay."|".
              $Cape_Liptrap."|".$Cave_Hill."|".$Clifton_Springs."|".$Colliers_Gap."|".
              $Craigie_Road."|".$Dandenong."|".$Donna_Buang."|".$Eagles_Nest."|".
              $Elephant."|".$Elliot."|".$Elmhurst_Hill."|".$Emu."|".$Eumeralla."|".
              $Flaxmans_Hill."|".$Flinders_Golf."|".$Flinders_Monument."|".$Gordon."|".
              $Grannies_Grave."|".$Hollowback."|".$Johanna."|".$Kilcunda."|".$Landscape."|".
              $Logans_Beach."|".$Lonarch."|".$Meuron."|".$Mittamatite."|".$Murmungee."|".
              $Mystic."|".$Ocean_Grove."|".$Paps."|".$Pines."|".$Portland."|".
              $Portsea."|".$Shoreham."|".$Spion."|".$Sugarloaf."|".$Sunnyside."|".
              $Tawonga."|".$Thistle_Hill."|".$Three_Sisters."|".$Werribee_Gorge."|".
              $Yarragon."|".$comments."|\n";
    fwrite($file_out, $output);
    if (!$file_out)
    {
      echo "<h2 style=\"text-align: center\">".
           "There is a problem writing to the survey file!<br>".
           "Systems Administrator has been informed, Please try again Later.</h2>";

      $subject = "VHPA survey program";
      $content = "Problems writing to survey file\n\n".
           "Check there is enough room in directory and permission are correct\n".
         $from = "Webmaster@vhpa.org.au";
         $to = "Webmaster@vhpa.org.au";
         mail ($to, $subject, $content, $from);

      exit;
     }


    fclose ($file_out);
    flush ($file_out);
?>
<html>
<head><!-- Author: Sharyn Gingell-Kent-->
<title>VHPA Survey Thankyou</title>
</head>

<body bgcolor="#FFFFFF">
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</font> 
<h2 style="text-align:center"><font face="Verdana, Arial, Helvetica, sans-serif">Thank 
  you for participating <br>
  in the VHPA 2002/03 Site Survey</font></h2>
<h2 style="text-align:center">&nbsp;</h2>
<h2 style="text-align:center"><font face="Verdana, Arial, Helvetica, sans-serif"><a href="http://www.vhpa.org.au/">Press 
  this link to return to the VHPA home page</a></font></h2>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<br>
<br>
</font>
</body>
</html>

<?php

    exit;


?>


